/*
 * GeometryBuilder.cpp
 *
 *  Created on: Feb 16, 2016
 *      Author: tfy12jsg
 */

#include <vr/Vec3.h>

#include <vector>

#include "GeometryBuilder.h"

GeometryBuilder::GeometryBuilder() {
	// TODO Auto-generated constructor stub

}

GeometryBuilder::~GeometryBuilder() {
	// TODO Auto-generated destructor stub
}

shared_ptr<Geometry> GeometryBuilder::buildQuad(float size) {
	vr::Vec3Vector v_quad;
	vr::Vec3Vector n_quad;
	vr::UIntVector i_quad;
	vr::Vec2Vector t_quad;

	v_quad.push_back(vr::Vec3(-size, -size, 0));
	v_quad.push_back(vr::Vec3(size, -size, 0));
	v_quad.push_back(vr::Vec3(-size, size, 0));
	v_quad.push_back(vr::Vec3(size, size, 0));

	n_quad.push_back(vr::Vec3(0, 0, 1.0f));
	n_quad.push_back(vr::Vec3(0, 0, 1.0f));
	n_quad.push_back(vr::Vec3(0, 0, 1.0f));
	n_quad.push_back(vr::Vec3(0, 0, 1.0f));

	i_quad.push_back(0);
	i_quad.push_back(1);
	i_quad.push_back(2);
	i_quad.push_back(1);
	i_quad.push_back(2);
	i_quad.push_back(3);

	t_quad.push_back(vr::Vec2(0, 0));
	t_quad.push_back(vr::Vec2(1.0f, 0));
	t_quad.push_back(vr::Vec2(0, 1.0f));
	t_quad.push_back(vr::Vec2(1.0f, 1.0f));

	shared_ptr<Geometry> geom(new Geometry);
	geom->setVertices(v_quad);
	geom->setNormals(n_quad);
	geom->setIndices(i_quad);
	geom->setTexCoords(t_quad);

	return geom;
}
