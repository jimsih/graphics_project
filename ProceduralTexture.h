/*
 * ProceduralTexture.h
 *
 *  Created on: Mar 10, 2016
 *      Author: tfy12jsg
 */

#ifndef PROCEDURALTEXTURE_H_
#define PROCEDURALTEXTURE_H_

class ProceduralTexture {
public:
	ProceduralTexture();
	virtual ~ProceduralTexture();

	void apply();

	GLuint getTexture();

private:
	GLuint fbo;
	GLuint texture;
};

#endif /* PROCEDURALTEXTURE_H_ */
