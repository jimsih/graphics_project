/*
 * Scene.cpp
 *
 *  Created on: Feb 5, 2016
 *      Author: tfy12jsg
 */

#include <GL/glew.h>
#include <iostream>
#include <vr/glErrorUtil.h>
#include <string>
#include <vector>
#include <vr/DrawText.h>
#include <GL/glut.h>

#include "Scene.h"
#include "Globals.h"
#include "Light.h"

Scene* Scene::instance = NULL;

Scene::Scene() {

	biasMatrix = vr::Matrix( 0.5, 0.0, 0.0, 0.0,
							 0.0, 0.5, 0.0, 0.0,
							 0.0, 0.0, 0.5, 0.0,
							 0.5, 0.5, 0.5, 1.0);
}

void Scene::screenSizeUpdate() {
	m_gBuffer.init();
	m_postProcessor->init();
}

void Scene::draw() {

	/* Update Scene */
	updateVisitor.traverseScene();

	renderShadowPass();

	renderGeometryPass();

	/* Light pass */
	GLuint buffer = 0;
	if (m_postProcessor->isActive()) {
		m_postProcessor->apply();
		buffer = m_postProcessor->getFramebuffer();
	}

	m_gBuffer.draw();
	m_gBuffer.copyDepthBuffer(buffer);

	/* Render extra items */
	for (shared_ptr<Node> light : m_lightsources) {
		Light* l = (Light*) light.get();
		if (l->isActive())
			l->draw();
	}

	renderBillboards();
	renderSkybox();

	if (m_debugMode) {
		m_hud.draw();
	}


	if (m_postProcessor->isActive()) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		m_postProcessor->draw();
	}

	m_gBuffer.copyDepthBuffer(0);

	/* Calculate fps */
	double dt = Globals::getInstance()->timer.getTime();
	string str = std::to_string((int)(1000/dt));
	vr::Text::drawText(25.0, 30.0, str.append(" FPS").c_str());

	glutSwapBuffers();

	Globals::getInstance()->timer.reset(true);
}

void Scene::renderShadowPass() {
	setGlobalProgram(Globals::getInstance()->getProgram("shadowProgram"));
	useGlobalProgram(true);

	Light* light = (Light*)m_lightsources[0].get();
	shared_ptr<Camera> lightCamera(new Camera(light->getPosition(), vr::Vec3(0,0,0), vr::Vec3(0, 1.0f, 0)));
	lightCamera->setProjection(light->getProjection());

	shared_ptr<Camera> mainCamera(m_defaultCamera);
	setMainCamera(lightCamera);

	m_shadowMap.apply();
	renderVisitor.traverseScene();
	useGlobalProgram(false);
	setMainCamera(mainCamera);
}

void Scene::renderGeometryPass() {
	setGlobalProgram(Globals::getInstance()->getProgram("geomPassProgram"));
	useGlobalProgram(true);
	m_gBuffer.apply();
	renderVisitor.traverseScene();
	useGlobalProgram(false);
}


void Scene::renderSkybox() {
	//glCullFace(GL_FRONT);
	glDepthFunc(GL_LEQUAL);

	Geometry* skybox = (Geometry*) m_skybox.get();
	GLuint program = skybox->getState()->getProgram();
	glUseProgram(program);
	skybox->getState()->apply();
	m_defaultCamera->apply(program);

	skybox->draw(program);

	glDepthFunc(GL_LESS);

	glUseProgram(0);
}

void Scene::renderBillboards() {
	vr::Vec3 pos = vr::Vec3(-20, 5, 0);
	vr::Vec3 look = (m_defaultCamera->getPosition() - pos).normalized();
	vr::Vec3 right = (vr::Vec3(0, 1.0f, 0) ^ look).normalized();
	vr::Vec3 up = look ^ right;

	vr::Matrix rot = vr::Matrix(-right.x(), 0, -right.z(), 0,
								up.x(), up.y(), up.z(), 0,
								look.x(), 0, look.z(), 0,
								pos.x(), pos.y(), pos.z(), 1);

	GLuint program = debugQuad->getState()->getProgram();

	glUseProgram(program);
	debugQuad->getState()->apply(program);
	m_defaultCamera->apply(program);

	glUniformMatrix4fv(glGetUniformLocation(program, "M"), 1, GL_FALSE, rot.ptr());
	debugQuad->draw(program);

	glUseProgram(0);
}

vr::Matrix Scene::getShadowBiasMatrix(unsigned int lightNumber) {
	Light* light = (Light*)m_lightsources[lightNumber].get();
	vr::Matrix depthProjection = light->getProjection();
	vr::Matrix depthView = vr::Matrix::lookAt(light->getPosition(), vr::Vec3(0,0,0), vr::Vec3(0,1.0f,0));
	vr::Matrix depthMVP = depthView * depthProjection;
	vr::Matrix depthBiasMVP = depthMVP * biasMatrix;

	return depthBiasMVP;

}

void Scene::applyLightsources(GLuint program) {

	unsigned int nrOfPointLights = 0;
	unsigned int nrOfDirectionalLights = 0;
	for (unsigned int i=0; i<m_lightsources.size(); i++) {
		Light* light = (Light*)m_lightsources[i].get();
		if (light->isActive()) {
			if (light->getType() == Light::POINT) {
				light->apply(program, nrOfPointLights);
				nrOfPointLights++;
			} else if (light->getType() == Light::DIRECTIONAL) {
				light->apply(program, nrOfDirectionalLights);
				nrOfDirectionalLights++;
			}
		}
	}

	glUniform1i(glGetUniformLocation(program, "numPointLights"), nrOfPointLights);
	glUniform1i(glGetUniformLocation(program, "numDirectionalLights"), nrOfDirectionalLights);
}
