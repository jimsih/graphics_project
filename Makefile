OBJS   = main.o Camera.o Transform.o Geometry.o Group.o Material.o Light.o State.o Reader.o shadertools.o Scene.o InputHandler.o NodeVisitor.o RenderVisitor.o UpdateVisitor.o Texture.o Globals.o GeometryBuilder.o RenderToTexture.o ShadowMap.o TextureSkyBox.o HUD.o GBuffer.o PostProcessor.o ProceduralTexture.o
TARGET = MySceneGraph

CXX = g++

# Non-Optimized
DBFLAGS  = -O0 -g3 -ggdb3 -fno-inline
# Optimized
#DBFLAGS = -O2 

WFLAGS   =  -pthread -Wall -ansi -std=gnu++11
GLFLAGS  = `pkg-config --cflags gtk+-3.0` -Ivrlib/include/ 
LGLFLAGS = `pkg-config --libs gtk+-3.0` -Lvrlib/lib/ -lvrlib -lfreetype -lpng -ljpeg -lz -lpthread -lGL -lGLEW -lGLU -lglut
CXXFLAGS = $(WFLAGS) $(DFLAGS) $(GLFLAGS)
LDFLAGS  = -export-dynamic -lXext -lX11 $(LGLFLAGS)


all: $(TARGET)
clean:
	rm -f $(OBJS)

.SUFFIXES: .cc
.cc.o:
	$(CXX) -c $(CXXFLAGS) $<
.c.o:
	$(CXX) -c $(CXXFLAGS) $<
$(TARGET): $(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LDFLAGS) 
