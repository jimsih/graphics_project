/*
 * Geometry.h
 *
 *  Created on: Jan 28, 2016
 *      Author: tfy12jsg
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include <vector>
#include <vr/Vec3.h>
#include <vr/ObjLoader.h>
#include <GL/glew.h>

#include "Node.h"

class Geometry : public Node {

public:
	Geometry();

	void draw(GLuint program);

	void acceptVisitor(NodeVisitor *visitor);

	void setVertices(const vr::Vec3Vector& vertices);
	void setNormals(const vr::Vec3Vector& normals);
	void setIndices(const vr::UIntVector& indices);
	void setTexCoords(const vr::Vec2Vector& texCoords);

	void initBuffers();

	void setMaterial(const vr::material_t& material) {m_material = material;}
	vr::material_t getMaterial() {return m_material;}

	vr::Vec3Vector getVertices() {return m_vertices;}

protected:

private:
	vr::Vec3Vector m_vertices;
	vr::UIntVector m_indices;
	vr::Vec3Vector m_normals;
	vr::Vec2Vector m_texCoords;

	vr::material_t m_material;

	GLuint vao = 0;
	GLuint vertices_vbo = 0;
	GLuint indices_ebo = 0;
	GLuint normals_vbo = 0;
	GLuint texCoords_vbo = 0;


	GLuint vbo = 0;

};



#endif /* GEOMETRY_H_ */
