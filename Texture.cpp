/*
 * Texture.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: jimmy
 */

#include <GL/glew.h>
#include <vr/glUtils.h>
#include <vr/glErrorUtil.h>
#include <iostream>

#include "Texture.h"

Texture::Texture() {

}

Texture::Texture(string file) {
	m_idTexture = vr::createTexture(file);
}

Texture::Texture(GLuint textureID) {
	m_idTexture = textureID;
}

void Texture::loadFile(string file) {
	m_idTexture = vr::createTexture(file);
}

Texture::~Texture() {
	glDeleteTextures(1, &m_idTexture);
}

void Texture::apply(int textureUnit) {
	glActiveTexture(GL_TEXTURE0+textureUnit);
	glBindTexture(GL_TEXTURE_2D, m_idTexture);
	CheckErrorsGL("Apply texture");
}

