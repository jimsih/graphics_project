/*
 * State.h
 *
 *  Created on: Jan 28, 2016
 *      Author: tfy12jsg
 */

#ifndef STATE_H_
#define STATE_H_

#include <GL/glew.h>
#include <vector>
#include <memory>

#include "Material.h"
#include "Texture.h"

using namespace std;

class State {

public:

	State() : polygonMode(GL_FILL) {}
	State(Material* material, GLuint program) :
		m_material(material), m_program(program), polygonMode(GL_FILL) {}

	void apply();
	void apply(GLuint program);

	GLuint getProgram() {return m_program;}
	void setProgram(GLuint program) {m_program = program;}

	void setMaterial(Material* material);

	void setDiffuseTexture(Texture* texture);
	void setSpecularTexture(Texture* texture);

	void setPolygonMode(GLenum mode);

protected:

private:
	void bindState(GLuint program);

	Material* m_material = nullptr;
	Texture* m_diffuseTexture = nullptr;
	bool m_hasDiffuseTex = false;
	Texture* m_specularTexture = nullptr;
	bool m_hasSpecularTex = false;

	GLuint m_program = 0;
	GLenum polygonMode;

};



#endif /* STATE_H_ */
