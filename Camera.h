/*
 * Camera.h
 *
 *  Created on: Jan 21, 2016
 *      Author: tfy12jsg
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include <vr/Vec3.h>
#include <vr/Matrix.h>

#include "Globals.h"
#include "Node.h"
#include "Group.h"
#include "NodeVisitor.h"

class Camera : public Group {

public:

	Camera();
	Camera(vr::Vec3 pos, vr::Vec3 lookAt, vr::Vec3 up);
	virtual ~Camera();

	void acceptVisitor(NodeVisitor *visitor);
	void mouseInputFunction(int x, int y);

	void apply(GLuint program);

	void reset();

	void update();
	void translate(const vr::Vec3 &v);
	void rotate(float rad, vr::Vec3 &axis);
	void rotatex(float rad);
	void rotatey(float rad);

	void setPosition(vr::Vec3 pos);
	vr::Vec3 getPosition();
	void setLookAt(vr::Vec3 lookAt);

	vr::Matrix getProjection() {return m_projection;}
	vr::Matrix getView() {return m_view;}

	void set_top(float top);
	void set_near(float near);
	void set_far(float far);
	void set_fov(float fov);

	void usePerspectiveProj(bool b);
	void updateProjection();
	void setProjection(vr::Matrix projection);

private:

	vr::Vec3 m_startPosition;
	vr::Vec3 m_startOrientation;

	vr::Vec3 m_position;
	vr::Vec3 m_lookAt;
	vr::Vec3 m_up;

	float m_top = 1.0f;
	float m_near = 0.1f;
	float m_far = 500.0f;
	float m_fov = 45; //70*vr::PI/360;

	vr::Matrix m_projection;
	vr::Matrix m_view;
	bool isPerspective = true;

};

void cameraMouseTracker(shared_ptr<Node>& camera, int x, int y);
void cameraKeyboardInput(shared_ptr<Node>& camera, unsigned char key, int x, int y);
void cameraUpdate(shared_ptr<Node>& camera);

#endif /* CAMERA_H_ */

