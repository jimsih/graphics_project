/*
 * RenderToTexture.h
 *
 *  Created on: Feb 15, 2016
 *      Author: tfy12jsg
 */

#ifndef RENDERTOTEXTURE_H_
#define RENDERTOTEXTURE_H_

#include "Group.h"
#include "NodeVisitor.h"

class RenderToTexture : public Group {
public:
	RenderToTexture();
	virtual ~RenderToTexture();

	virtual void acceptVisitor(NodeVisitor *visitor) =0;
	virtual void apply() =0;
	Texture* getTexture();

protected:
	Texture* m_texture = nullptr;
};

#endif /* RENDERTOTEXTURE_H_ */
