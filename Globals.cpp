/*
 * Globals.cpp
 *
 *  Created on: Feb 10, 2016
 *      Author: tfy12jsg
 */

#include <iostream>

#include "Globals.h"

Globals* Globals::instance = nullptr;

Globals* Globals::getInstance() {
	if (!instance) {
		instance = new Globals();
	}
	return instance;
}

float Globals::Aspect() {return (float)WIDTH/HEIGHT;}

//GLuint Globals::getLightningProgram() {
//	return lightProgram;
//}
//
//void Globals::setLightningProgram(GLuint program) {
//	lightProgram = program;
//}
//
//GLuint Globals::getShadowMapProgram() {
//	return shadowMapProgram;
//}
//
//void Globals::setShadowMapProgram(GLuint program) {
//	shadowMapProgram = program;
//}
//
//GLuint Globals::getSimpleShaderProgram() {
//	return simpleShader;
//}
//
//void Globals::setSimpleShaderProgram(GLuint program) {
//	simpleShader = program;
//}

void Globals::addProgram(string name, GLuint program) {
	m_programs[name] = program;
}

GLuint Globals::getProgram(string name) {
	if (m_programs.find(name) == m_programs.end()) {
		std::cerr << "Program " << name << " does not exist" << std::endl;
		return 0;
	}
	return m_programs[name];
}
