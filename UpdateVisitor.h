/*
 * UpdateVisitor.h
 *
 *  Created on: Feb 8, 2016
 *      Author: jimmy
 */

#ifndef UPDATEVISITOR_H_
#define UPDATEVISITOR_H_

#include "NodeVisitor.h"

class UpdateVisitor : public NodeVisitor {
public:
	UpdateVisitor();
	virtual ~UpdateVisitor();

	void traverseScene();
	void visit(shared_ptr<Camera> camera);
	void visit(shared_ptr<Transform> transform);
	void visit(shared_ptr<Geometry> geometry);
	void visit(shared_ptr<RenderToTexture> render2Texture);
};

#endif /* UPDATEVISITOR_H_ */
