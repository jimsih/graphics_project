/*
 * NodeVisitor.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: jimmy
 */

#include <iostream>

#include "NodeVisitor.h"
#include "Node.h"
#include "Group.h"

void NodeVisitor::visit(shared_ptr<Group> group) {
	for (std::shared_ptr<Node> &node : group->getNodes()) {
		node->acceptVisitor(this);
	}
}

