#include "Transform.h"

#include <iostream>

Transform::Transform() {
	m_transform = vr::Matrix();
}

Transform::Transform(vr::Matrix transform) {
	m_transform = transform;
}

Transform::~Transform() {
	// TODO Auto-generated destructor stub
}

void Transform::acceptVisitor(NodeVisitor *visitor) {
	visitor->visit(std::dynamic_pointer_cast<Transform>(shared_from_this()));
}

void Transform::setTransform(vr::Matrix transform) {
	m_transform = transform;
}

vr::Matrix Transform::getTransform() {
	return m_transform;
}



