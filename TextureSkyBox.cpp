/*
 * TextureSkyBox.cpp
 *
 *  Created on: Feb 24, 2016
 *      Author: tfy12jsg
 */

#include <GL/glew.h>
#include <vr/glErrorUtil.h>
#include <vr/Image.h>

#include "TextureSkyBox.h"
#include "Reader.h"


TextureSkyBox::TextureSkyBox() {

}

TextureSkyBox::~TextureSkyBox() {

}

void TextureSkyBox::apply(int textureUnit) {
	glActiveTexture(GL_TEXTURE0+textureUnit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_idTexture);
	CheckErrorsGL("Apply texture");
}

void TextureSkyBox::loadCubemap(vector<string> faces) {
    GLuint textureID;
    glGenTextures(1, &textureID);
    glActiveTexture(GL_TEXTURE0);

    vr::Image* image;
    Reader reader;

    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
    for(GLuint i = 0; i < faces.size(); i++)
    {
    	image = reader.loadImage(faces[i]);
        glTexImage2D(
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
            GL_RGB, image->width(), image->height(), 0, GL_RGB, GL_UNSIGNED_BYTE, image->data()
        );
        delete image;
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    m_idTexture = textureID;
}
