/*
 * HUD.h
 *
 *  Created on: Mar 1, 2016
 *      Author: tfy12jsg
 */

#ifndef HUD_H_
#define HUD_H_

#include <memory>
#include <vector>
#include <utility>
#include <vr/Vec2.h>
#include <GL/glew.h>

#include "Texture.h"
#include "Geometry.h"

using namespace std;

class HUD {
public:
	HUD();
	virtual ~HUD();

	void addItem(vr::Vec2 position, float width, float height, Texture* texture);

	void draw();

private:
	vector<pair<shared_ptr<Geometry>, Texture*>> displays;
	GLuint hudProgram;

};

#endif /* HUD_H_ */
