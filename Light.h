/*
 * Light.h
 *
 *  Created on: Feb 2, 2016
 *      Author: tfy12jsg
 */

#ifndef LIGHT_H_
#define LIGHT_H_

#include <GL/glew.h>
#include <vr/Vec3.h>

#include "Node.h"

class Light : public Node {
public:

	 enum LIGHT_TYPE {
		DIRECTIONAL,
		POINT
	};

	Light();
	Light(LIGHT_TYPE type, vr::Vec3 position, vr::Vec3 ambient, vr::Vec3 diffuse, vr::Vec3 specular);

	virtual ~Light();

	void acceptVisitor(NodeVisitor *visitor);

	void apply(GLuint program, unsigned int lightNumber);
	void apply(GLuint program);

	vr::Vec3 getAmbient();
	void setAmbient(vr::Vec3 &ambient);

	vr::Vec3 getDiffuse();
	void setdiffuse(vr::Vec3 &diffuse);

	vr::Vec3 getSpecular();
	void setSpecular(vr::Vec3 &specular);

	vr::Vec3 getPosition();
	void setPosition(vr::Vec3 position);

	vr::Matrix getProjection();
	void setProjection(vr::Matrix projection);

	LIGHT_TYPE getType();

	void draw(GLuint program);
	void draw();

	void setActive(bool b);
	bool isActive();

protected:

private:
	void init();
	void applyPointLight(GLuint program, unsigned int lightNumber);
	void applyDirectionalLight(GLuint program, unsigned int lightNumber);

	bool m_active = true;

	vr::Matrix m_projection;

	LIGHT_TYPE m_type;
	vr::Vec3 m_position;
	vr::Vec3 m_ambient;
	vr::Vec3 m_diffuse;
	vr::Vec3 m_specular;

	float m_constant;
	float m_linear;
	float m_quadratic;

	shared_ptr<Geometry> m_geom;

};



#endif /* LIGHT_H_ */
