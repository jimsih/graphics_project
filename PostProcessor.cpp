/*
 * PostProcessor.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: tfy12jsg
 */

#include <iostream>

#include "PostProcessor.h"
#include "Scene.h"
#include "GeometryBuilder.h"

PostProcessor::PostProcessor(GLuint program) {
	m_program = program;

	GeometryBuilder gb;
	screenQuad = gb.buildQuad(1);
	glGenFramebuffers(1, &fbo);
	init();

}

PostProcessor::~PostProcessor() {
	// TODO Auto-generated destructor stub
}

void PostProcessor::cleanBuffers() {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glDeleteTextures(1, &texColor);

	glDeleteRenderbuffers(1, &rbo);
}

void PostProcessor::init() {
	cleanBuffers();

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	int width = Globals::getInstance()->WIDTH;
	int height = Globals::getInstance()->HEIGHT;
	glGenTextures(1, &texColor);
	glBindTexture(GL_TEXTURE_2D, texColor);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColor, 0);

	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PostProcessor::apply() {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void PostProcessor::draw() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(m_program);

	glUniform1i(glGetUniformLocation(m_program, "screenTexture"), 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texColor);

	screenQuad->draw(m_program);

	glUseProgram(0);
}

GLuint PostProcessor::getFramebuffer() {
	return fbo;
}

bool PostProcessor::isActive() {
	return m_active;
}

void PostProcessor::setActive(bool b) {
	m_active = b;
}
