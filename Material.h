
#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <vr/Vec3.h>
#include <vr/ObjLoader.h>
#include <GL/glew.h>

class Material {
public:
	Material();
	Material(vr::material_t material) : m_material(material) {};
	virtual ~Material();

	void apply(GLuint program);

	void setMaterial(const vr::material_t &material) {m_material = material;}
	vr::material_t getMaterial() {return m_material;}

	void setAmbient(vr::Vec3 &ambient) {m_material.ambient = ambient;}

	void setDiffuse(vr::Vec3 &diffuse) {m_material.diffuse = diffuse;}

	void setSpecular(vr::Vec3 &specular) {m_material.specular = specular;}

	void setShininess(float shininess) {m_material.shininess = shininess;}

protected:

private:
	vr::material_t m_material;
};



#endif /* MATERIAL_H_ */
