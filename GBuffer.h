/*
 * GBuffer.h
 *
 *  Created on: Mar 3, 2016
 *      Author: tfy12jsg
 */

#ifndef GBUFFER_H_
#define GBUFFER_H_

#include <GL/glew.h>
#include <memory>

#include "Geometry.h"

class GBuffer {
public:
	GBuffer();
	virtual ~GBuffer();

	 enum GBUFFER_TYPE {
		GBUFFER_POSITION,
		GBUFFER_NORMAL,
		GBUFFER_DIFFUSE,
		GBUFFER_SPECULAR,
		GBUFFER_AMBIENT
	};

	void apply();
	void draw();

	GLuint getBuffer(GBUFFER_TYPE buffer);

	void copyDepthBuffer(GLuint framebuffer);

	void init();


private:
	void cleanBuffers();

	GLuint gBuffer = 0;

	GLuint gPosition = 0;
	GLuint gNormal = 0;
	GLuint gDiffuse = 0;
	GLuint gSpecular = 0;
	GLuint gAmbient = 0;

	GLuint rboDepth = 0;

	shared_ptr<Geometry> screenQuad;
};

#endif /* GBUFFER_H_ */
