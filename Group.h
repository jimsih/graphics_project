/*
 * Group.h
 *
 *  Created on: Jan 28, 2016
 *      Author: tfy12jsg
 */

#ifndef GROUP_H_
#define GROUP_H_

#include <vector>
#include <memory>

#include "Node.h"

using namespace std;

class Group : public Node {

public:
	Group();
	Group(vector<shared_ptr<Node>> children);

	void acceptVisitor(NodeVisitor *visitor);

	void addNode(shared_ptr<Node> node);
	vector<shared_ptr<Node>> getNodes();
protected:
	vector<shared_ptr<Node>> m_children;
private:


};



#endif /* GROUP_H_ */
