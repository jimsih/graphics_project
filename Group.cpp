#include "Group.h"

Group::Group() {
	m_children = std::vector<shared_ptr<Node>>();
}

Group::Group(std::vector<shared_ptr<Node>> children) {
	m_children = children;
}

void Group::acceptVisitor(NodeVisitor *visitor) {
	visitor->visit(std::dynamic_pointer_cast<Group>(shared_from_this()));
}

void Group::addNode(shared_ptr<Node> node) {
	m_children.push_back(node);
}

std::vector<shared_ptr<Node>> Group::getNodes() {
	return m_children;
}



