/*
 * PostProcessor.h
 *
 *  Created on: Mar 9, 2016
 *      Author: tfy12jsg
 */

#ifndef POSTPROCESSOR_H_
#define POSTPROCESSOR_H_

#include <GL/glew.h>
#include <memory>

#include "Geometry.h"
#include "RenderToTexture.h"

class PostProcessor {
public:
	PostProcessor(GLuint program);
	virtual ~PostProcessor();

	void apply();
	void draw();

	GLuint getFramebuffer();

	void init();

	bool isActive();
	void setActive(bool b);

private:
	void cleanBuffers();

	GLuint fbo;

	GLuint texColor = 0;
	GLuint rbo = 0;

	GLuint m_program;

	shared_ptr<Geometry> screenQuad;

	bool m_active = false;
};

#endif /* POSTPROCESSOR_H_ */
