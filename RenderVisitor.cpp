/*
 * RenderVisitor.cpp
 *
 *  Created on: Feb 7, 2016
 *      Author: jimmy
 */

#include <vector>
#include <memory>
#include <iostream>
#include <vr/glErrorUtil.h>

#include "RenderVisitor.h"
#include "RenderToTexture.h"
#include "Node.h"
#include "Group.h"
#include "Transform.h"
#include "Camera.h"
#include "Geometry.h"
#include "Scene.h"
#include "Light.h"

using namespace std;

RenderVisitor::RenderVisitor() {

}

RenderVisitor::~RenderVisitor() {
	// TODO Auto-generated destructor stub
}

void RenderVisitor::traverseScene() {
	Scene *scene = Scene::getInstance();

	if (scene->isRenderFromLight()) {
		Light* light = (Light*)scene->getLightSources()[0].get();
		shared_ptr<Camera> camera(new Camera(light->getPosition(), vr::Vec3(0,0,0), vr::Vec3(0, 1.0f, 0)));
		camera->setProjection(light->getProjection());
		m_cameraStack.push(camera);
	} else {
		m_cameraStack.push(scene->getMainCamera());
	}

	//glCullFace(GL_BACK);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (shared_ptr<Node> &node : scene->getNodes()) {
		node->acceptVisitor(this);
	}

	glViewport(0, 0, Globals::getInstance()->WIDTH, Globals::getInstance()->HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderVisitor::visit(shared_ptr<Group> group) {
	if (group->hasState()) {
		m_stateStack.push(group->getState());
	}

	for (std::shared_ptr<Node> &node : group->getNodes()) {
		node->acceptVisitor(this);
	}

	if (group->hasState()) {
		m_stateStack.pop();
	}
}

void RenderVisitor::visit(shared_ptr<Camera> camera) {
	if (camera->hasState()) {
		m_stateStack.push(camera->getState());
	}

	m_cameraStack.push(camera);
	for (std::shared_ptr<Node> &node : camera->getNodes()) {
		node->acceptVisitor(this);
	}
	m_cameraStack.pop();

	if (camera->hasState()) {
		m_stateStack.pop();
	}
}

void RenderVisitor::visit(shared_ptr<Transform> transform) {
	if (transform->hasState()) {
		m_stateStack.push(transform->getState());
	}

	m_matrixStack.push(transform->getTransform());

	for (std::shared_ptr<Node> &node : transform->getNodes()) {
		node->acceptVisitor(this);
	}

	m_matrixStack.pop();

	if (transform->hasState()) {
		m_stateStack.pop();
	}
}

void RenderVisitor::visit(shared_ptr<Geometry> geometry) {
	if (geometry->hasState()) {
		m_stateStack.push(geometry->getState());
	}

	if (m_stateStack.size() == 0) {
		std::cout << "No state when drawing geometry!" << std::endl;
		return;
	}

	Scene* scene = Scene::getInstance();

	shared_ptr<State> state = m_stateStack.top();
	GLuint program = state->getProgram();
	if (scene->isGlobalProgram()) {
		program = scene->getGlobalProgram();
	}

	glUseProgram(program);

	state->apply(program);
	m_cameraStack.top()->apply(program);

	stack<vr::Matrix> matrixStack = m_matrixStack;
	vr::Matrix modelMatrix;

	if (matrixStack.size() > 0) {
		modelMatrix = matrixStack.top();
		matrixStack.pop();
		for (unsigned int i=0; i<matrixStack.size(); i++) {
			modelMatrix = matrixStack.top()*modelMatrix;
			matrixStack.pop();
		}
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "M"), 1, GL_FALSE, modelMatrix.ptr());

	if (scene->isRenderShadow()) {
		glUniform1i(glGetUniformLocation(program, "hasShadowMap"), true);
		vr::Matrix depthBiasMVP = modelMatrix * scene->getShadowBiasMatrix(0);
		glUniformMatrix4fv(glGetUniformLocation(program, "DepthBiasMVP"), 1, GL_FALSE, depthBiasMVP.ptr());
	} else {
		glUniform1i(glGetUniformLocation(program, "hasShadowMap"), false);
	}

	geometry->draw(program);

	glUseProgram(0);

	if (geometry->hasState()) {
		m_stateStack.pop();
	}
}

void RenderVisitor::visit(shared_ptr<RenderToTexture> render2Texture) {
	if (render2Texture->hasState()) {
		m_stateStack.push(render2Texture->getState());
	}

	render2Texture->apply();

	for (std::shared_ptr<Node> &node : render2Texture->getNodes()) {
		node->acceptVisitor(this);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (render2Texture->hasState()) {
		m_stateStack.pop();
	}
}

