/*
 * Globals.h
 *
 *  Created on: Jan 28, 2016
 *      Author: tfy12jsg
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <map>
#include <string>
#include <GL/glew.h>
#include <vr/Timer.h>

using namespace std;

class Globals {

public:

	static Globals* getInstance();

	bool DEBUG;

	int WIDTH = 800;
	int HEIGHT = 600;
	vr::Timer timer;

//	GLuint getLightningProgram();
//	void setLightningProgram(GLuint program);
//
//	GLuint getShadowMapProgram();
//	void setShadowMapProgram(GLuint program);
//
//	GLuint getSimpleShaderProgram();
//	void setSimpleShaderProgram(GLuint program);

	void addProgram(string name, GLuint program);
	GLuint getProgram(string name);

	float Aspect();

private:
	static Globals* instance;

	map<string, GLuint> m_programs;

	GLuint lightProgram = 0;
	GLuint shadowMapProgram = 0;
	GLuint simpleShader = 0;


};


#endif /* GLOBALS_H_ */
