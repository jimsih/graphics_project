/*
 * Light.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: tfy12jsg
 */

#include <string>
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <vr/Matrix.h>
#include <vr/glErrorUtil.h>

#include "Light.h"
#include "Scene.h"
#include "Reader.h"

Light::Light() {
	vr::Vec3Vector v;
	init();
}

Light::Light(LIGHT_TYPE type, vr::Vec3 position, vr::Vec3 ambient, vr::Vec3 diffuse, vr::Vec3 specular) {
	m_position = position, m_ambient = ambient, m_diffuse = diffuse, m_specular = specular;
	m_type = type;

	m_constant = 1.0f;
	m_linear = 0.07f;
	m_quadratic = 0.017f;

	init();
}

void Light::init() {
	Reader reader;

	m_projection = vr::Matrix::ortho(-20, 20, -20, 20, 0.2, 100);

	GeometrySharedPtrVector geometries;
	reader.loadObjFile("vrlib/models/sphere.obj", geometries);

	m_geom = geometries[0];

	CheckErrorsGL("Light init buffers");
	glBindVertexArray(0);
}


Light::~Light() {

}

void Light::acceptVisitor(NodeVisitor *visitor) {

}

void Light::apply(GLuint program, unsigned int lightNumber) {

	if (m_type == POINT) {
		applyPointLight(program, lightNumber);
	} else if (m_type == DIRECTIONAL) {
		applyDirectionalLight(program, lightNumber);
	}

	CheckErrorsGL("Light apply");
}

void Light::applyPointLight(GLuint program, unsigned int lightNumber) {
	std::string number = std::to_string(lightNumber);

	glUniform3fv(glGetUniformLocation(program, std::string("pointLights["+number+"].position").c_str()),
			1, m_position.ptr());

	glUniform3fv(glGetUniformLocation(program, std::string("pointLights["+number+"].ambient").c_str()),
			1, m_ambient.ptr());

	glUniform3fv(glGetUniformLocation(program, std::string("pointLights["+number+"].diffuse").c_str()),
			1, m_diffuse.ptr());

	glUniform3fv(glGetUniformLocation(program, std::string("pointLights["+number+"].specular").c_str()),
			1, m_specular.ptr());
	glUniform1f(glGetUniformLocation(program, std::string("pointLights["+number+"].constant").c_str()),
			m_constant);
	glUniform1f(glGetUniformLocation(program, std::string("pointLights["+number+"].linear").c_str()),
			m_linear);
	glUniform1f(glGetUniformLocation(program, std::string("pointLights["+number+"].quadratic").c_str()),
			m_quadratic);
}

void Light::applyDirectionalLight(GLuint program, unsigned int lightNumber) {
	std::string number = std::to_string(lightNumber);

	vr::Vec3 direction = m_position.normalized();
	glUniform3fv(glGetUniformLocation(program, std::string("directionalLights["+number+"].direction").c_str()),
			1, direction.ptr());

	glUniform3fv(glGetUniformLocation(program, std::string("directionalLights["+number+"].ambient").c_str()),
			1, m_ambient.ptr());

	glUniform3fv(glGetUniformLocation(program, std::string("directionalLights["+number+"].diffuse").c_str()),
			1, m_diffuse.ptr());

	glUniform3fv(glGetUniformLocation(program, std::string("directionalLights["+number+"].specular").c_str()),
			1, m_specular.ptr());
}

vr::Vec3 Light::getAmbient() {return m_ambient;}
void Light::setAmbient(vr::Vec3 &ambient) {m_ambient = ambient;}

vr::Vec3 Light::getDiffuse() {return m_diffuse;}
void Light::setdiffuse(vr::Vec3 &diffuse) {m_diffuse = diffuse;}

vr::Vec3 Light::getSpecular() {return m_specular;}
void Light::setSpecular(vr::Vec3 &specular) {m_specular = specular;}

vr::Vec3 Light::getPosition() {
	return m_position;
}
void Light::setPosition(vr::Vec3 position) {
	m_position = position;
}

vr::Matrix Light::getProjection() {return m_projection;}
void Light::setProjection(vr::Matrix projection) {m_projection = projection;}


void Light::draw(GLuint program) {
	glUseProgram(program);

	glUniform3fv(glGetUniformLocation(program, "color"),
			1, m_diffuse.ptr());

	Camera* camera = Scene::getInstance()->getMainCamera().get();
	vr::Matrix MVP = vr::Matrix::translate(m_position) * camera->getView() * camera->getProjection();

	glUniformMatrix4fv(glGetUniformLocation(program, "MVP"), 1, GL_FALSE, MVP.ptr());

	m_geom->draw(program);
	glUseProgram(0);

	CheckErrorsGL("Light draw");

}

void Light::draw() {
	draw(Globals::getInstance()->getProgram("lightsourceProgram"));
}

void Light::setActive(bool b) {
	m_active = b;
}

bool Light::isActive() {
	return m_active;
}

Light::LIGHT_TYPE Light::getType() {
	return m_type;
}

