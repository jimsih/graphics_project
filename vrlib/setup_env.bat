@echo off

REM Directory of where this bat file is
set CURR_DIR=%~d0%~p0

set VR_PATH=%CURR_DIR%

set AGX_FILE_PATH=%AGX_FILE_PATH%;%CURR_DIR%

set DEPENDENCY_PATH=%CURR_DIR%\dependencies
:: Setup visual studio path
call %CURR_DIR%\setup_vs.bat

:: Path to GLUT
set GLUT_ROOT_PATH=%DEPENDENCY_PATH%\glut
if NOT EXIST %GLUT_ROOT_PATH% (
  echo.
  echo *** Missing GLUT, have you unpacked win32_dependencies.zip into dependencies?
  echo.
  goto error
)

:: Path to GLEW
set GLEW_ROOT_PATH=%DEPENDENCY_PATH%\glew
if NOT EXIST %GLEW_ROOT_PATH% (
  echo.
  *** Missing GLEW, have you unpacked win32_dependencies.zip into dependencies?
  echo.
  goto error
)

set ZLIB_DIR=%DEPENDENCY_PATH%\

set PNG_DIR=%DEPENDENCY_PATH%\

:: Add to path so .dll files can be found
set PATH=%PATH%;%GLUT_ROOT_PATH%\bin;%GLEW_ROOT_PATH%\bin;%DEPENDENCY_PATH%\bin;%VR_PATH%\bin
set LIB=%LIB%;%CURR_DIR%\dependencies\lib
set INCLUDE=%INCLUDE%;%CURR_DIR%\dependencies\include

echo Found glew at: %GLEW_ROOT_PATH%
echo Found glut at: %GLUT_ROOT_PATH%

:error
exit /B 1

:exit
exit /B 0