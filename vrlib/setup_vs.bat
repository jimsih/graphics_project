@echo off

REM Setup the MSDEV environment

:: VS2013
set VC_BAT="%VS120COMNTOOLS%vsvars32.bat"
IF EXIST %VC_BAT% (
  GOTO done
)

:: VS2012
set VC_BAT="%VS110COMNTOOLS%vsvars32.bat"
IF EXIST %VC_BAT% (
  GOTO done
)


:: VS2005
set VC_BAT="%VS80COMNTOOLS%vsvars32.bat"
IF EXIST %VC_BAT% (
  GOTO done
)

:: VS2010
set VC_BAT="%VS100COMNTOOLS%vsvars32.bat"
IF EXIST %VC_BAT% (
  GOTO done
)


:: VS2008
set VC_BAT="%VS90COMNTOOLS%vsvars32.bat"
IF EXIST %VC_BAT% (
  GOTO done
)




IF NOT EXIST %VC_BAT%  (
  echo. ***
  echo. "*** Unable to find a Microsoft Developer environment "VS2005, VS2008 or VS2010"
  echo. *** Compiling and linking your own applications is not possible.
  echo. *** %VC_BAT% does not exist. 
  echo. *** Edit this script to reflect the path of the above file
  echo. 
  goto exit
)

:done
call %VC_BAT%

:exit