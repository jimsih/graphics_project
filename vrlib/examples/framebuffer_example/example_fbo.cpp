/**
This example demonstrates how to render a scene into a texture, apply an image filter on that texture
Then render the filtered texture onto a screen aligned quad.
*/


#ifndef _WIN32
/*
Workaround for some kind of linker bug. See https://bugs.launchpad.net/ubuntu/+source/nvidia-graphics-drivers-319/+bug/1248642
*/
#include <pthread.h>
void junk() {
  int i;
  i=pthread_getconcurrency();
};
#endif


#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <vr/shaderUtils.h>

#include <vr/FrameBufferObject.h>
#include <vr/RenderBuffer.h>
#include <vr/glErrorUtil.h>
#include <vr/shaderUtils.h>

// forward declarations
class HelloGPGPU;
void reshape(int w, int h);

// globals
HelloGPGPU  *g_pHello;

// This shader performs a 9-tap Laplacian edge detection filter.
// (converted from the separate "edges.cg" file to embedded GLSL string)
static const char *edgeFragSource = STRINGIFY(
  uniform sampler2D texUnit;
  void main(void)
  {
     const float offset = 1.0 / 512.0;
     vec2 texCoord = gl_TexCoord[0].xy;
     vec4 c  = texture2D(texUnit, texCoord);
     vec4 bl = texture2D(texUnit, texCoord + vec2(-offset, -offset));
     vec4 l  = texture2D(texUnit, texCoord + vec2(-offset,     0.0));
     vec4 tl = texture2D(texUnit, texCoord + vec2(-offset,  offset));
     vec4 t  = texture2D(texUnit, texCoord + vec2(    0.0,  offset));
     vec4 ur = texture2D(texUnit, texCoord + vec2( offset,  offset));
     vec4 r  = texture2D(texUnit, texCoord + vec2( offset,     0.0));
     vec4 br = texture2D(texUnit, texCoord + vec2( offset,  offset));
     vec4 b  = texture2D(texUnit, texCoord + vec2(    0.0, -offset));
     gl_FragColor = 8.0 * (c + -0.125 * (bl + l + tl + t + ur + r + br + b));
  } 
  );


// This class encapsulates all of the GPGPU functionality of the example.
class HelloGPGPU
{
public: // methods
  HelloGPGPU(int w, int h)
    : _rAngle(0),
    _iWidth(w),
    _iHeight(h),
    _fbo(),// create a new frame buffer object
    _rb()  // optional: create a new render buffer object
  {
    // Create a simple 2D texture.
    // This example uses render to texture  by using the new Framebuffer Objects.
    //At the time of writing (1 july 2005) these are only implemented
    //in NVIDIA drivers for windows/linux.(I don't know about other vendors)
    //glew version 1.31 or newer is required.

    // GPGPU CONCEPT 1: Texture = Array.
    // Textures are the GPGPU equivalent of arrays in standard
    // computation. Here we allocate a texture large enough to fit our
    // data (which is arbitrary in this example).

    // OpenGL Good Programming Practice : Check errors
    // - OpenGL errors accumulate silently until checked.
    //   This utility checks the accumulated errors and reports
    //   them (to cerr by default). The error checking disappears
    //   with release builds by defining NDEBUG.
    CheckErrorsGL("BEGIN : Creating textures");
    glGenTextures(2, _iTexture); // create (reference to) a new texture

    // initialize the two textures
    // The first is used as rendering target for the sample scene
    //This one is used as reading source for the filter
    //The destination of the filter is the second texture
    //This second texture is the source for the final rendering display to the screen
    for(int i=0;i<2;i++)
    {
      glBindTexture(GL_TEXTURE_2D, _iTexture[i]);
      // (set texture parameters here)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
      //create the texture
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _iWidth, _iHeight,
        0, GL_RGB, GL_FLOAT, 0);
    }
    CheckErrorsGL("END : Creating textures");

    _fbo.Bind(); // Bind framebuffer object.
    CheckErrorsGL("BEGIN : Configuring FBO");

    // Attach texture to framebuffer color buffer
    _fbo.AttachTexture(GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, _iTexture[0]);
    _fbo.AttachTexture(GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, _iTexture[1]);

    // Optional: initialize depth renderbuffer
    //_rb.Set( GL_DEPTH_COMPONENT24, _iWidth, _iHeight );
    //_fbo.AttachRenderBuffer( GL_DEPTH_ATTACHMENT_EXT, _rb.GetId() );

    // Validate the FBO after attaching textures and render buffers
    _fbo.IsValid();

    // Disable FBO rendering for now...
    FramebufferObject::Disable();
    CheckErrorsGL("END : Configuring FBO");

    // GPGPU CONCEPT 2: Fragment Program = Computational Kernel.
    // A fragment program can be thought of as a small computational
    // kernel that is applied in parallel to many fragments
    // simultaneously.  Here we load a kernel that performs an edge
    // detection filter on an image.
    _programObject = glCreateProgramObjectARB();
    CheckErrorsGL("BEGIN : Configuring Shader");

    // Create the edge detection fragment program
    _fragmentShader = vr::loadShaderFromString(edgeFragSource,GL_FRAGMENT_SHADER);
    glAttachObjectARB(_programObject, _fragmentShader);

    // Link the shader into a complete GLSL program.
    glLinkProgramARB(_programObject);
    if (!vr::checkLinkStatus(_programObject))
    {
      fprintf(stderr, "Filter shader could not be linked\n");
      exit(1);
    }

    // Get location of the sampler uniform
    _texUnit = glGetUniformLocationARB(_programObject, "texUnit");
    CheckErrorsGL("END : Configuring Shader");
  }

  ~HelloGPGPU()
  {
  }

  // This method updates the texture by rendering the geometry (a teapot
  // and 3 rotating tori) and copying the image to a texture.
  // It then renders a second pass using the texture as input to an edge
  // detection filter.  It copies the results of the filter to the texture.
  // The texture is used in HelloGPGPU::display() for displaying the
  // results.
  void update()
  {
    CheckErrorsGL("BEGIN : HelloGPGPU::update()");
    glGetIntegerv(GL_DRAW_BUFFER, &_currentDrawbuf); // Save the current Draw buffer
    _fbo.Bind(); 									 // Render to the FBO
    glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);			 // Draw into the first texture
    _rAngle += 0.5f;
    CheckErrorsGL("HelloGPGPU::update() : After FBO setup");

    // store the window viewport dimensions so we can reset them,
    // and set the viewport to the dimensions of our texture
    int vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);

    // GPGPU CONCEPT 3a: One-to-one Pixel to Texel Mapping: A Data-
    //                   Dimensioned Viewport.
    // We need a one-to-one mapping of pixels to texels in order to
    // ensure every element of our texture is processed. By setting our
    // viewport to the dimensions of our destination texture and drawing
    // a screen-sized quad (see below), we ensure that every pixel of our
    // texel is generated and processed in the fragment program.
    glViewport(0, 0, _iWidth, _iHeight);

    // Render a teapot and 3 tori
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glRotatef(-_rAngle, 0, 1, 0.25);
    glutSolidTeapot(0.5);
    glPopMatrix();
    glPushMatrix();
    glRotatef(2.1 * _rAngle, 1, 0.5, 0);
    glutSolidTorus(0.05, 0.9, 64, 64);
    glPopMatrix();
    glPushMatrix();
    glRotatef(-1.5 * _rAngle, 0, 1, 0.5);
    glutSolidTorus(0.05, 0.9, 64, 64);
    glPopMatrix();
    glPushMatrix();
    glRotatef(1.78 * _rAngle, 0.5, 0, 1);
    glutSolidTorus(0.05, 0.9, 64, 64);
    glPopMatrix();
    CheckErrorsGL("HelloGPGPU::update() : After first render pass");

    glDrawBuffer(GL_COLOR_ATTACHMENT1_EXT);//Draw into the second texture
    // read from the first texture
    glBindTexture(GL_TEXTURE_2D, _iTexture[0]);
    //No need for copy glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, _iWidth, _iHeight);

    // run the edge detection filter over the geometry texture
    // Activate the edge detection filter program
    glUseProgramObjectARB(_programObject);

    // identify the bound texture unit as input to the filter
    glUniform1iARB(_texUnit, 0);

    // GPGPU CONCEPT 4: Viewport-Sized Quad = Data Stream Generator.
    // In order to execute fragment programs, we need to generate pixels.
    // Drawing a quad the size of our viewport (see above) generates a
    // fragment for every pixel of our destination texture. Each fragment
    // is processed identically by the fragment program. Notice that in
    // the reshape() function, below, we have set the frustum to
    // orthographic, and the frustum dimensions to [-1,1].  Thus, our
    // viewport-sized quad vertices are at [-1,-1], [1,-1], [1,1], and
    // [-1,1]: the corners of the viewport.
    glBegin(GL_QUADS);
    {
      glTexCoord2f(0, 0); glVertex3f(-1, -1, -0.5f);
      glTexCoord2f(1, 0); glVertex3f( 1, -1, -0.5f);
      glTexCoord2f(1, 1); glVertex3f( 1,  1, -0.5f);
      glTexCoord2f(0, 1); glVertex3f(-1,  1, -0.5f);
    }
    glEnd();
    CheckErrorsGL("HelloGPGPU::update() : After second render pass");

    // disable the filter
    glUseProgramObjectARB(0);

    // GPGPU CONCEPT 5: Copy To Texture (CTT) = Feedback.
    // We have just invoked our computation (edge detection) by applying
    // a fragment program to a viewport-sized quad. The results are now
    // in the frame buffer. To store them, we copy the data from the
    // frame buffer to a texture.  This can then be fed back as input
    // for display (in this case) or more computation (see
    // more advanced samples.)

    // update the texture again, this time with the filtered scene
    //glBindTexture(GL_TEXTURE_2D, _iTexture[0]);
    //glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, _iWidth, _iHeight);

    // restore the stored viewport dimensions
    glViewport(vp[0], vp[1], vp[2], vp[3]);
    CheckErrorsGL("END : HelloGPGPU::update()");
  }

  void display()
  {
    CheckErrorsGL("BEGIN : HelloGPGPU::display()");

    // Disable FBO rendering; Render to the window
    FramebufferObject::Disable();
    glDrawBuffer(_currentDrawbuf);

    // Bind the filtered texture
    glBindTexture(GL_TEXTURE_2D, _iTexture[1]);//read from the second texture
    glEnable(GL_TEXTURE_2D);
    CheckErrorsGL("HelloGPGPU::display() : After FBO/Texture setup");

    // Render a full-screen quad textured with the results of our
    // computation.  Note that this is not part of the computation: this
    // is only the visualization of the results.
    glBegin(GL_QUADS);
    {
      glTexCoord2f(0, 0); glVertex3f(-1, -1, -0.5f);
      glTexCoord2f(1, 0); glVertex3f( 1, -1, -0.5f);
      glTexCoord2f(1, 1); glVertex3f( 1,  1, -0.5f);
      glTexCoord2f(0, 1); glVertex3f(-1,  1, -0.5f);
    }
    glEnd();
    CheckErrorsGL("HelloGPGPU::display() : After render pass");

    glDisable(GL_TEXTURE_2D);
    CheckErrorsGL("END : HelloGPGPU::display()");
  }

protected: // data
  int           _iWidth, _iHeight; // The dimensions of our array
  float         _rAngle;           // used for animation

  GLuint			   _iTexture[2]; // The texture used as a data array
  FramebufferObject  _fbo;		 // The framebuffer object used for rendering to the texture
  Renderbuffer	   _rb;	         // Optional: The renderbuffer object used for depth

  GLhandleARB   _programObject;    // the program used to update
  GLhandleARB   _fragmentShader;

  GLint         _texUnit;          // a parameter to the fragment program
  GLint		  _currentDrawbuf;
};

// GLUT idle function
void idle()
{
  glutPostRedisplay();
}

// GLUT display function
void display()
{
  g_pHello->update();  // update the scene and run the edge detect filter
  g_pHello->display(); // display the results
  glutSwapBuffers();
}

// GLUT reshape function
void reshape(int w, int h)
{
  if (h == 0) h = 1;

  glViewport(0, 0, w, h);

  // GPGPU CONCEPT 3b: One-to-one Pixel to Texel Mapping: An Orthographic
  //                   Projection.
  // This code sets the projection matrix to orthographic with a range of
  // [-1,1] in the X and Y dimensions. This allows a trivial mapping of
  // pixels to texels.
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(-1, 1, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

// Called at startup
void initialize()
{
  // Initialize the "OpenGL Extension Wrangler" library
  glewInit();

  // Ensure we have the necessary OpenGL Shading Language extensions.
  if (glewGetExtension("GL_ARB_fragment_shader")      != GL_TRUE ||
    glewGetExtension("GL_ARB_vertex_shader")        != GL_TRUE ||
    glewGetExtension("GL_ARB_shader_objects")       != GL_TRUE ||
    glewGetExtension("GL_ARB_shading_language_100") != GL_TRUE)
  {
    fprintf(stderr, "Driver does not support OpenGL Shading Language\n");
    exit(1);
  }

  // Create the example object
  g_pHello = new HelloGPGPU(512, 512);
}

void keyboard(unsigned char key, int x, int y)
{
  switch(key) {
  case 27:
    exit(1);
    break;
  }
}

// The main function
int main(int argc, char **argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowSize(512, 512);
  glutCreateWindow("Hello, GPGPU! (GLSL version)");
  glutKeyboardFunc(keyboard);
  glutIdleFunc(idle);
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);

  initialize();

  glutMainLoop();
  return 0;
}

