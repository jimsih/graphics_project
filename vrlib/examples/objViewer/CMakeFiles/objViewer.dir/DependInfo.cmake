# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tfy12/tfy12jsg/Documents/vrlib/examples/BallController.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/examples/objViewer/CMakeFiles/objViewer.dir/__/BallController.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/examples/Geometry.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/examples/objViewer/CMakeFiles/objViewer.dir/__/Geometry.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/examples/objViewer/objViewer.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/examples/objViewer/CMakeFiles/objViewer.dir/objViewer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "HAVE_FREETYPE"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/usr/include/freetype2"
  "/usr/include/freetype2/freetype2"
  "examples/objViewer/.."
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
