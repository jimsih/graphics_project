// Always include windows.h BEFORE GLUT

#include <GL/glew.h>

#ifdef _WIN32
#define  _CRT_SECURE_NO_WARNINGS 1

#include <windows.h>
#endif
#include <GL/glut.h>

#include <vr/Matrix.h>
#include <vr/Vec3.h>

#include <iostream>
#include <list>
#include <numeric>

#include <vr/FileSystem.h>

#include <vr/ObjLoader.h>
#include <vr/glErrorUtil.h>
#include "BallController.h"
#include <vr/Timer.h>
#include <vr/DrawText.h>
#include <sstream>

#include "Geometry.h"
#include "Scene.h"

Scene<void *> g_scene;


void
idle()
{
	// update data
	glutPostRedisplay();
}

vr::Vec4Vector initLight()
{

	vr::Vec4Vector lights;

	const GLfloat light_ambient[] = { 0.1f, 0.1f, 0.1f, 1.0f };
	const GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float maxDistance = g_scene.getMaxDistance();
	vr::Vec4 light_position = vr::Vec4(g_scene.getCenter() + vr::Vec3(maxDistance, maxDistance, maxDistance), 1);
	lights.push_back(light_position);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	//glEnable(GL_COLOR_MATERIAL);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position.ptr());

	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
	light_position *= -1;
	light_position.w() = 1;
	glLightfv(GL_LIGHT1, GL_POSITION, (light_position).ptr());
	lights.push_back(light_position);



	GLfloat globalAmbient[] = { 0.1, 0.1, 0.1 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbient);

	return lights;
}

void
initDefaultMaterial()
{
	const GLfloat mat_ambient[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	const GLfloat mat_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat high_shininess[] = { 100.0f };


	// Default material properties
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
}


void drawLights(const vr::Vec4Vector& lightPos)
{
	if (!g_scene.drawLights)
		return;

	glDisable(GL_LIGHTING);
	glColor3f(1.0, 1.0, 0.0);
	vr::Vec4Vector::const_iterator it = lightPos.begin();
	for (; it != lightPos.end(); ++it)
	{
		vr::Matrix m;
		m.setTrans(it->x(), it->y(), it->z());
		glPushMatrix();
		glMultMatrixf(m.ptr());
		glutSolidSphere(g_scene.getMaxDistance()*0.01, 10, 10);
		glPopMatrix();
	}
	glEnable(GL_LIGHTING);
}

void
display()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	vr::Vec4Vector lightPos = initLight();

	initDefaultMaterial();

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMultMatrixf(g_scene.modelView.ptr());

	// Control the camera
	g_scene.ballController.applyTransform();

	drawLights( lightPos );

	// Draw the FPS count
	g_scene.drawFPS();

	// Good test model!
	//glutSolidTeapot(1.0);

	g_scene.update();

	// Draw the origo
	g_scene.drawOrigo();


	// Ok we are done
	//glFinish();
	glutSwapBuffers();
	glutIdleFunc(idle);
};


// We will come here if the window is resized
void reshape(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	float ratio = 1.0* w / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	g_scene.ballController.clientAreaResize(w, h);

	// Set the correct perspective.
	float farDistance = g_scene.getMaxDistance();
	gluPerspective(45, ratio, 0.1, farDistance * 8);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}




// Get here if we move the mouse
void
motion(int x, int y)
{
	g_scene.ballController.mouseMove(x, y);
}


// Get here if we press a mouse button
void
mouse(int button, int state, int x, int y)
{
	int modifiers = glutGetModifiers();
	bool shiftDown = (modifiers & GLUT_ACTIVE_SHIFT) != 0;
	bool ctrlDown = (modifiers & GLUT_ACTIVE_CTRL) != 0;

	switch (button) {

		// Rotate the scene
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN) {

			if (ctrlDown)
				g_scene.ballController.mouseDown(x, y, BallController::MIDDLE);
			else if (shiftDown)
				g_scene.ballController.mouseDown(x, y, BallController::RIGHT);
			else
				g_scene.ballController.mouseDown(x, y, BallController::LEFT);
		}
		else
		{
			if (ctrlDown)
				g_scene.ballController.mouseUp(x, y, BallController::MIDDLE);
			else if (shiftDown)
				g_scene.ballController.mouseUp(x, y, BallController::RIGHT);
			else
				g_scene.ballController.mouseUp(x, y, BallController::LEFT);
		}

		break;

		// Pan & Zoom
	case GLUT_MIDDLE_BUTTON:

		if (shiftDown)
		{
			if (state == GLUT_DOWN)
				g_scene.ballController.mouseDown(x, y, BallController::RIGHT);
			else
				g_scene.ballController.mouseUp(x, y, BallController::RIGHT);
			break;
		}
		else
		{
			if (state == GLUT_DOWN)
				g_scene.ballController.mouseDown(x, y, BallController::MIDDLE);
			else
				g_scene.ballController.mouseUp(x, y, BallController::MIDDLE);
			break;
		}
	default:
		break;
	}
}


void loadScene();


// Get here if a keyboard key is pressed
void
keyboard(unsigned char c, int /*x*/, int /*y*/)
{
	g_scene.ballController.key(c);

	switch (c) {

		// Reload the scene
	case 'r':
		loadScene();
		break;
		// Quit
	case 'Q':
	case 'q':
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}


// Get here if a special key is pressed
void
special(int c, int /*x*/, int /*y*/)
{
	g_scene.ballController.key(c);

	switch (c) {
	case GLUT_KEY_F1:
		std::cout << "F1" << std::endl;
		break;
	case GLUT_KEY_F2:
		std::cout << "F2" << std::endl;
		break;
	case GLUT_KEY_HOME:
		std::cout << "Home" << std::endl;
		break;
	case GLUT_KEY_END:
		std::cout << "End" << std::endl;
		break;
	case GLUT_KEY_UP:
		std::cout << "Up" << std::endl;
		break;
	case GLUT_KEY_DOWN:
		std::cout << "Down" << std::endl;
		break;
	default:
		break;
	}
}


// Available menu actions
enum MenuItems
{
	TOGGLE_NORMALS,
	TOGGLE_DRAW_ORIGO,
	RESET_CAMERA,
	RELOAD_SCENE,
	TOGGLE_USE_ORIGO_AS_ROTATION_CENTER,
	TOGGLE_DRAW_FPS,
	TOGGLE_DRAW_LIGHTS
};


// Come here if a menu item is selected
// val will have one of the values from the MenuItems enum
void menuCallback(int val)
{

	if (val == TOGGLE_NORMALS)
	{

		bool f = g_scene.getEnableDrawNormals();
		g_scene.setEnableDrawNormals(!f);
	}

	if (val == TOGGLE_DRAW_ORIGO)
	{
		bool f = g_scene.shouldDrawOrigo;
		g_scene.setShouldDrawOrigo(!f);
	}

	if (val == TOGGLE_DRAW_FPS)
	{
		bool f = g_scene.shouldDrawFPS;
		g_scene.setShouldDrawFPS(!f);
	}


	if (val == RESET_CAMERA)
	{
		vr::Vec3 center = g_scene.getCenter();
		g_scene.modelView.makeLookAt(vr::Vec3(center[0], center[1] - g_scene.getMaxDistance() * 2, center[2]), center, vr::Vec3(0, 0, 1));
		g_scene.ballController.home();
	}

	if (val == RELOAD_SCENE)
	{
		loadScene();
	}

	if (val == TOGGLE_DRAW_LIGHTS)
		g_scene.drawLights = !g_scene.drawLights;

	if (val == TOGGLE_USE_ORIGO_AS_ROTATION_CENTER)
	{
		vr::Vec3 center = g_scene.getCenter();
		vr::Vec3 homeCenter = g_scene.ballController.getCenter();

		if (center == homeCenter)
		{
			g_scene.ballController.setHome(vr::Vec3(), vr::Vec3(), vr::Quat(vr::Vec3::X_AXIS(), vr::Vec3::X_AXIS()));
		}
		else
			g_scene.ballController.setHome(vr::Vec3(), center, vr::Quat(vr::Vec3::X_AXIS(), vr::Vec3::X_AXIS()));

	}
}


typedef std::vector<std::string> StringVector;
StringVector loadableFiles;

int
main(int argc, char** argv)
{

	if (argc > 1)
	{
		for (int i = 1; i < argc; i++)
			loadableFiles.push_back(argv[i]);
	}
	else
	{
		loadableFiles.push_back("/models/5426_C3PO_Robot_Star_Wars.obj");
			//loadableFiles.push_back(std::string(vrPath) + "/models/sphere.obj");
	}
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);

	glutInitWindowSize(1280,720);
	glutInitWindowPosition(10, 10);
	glutCreateWindow("GLUT Viewer");

	glutDisplayFunc(display);

	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutReshapeFunc(reshape);

	glutCreateMenu(menuCallback);
	glutAddMenuEntry("Toggle normals", TOGGLE_NORMALS);
	glutAddMenuEntry("Toggle origo", TOGGLE_DRAW_ORIGO);
	glutAddMenuEntry("Toggle FPS", TOGGLE_DRAW_FPS);
	glutAddMenuEntry("Reset camera", RESET_CAMERA);
	glutAddMenuEntry("Reload scene", RELOAD_SCENE);
	glutAddMenuEntry("Toggle origo as rotation center", TOGGLE_USE_ORIGO_AS_ROTATION_CENTER);
	glutAddMenuEntry("Toggle draw light", TOGGLE_DRAW_LIGHTS);

	glutAttachMenu(GLUT_RIGHT_BUTTON);

	glewInit();

	// Ensure we have the necessary OpenGL Shading Language extensions.
	if (glewGetExtension("GL_ARB_fragment_shader") != GL_TRUE ||
		glewGetExtension("GL_ARB_vertex_shader") != GL_TRUE ||
		glewGetExtension("GL_ARB_shader_objects") != GL_TRUE ||
		glewGetExtension("GL_ARB_shading_language_100") != GL_TRUE)
	{
		std::cerr << "Driver does not support OpenGL Shading Language" << std::endl;
		exit(1);
	}


	loadScene();

	vr::Vec3 center = g_scene.getCenter();

	g_scene.modelView.makeLookAt(vr::Vec3(center[0], center[1] - g_scene.getMaxDistance() * 2, center[2]), center, vr::Vec3(0, 0, 1));


	g_scene.ballController.setDrawConstraints(true);
	g_scene.ballController.setHome(vr::Vec3(), center, vr::Quat(vr::Vec3::X_AXIS(), vr::Vec3::X_AXIS()));
	g_scene.ballController.home();


	// Start the app
	glutMainLoop();
}



// Load the files into the scene
void loadScene()
{
	g_scene.init();

	for (StringVector::const_iterator it = loadableFiles.begin(); it != loadableFiles.end(); ++it)
	{
		size_t numRead = 0;
		std::string path = vr::FileSystem::convertToNativeFilePath(*it);
		std::cerr << "Loading \'" << path << "\'" << std::endl;
		GeometrySharedPtrVector geometries;
		numRead = Geometry::loadObjFile(path, geometries);
		if (!numRead)
		{
			std::cerr << "Error loading model: " << path << std::endl;
		}
		else
		{
			std::shared_ptr<Node> node(new Node(geometries));
			g_scene.add(node);
		}
	}

	g_scene.ballController.setTranslateScale(g_scene.getMaxDistance()*0.3);
}
