/*
  Multi-texture example.

  Anders Backman, Ume� University

*/




#ifndef _WIN32
/*
Workaround for some kind of linker bug. See https://bugs.launchpad.net/ubuntu/+source/nvidia-graphics-drivers-319/+bug/1248642
*/
#include <pthread.h>
void junk() {
  int i;
  i=pthread_getconcurrency();
};
#endif


#include <stdlib.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <string>
#include <vector>
#include <vr/BMPReader.h>
#include <vr/Vec4.h>
#include <vr/glErrorUtil.h>
#include <vr/shaderUtils.h>

GLuint g_texture0;
GLuint g_texture1;
GLuint g_positionBufferObject;
GLuint g_texcoordBufferObject;
GLuint g_texcoordBufferObject2;

GLuint g_vShader;
GLuint g_fShader;
GLuint g_program;
GLuint g_texture0Uniform;
GLuint g_texture1Uniform;
GLuint g_sinUniform;


void createBoxBuffers()
{
  std::vector<vr::Vec3> vertexPositions;
  vertexPositions.push_back( vr::Vec3(-0.5,-0.5, -0.5) );
  vertexPositions.push_back( vr::Vec3(0.5,-0.5, -0.5) );
  vertexPositions.push_back( vr::Vec3(0.5,0.5, -0.5) );
  vertexPositions.push_back( vr::Vec3(-0.5,0.5, -0.5) );

  std::vector<vr::Vec2> texCoords;
  texCoords.push_back( vr::Vec2(0,0) );
  texCoords.push_back( vr::Vec2(1,0) );
  texCoords.push_back( vr::Vec2(1,1) );
  texCoords.push_back( vr::Vec2(1,0) );

  // Generate buffer object for the vertex positions
  glGenBuffers(1, &g_positionBufferObject);

  // Make this buffer active
  glBindBuffer(GL_ARRAY_BUFFER, g_positionBufferObject);
  // Copy the data to OpenGL
  glBufferData(GL_ARRAY_BUFFER, sizeof(vr::Vec3::value_type)*3*vertexPositions.size(), &vertexPositions[0], GL_STATIC_DRAW);
  DO_GL_AND_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));

  // Generate buffer object for the texture coordinates
  glGenBuffers(1, &g_texcoordBufferObject);

  glBindBuffer(GL_ARRAY_BUFFER, g_texcoordBufferObject);
  
  // Copy the data to OpenGL
  glBufferData(GL_ARRAY_BUFFER, sizeof(vr::Vec2::value_type)*2*texCoords.size(), &texCoords[0], GL_STATIC_DRAW);


  // Generate buffer object for the texture coordinates
  glGenBuffers(1, &g_texcoordBufferObject2);

  glBindBuffer(GL_ARRAY_BUFFER, g_texcoordBufferObject2);

  // Copy the data to OpenGL
  glBufferData(GL_ARRAY_BUFFER, sizeof(vr::Vec2::value_type)*3*texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
  DO_GL_AND_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

// Function that loads a bmp file and creates and returns a 2d texture
GLint createTexture(const std::string& bmp_image)
{
  GLuint id;

  vr::BMPReader reader;
  vr::Image *image = reader.readImage(bmp_image);
  if (!image){
    std::cerr << "Unable to load image: " << bmp_image << std::endl;
    return 0;
  }


  // allocate a texture name
  glGenTextures(1, &id);

  // select the current texture
  glBindTexture(GL_TEXTURE_2D, id);

  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);


  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
    GL_LINEAR_MIPMAP_NEAREST);

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
    GL_REPEAT);
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
    GL_REPEAT);

  // build our texture mipmaps
  gluBuild2DMipmaps( GL_TEXTURE_2D, 3, image->width(), image->height(),
    GL_RGB, GL_UNSIGNED_BYTE, image->data());

  glBindTexture(GL_TEXTURE_2D, 0);

  return id;

}

float g_n=0;
  void
drawBox(void)
{
  glDisable(GL_LIGHTING);

  // First enable our shader program
  DO_GL_AND_CHECK(glUseProgramObjectARB(g_program));

  glUniform1i(g_texture0Uniform, 0);
  glUniform1i(g_texture1Uniform, 1);

  // Update an uniform with the calculated value
  g_n+=0.01;
  float f = (1+sinf(g_n))*0.5;
  glUniform1fARB(g_sinUniform, f); // A float value between 0 and 1

  // Vertex array object
  DO_GL_AND_CHECK(glEnableClientState(GL_VERTEX_ARRAY));
  DO_GL_AND_CHECK(glBindBuffer(GL_ARRAY_BUFFER, g_positionBufferObject));
  DO_GL_AND_CHECK(glVertexPointer(3, GL_FLOAT,0,0));


  // Texture coordinates for unit0
  glClientActiveTexture(GL_TEXTURE0);
  DO_GL_AND_CHECK(glEnableClientState(GL_TEXTURE_COORD_ARRAY));
  DO_GL_AND_CHECK(glBindBuffer(GL_ARRAY_BUFFER, g_texcoordBufferObject));
  DO_GL_AND_CHECK(glTexCoordPointer(2,GL_FLOAT,0,0));


  // Texture coordinates for unit1, the call to glBindBuffer could be commented away, then we use the same 
  // coordinates as for unit0
  glClientActiveTexture(GL_TEXTURE1);
  DO_GL_AND_CHECK(glEnableClientState(GL_TEXTURE_COORD_ARRAY));
  DO_GL_AND_CHECK(glBindBuffer(GL_ARRAY_BUFFER, g_texcoordBufferObject2)); // Could be commented away
  DO_GL_AND_CHECK(glTexCoordPointer(2,GL_FLOAT,0,0));


  // Now texture unit 1
  // 1: First Activate texture unit 1
  glActiveTextureARB(GL_TEXTURE1);

  // 2: THEN enable 2D texturing 
  glEnable(GL_TEXTURE_2D);

  // Now bind a texture to the activated unit
  glBindTexture(GL_TEXTURE_2D, g_texture1);

  // Setup texture unit 0
  // 1: First Activate texture unit 0
  glActiveTextureARB(GL_TEXTURE0);

  // 2: THEN enable 2D texturing 
  glEnable(GL_TEXTURE_2D);

  // Now bind a texture to the activated unit
  glBindTexture(GL_TEXTURE_2D, g_texture0);

  // Now do the drawing
  DO_GL_AND_CHECK(glDrawArrays(GL_QUADS,0,4));

  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  // Disable use of program
  glUseProgramObjectARB(0);
  
}

void
display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  drawBox();
  glutSwapBuffers();
}

bool 
  initShaders()
{
  // Create a vertex shader from a string
  g_vShader = vr::loadShaderFromString( 
    STRINGIFY( 
  
    void main() 
    { 
      gl_TexCoord[0] = gl_MultiTexCoord0;
      gl_TexCoord[1] = gl_MultiTexCoord1;
      gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex; 
    }  
  )
  , GL_VERTEX_SHADER);

  // Create a fragment shader from a string
  g_fShader = vr::loadShaderFromString(
    STRINGIFY(
      uniform sampler2D u_tex0;
      uniform sampler2D u_tex1;
      uniform float u_sinTime;

      void main() 
      { 
        vec4 color1 = texture2D(u_tex0, gl_TexCoord[0].st);
        vec4 color2 = texture2D(u_tex1, gl_TexCoord[1].st);

        // Use the float value to linear interpolate between color1 and color2
        vec4 color = mix(color1, color2, u_sinTime);

        gl_FragColor =  color; 
      }
    )
  
    , GL_FRAGMENT_SHADER);

  // Create a program object where the both shaders are attached.
  // A Shader Program consists of 1 or two shaders.
  g_program = glCreateProgramObjectARB();
  glAttachObjectARB(g_program, g_vShader);
  glAttachObjectARB(g_program, g_fShader);

  // Try to link the program (with the two attached shaders)
  DO_GL_AND_CHECK(glLinkProgramARB(g_program));

  // Check if it was successful
  if (!vr::checkLinkStatus(g_program))
    return false;

  // Locate the two uniforms in the linked program. We will assign values to them later on
  DO_GL_AND_CHECK(g_texture0Uniform = glGetUniformLocation(g_program, "u_tex0"));
  DO_GL_AND_CHECK(g_texture1Uniform = glGetUniformLocation(g_program, "u_tex1"));
  DO_GL_AND_CHECK(g_sinUniform = glGetUniformLocation(g_program, "u_sinTime"));

  return true;
}


void
init(void)
{

  glewInit();

  // Make sure we have the extensions we need.
  if (glewGetExtension("GL_ARB_multitexture") != GL_TRUE) {
    std::cerr << "Driver does not support GL_multitexture" << std::endl;
    exit(1);
  }

  if (glewGetExtension("GL_ARB_fragment_shader")      != GL_TRUE ||
    glewGetExtension("GL_ARB_vertex_shader")        != GL_TRUE ||
    glewGetExtension("GL_ARB_shader_objects")       != GL_TRUE ||
    glewGetExtension("GL_ARB_shading_language_100") != GL_TRUE)
  {
    std::cerr << "Driver does not support OpenGL Shading Language\n" << std::endl;
    exit(1);
  }


  // Activate a new multitexture ID(GL_TEXTURE0), and bind the "normal" texture to it.
  glActiveTextureARB(GL_TEXTURE1);
  g_texture1 = createTexture("lightmap.bmp");
  if (!g_texture1)
    exit(1);

  // Activate a new multitexture ID(GL_TEXTURE0), and bind the "normal" texture to it.
  DO_GL_AND_CHECK(glActiveTextureARB(GL_TEXTURE0));

  // Create BASE texture
  g_texture0 = createTexture("box.bmp");
  if (!g_texture0)
    exit(1);

  createBoxBuffers();

  /* Use depth buffering for hidden surface elimination. */
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);

  /* Setup the projection matrix. */
  glMatrixMode(GL_PROJECTION);
  gluPerspective( /* field of view in degree */ 40.0,
                  /* aspect ratio */ 1.0,
                  /* Z near */ 1.0, /* Z far */ 10.0);


  // Position the camera
  glMatrixMode(GL_MODELVIEW);

  gluLookAt(0.0, 0.0, 2.0,  /* eye is at (0,0,5) */
    0.0, 0.0, 0.0,      /* center is at (0,0,0) */
    0.0, 1.0, 0.);      /* up is in positive Y direction */

  // Rotate the object that will be rendered later on relative to the camera
  glRotatef(-20, 0.0, 1.0, 0.0);

  initShaders();
}


void keyboard(unsigned char key, int x, int y)
{
  switch(key) {
    case 27:
      exit(1);
      break;
  }
}

// Deallocate OpenGL resources
void cleanup()
{
  glDeleteTextures(1, &g_texture0);
  glDeleteTextures(1, &g_texture1);

  glDeleteBuffers(1, &g_positionBufferObject);
  glDeleteBuffers(1, &g_texcoordBufferObject);
  glDeleteBuffers(1, &g_texcoordBufferObject2);

  glDeleteShader(g_vShader);
  glDeleteShader(g_fShader);
  glDeleteProgram(g_program);
}


int
main(int argc, char **argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(512, 512);
  glutCreateWindow("Multi texturing example");
  glutDisplayFunc(display);
  glutIdleFunc(display);
  glutKeyboardFunc(keyboard);
  init();
  glutMainLoop();

  cleanup();
  return 0;             
}
