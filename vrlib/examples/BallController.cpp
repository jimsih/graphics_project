#include "BallController.h"
#include <GL/glew.h>

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/glut.h>
#include <iostream>

//-----------------------------------------------------------------------------
// Construction/Destruction
//-----------------------------------------------------------------------------

/*!
\short absolute value function which executes a simple test
This function executes a simple test on \e val negativity returning the
opposite if true. Such a test can be faster than the call to the \e fabs
library routine
\return the absolute value of \e val
*/
inline real simpleabs(const real &val)
{
	return val>0 ? val : -val;
}


void epsilonCorrect(const vr::Vec3& vec, vr::Vec3& target)
{
	if (simpleabs(target[0])<epsilon && simpleabs(target[1])<epsilon && simpleabs(target[2])<epsilon)	target = vec;
}

BallController::BallController()
{
	initVars();
}


BallController::BallController(const real& rad)
{
	initVars();
	m_radius = vr::clampBetween<real>(rad, 0.1, 1.0);
}


BallController::BallController(const real& rad,const vr::Quat& initialOrient)
{
	initVars();
	m_radius = vr::clampBetween<real>(rad, 0.1, 1.0);
	m_currentQuat = initialOrient;
}

BallController& BallController::operator=(const BallController& other)
{
	if(this==&other) return *this;
	initVars();
	m_currentQuat = other.m_currentQuat;
	m_previousQuat = other.m_previousQuat;
	m_radius = other.m_radius;
	m_winWidth = other.m_winWidth;
	m_winHeight = other.m_winHeight;
	m_otherAxesNum = other.m_otherAxesNum;
	m_otherAxes = new vr::Vec3[m_otherAxesNum];
	for (int c = 0; c<m_otherAxesNum; c++) m_otherAxes[c] = other.m_otherAxes[c];
	m_ballColor = other.m_ballColor;
	return *this;
}

void BallController::mouseDown(real x, real y, Button button)
{
	m_xprev = (2 * x - m_winWidth) / m_winWidth;
	m_yprev = (m_winHeight - 2 * y) / m_winHeight;

	m_mouseButtonDown[button] = true;

	if (button == LEFT) {
		m_previousQuat = m_currentQuat;
		m_drawBallArea = m_projectionMethod2;// draw circle only if method 2 active
	}
}


void BallController::mouseUp(float /*x*/, float/* y*/, Button button)
{
	m_mouseButtonDown[button] = false;

	m_xprev = m_yprev = 0.0;
	m_drawBallArea = false;

	if (button == LEFT) {
		// save current rotation axes for bodyAxes constraint at next rotation
		m_bodyorientation.set(m_currentQuat);
		m_bodyAxes[0] = vr::Vec3(m_bodyorientation(0, 0), m_bodyorientation(1, 0), m_bodyorientation(2, 0));
		m_bodyAxes[1] = vr::Vec3(m_bodyorientation(0, 1), m_bodyorientation(1, 1), m_bodyorientation(2, 1));
		m_bodyAxes[2] = vr::Vec3(m_bodyorientation(0, 2), m_bodyorientation(1, 2), m_bodyorientation(2, 2));
	}
}


void BallController::mouseMove(real x, real y)
{

	real xcurr = (2 * x - m_winWidth) / m_winWidth;
	real ycurr = (m_winHeight - 2 * y) / m_winHeight;
	vr::Vec3 vfrom(m_xprev, m_yprev, 0);
	vr::Vec3 vto(xcurr,ycurr,0);

	if (m_mouseButtonDown[LEFT])
	{
		// find the two points on sphere according to the projection method
		projectOnSphere(vfrom);
		projectOnSphere(vto);
		// modify the vr::Vec3s according to the active constraint
		if (m_whichConstraints != NO_AXES)
		{
			vr::Vec3* axisSet = getUsedAxisSet();
			vfrom = constrainToAxis(vfrom, axisSet[m_currentAxisIndex]);
			vto = constrainToAxis(vto, axisSet[m_currentAxisIndex]);
		};
		// get the corresponding vr::Quat
		vr::Quat lastQuat = rotationFromMove(vfrom, vto);
		m_currentQuat *= lastQuat;
		m_xprev = xcurr;
		m_yprev = ycurr;

	}
	if (m_mouseButtonDown[MIDDLE])
	{
		vr::Vec3 dir(0, 0, 1);
		real d = ycurr - m_yprev;
		m_translation += dir *d*m_translateScale;

		dir.set(1, 0, 0);
		d = xcurr - m_xprev;
		m_translation += dir *d*m_translateScale;

		m_xprev = xcurr;
		m_yprev = ycurr;
	}
	if (m_mouseButtonDown[RIGHT])
	{
		vr::Vec3 dir(0, 1, 0);
		real d = ycurr - m_yprev;
		m_translation += dir *d*m_translateScale;

		m_xprev = xcurr;
		m_yprev = ycurr;
	}
	else if (m_whichConstraints != NO_AXES)
	{
		projectOnSphere(vto);
		m_currentAxisIndex = nearestConstraintAxis(vto);
	}
}

void BallController::home()
{
	m_currentQuat = m_previousQuat = m_homeOrientation;
	m_translation = m_homePosition;

	mouseUp(-1, -1, LEFT);
}

void BallController::applyTransform()
{
#ifdef REAL_IS_DOUBLE
	glMultMatrixd(currentQuat.getRotMatrix());
#else
	vr::Matrix m1;
	m1.setTrans(m_center);

	vr::Matrix m;
	m.setTrans(m_translation);
	glMultMatrixf(m.ptr());


	glMultMatrixf(m1.ptr());
	m.set(m_currentQuat);
	glMultMatrixf(m.ptr());
	m1.setTrans(-m_center);

	glMultMatrixf(m1.ptr());



#endif
}

void BallController::projectOnSphere(vr::Vec3& v) const
{
	real rsqr = m_radius*m_radius;
	real dsqr=v.x()*v.x()+v.y()*v.y();
	if (m_projectionMethod2)
	{
		// if inside sphere project to sphere else on plane
		if(dsqr>rsqr)
		{
			register real scale = (m_radius - .05) / sqrt(dsqr);
			v.x()*=scale;
			v.y()*=scale;
			v.z()=0;
		}
		else
		{
			v.z()=sqrt(rsqr-dsqr);
		}
	}
	else
	{
		// if relatively "inside" sphere project to sphere else on hyperbolic sheet
		if(dsqr<(rsqr*0.5))	v.z()=sqrt(rsqr-dsqr);
		else v.z()=rsqr/(2*sqrt(dsqr));
	};
}



vr::Quat BallController::rotationFromMove(const vr::Vec3& vfrom,const vr::Vec3& vto)
{
	if (m_projectionMethod2)
	{
		vr::Quat q;
		q.x()=vfrom.z()*vto.y()-vfrom.y()*vto.z();
		q.y()=vfrom.x()*vto.z()-vfrom.z()*vto.x();
		q.z()=vfrom.y()*vto.x()-vfrom.x()*vto.y();
		q.w()=vfrom*vto;
		return vr::Quat(q);
	}
	else
	{
// calculate axis of rotation and correct it to avoid "near zero length" rot axis
		vr::Vec3 rotaxis=(vto^vfrom);
		rotaxis.normalize();

		epsilonCorrect(vr::Vec3::X_AXIS(), rotaxis);
// find the amount of rotation
		vr::Vec3 d(vfrom-vto);
		real t = d.length() / (2.0*m_radius);
		vr::clampBetween<real>(t,-1.0,1.0);
		real phi=2.0*asin(t);
		std::swap(rotaxis[1], rotaxis[2]);
		rotaxis = rotaxis*(-1);
		return vr::Quat(phi,rotaxis);
	}
}

void BallController::key(unsigned int nChar)
{
	switch(nChar)
	{
	case GLUT_KEY_UP:
		m_currentQuat *= vr::Quat(vr::DegreesToRadians(m_angleKeyIncrement), vr::Vec3::X_AXIS());
		break;
	case GLUT_KEY_DOWN:
		m_currentQuat *= vr::Quat(vr::DegreesToRadians(-m_angleKeyIncrement), vr::Vec3::X_AXIS());
		break;
	case GLUT_KEY_RIGHT:
		m_currentQuat *= vr::Quat(vr::DegreesToRadians(m_angleKeyIncrement), vr::Vec3::Y_AXIS());
		break;
	case GLUT_KEY_LEFT:
		m_currentQuat *= vr::Quat(vr::DegreesToRadians(-m_angleKeyIncrement), vr::Vec3::Y_AXIS());
		break;
	case ' ':
		home();
		break;
	};
}

void BallController::drawBallLimit()
{
	// "spherical zone" of controller
#ifdef REAL_IS_DOUBLE
		glColor3dv(BallColor);
#else
	glColor3fv(m_ballColor.ptr());
#endif
	glCallList(m_gldisplayList);
}

void BallController::initDisplayLists()
{
	m_gldisplayList=glGenLists(1);
	if(m_gldisplayList!=0)
	{
		GLUquadricObj* qobj=gluNewQuadric();
		gluQuadricDrawStyle(qobj,GLU_LINE);
		gluQuadricNormals(qobj,GLU_NONE);
		glNewList(m_gldisplayList,GL_COMPILE);
		gluDisk(qobj, m_radius, m_radius, 24, 1);
		glEndList();
		gluDeleteQuadric(qobj);
	}
}

void BallController::setHome(const vr::Vec3& position, const vr::Vec3& center, const vr::Quat& orientation)
{
	m_homeOrientation = orientation;
	m_homePosition = position;
	m_center = center;
}


void BallController::initVars()
{
	m_homeOrientation = vr::Quat( vr::Vec3::X_AXIS(), vr::Vec3::X_AXIS());
	m_homePosition.set(0, 0, 0);
	m_center.set(0, 0, 0);

	m_translateScale = 1.0;
	m_winWidth = m_winHeight = 0;
	m_mouseButtonDown[LEFT] = m_mouseButtonDown[MIDDLE] = m_mouseButtonDown[RIGHT] = false;
	m_drawBallArea = m_projectionMethod2 = m_drawConstraints = false;
	m_xprev = m_yprev = 0.0;
	m_radius = 1.3;
	m_gldisplayList = m_currentAxisIndex = m_otherAxesNum = 0;
	m_ballColor = vr::Vec3(0.0, 0.5, 1.0);
	m_otherAxes = NULL;
	m_whichConstraints = NO_AXES;
	m_cameraAxes[0] = m_bodyAxes[0] = vr::Vec3::X_AXIS();
	m_cameraAxes[1] = m_bodyAxes[1] = vr::Vec3::Y_AXIS();
	m_cameraAxes[2] = m_bodyAxes[2] = vr::Vec3::Z_AXIS();
	m_bodyorientation.makeIdentity();
	m_angleKeyIncrement = 5;

	home();
}

void BallController::setColor(vr::Vec4 col)
{
	m_ballColor.x() = (col[0]) / 255.0;
	m_ballColor.y() = (col[1]) / 255.0;
	m_ballColor.z() = (col[2]) / 255.0;
}

void BallController::setColorV(vr::Vec3 colvec)
{
	colvec.clamp(0,1);
	m_ballColor = colvec;
}

vr::Vec3 BallController::constrainToAxis(const vr::Vec3& loose,const vr::Vec3& axis)
{
	vr::Vec3 onPlane;
    register real norm;
    onPlane = loose-axis*(axis*loose);
    norm = onPlane.length();
    if (norm > 0)
	{
		if (onPlane.z() < 0.0) onPlane = -onPlane;
		return ( onPlane/=sqrt(norm) );
    };
    if (axis.z() == 1) onPlane = vr::Vec3::X_AXIS();
	else
	{
		onPlane = vr::Vec3(-axis.y(), axis.x(), 0);
		onPlane.normalize();
    }
    return (onPlane);
}

int BallController::nearestConstraintAxis(const vr::Vec3& loose)
{
	vr::Vec3* axisSet=getUsedAxisSet();
	vr::Vec3 onPlane;
	register float max, dot;
	register int i, nearest;
	max = -1; 
	nearest = 0;
	if (m_whichConstraints == OTHER_AXES)
	{
		for (i = 0; i<m_otherAxesNum; i++)
		{
			onPlane = constrainToAxis(loose, axisSet[i]);
			dot = onPlane*loose;
			if (dot>max) max = dot; nearest = i;
		}
	}
	else
	{
		for (i=0; i<3; i++)
		{
			onPlane = constrainToAxis(loose, axisSet[i]);
			dot = onPlane*loose;
			if (dot>max)
			{
				max = dot;
				nearest = i;
			};
		}
	};
	return (nearest);
}

vr::Vec3* BallController::getUsedAxisSet()
{
    vr::Vec3* axes=NULL;
		switch (m_whichConstraints)
	{
		case CAMERA_AXES: axes = m_cameraAxes;
		break;
		case BODY_AXES: axes = m_bodyAxes;
		break;
		case OTHER_AXES: axes = m_otherAxes;
		break;
	};
	return axes;
}

void BallController::drawConstraints()
{
	glColor3f(0,.75f,0);
	if (m_whichConstraints == CAMERA_AXES)
	{
		glCallList(m_gldisplayList);
		glBegin(GL_LINES);
		glVertex3f(-m_radius, 0, 0);
		glVertex3f(m_radius, 0, 0);
		glVertex3f(0, -m_radius, 0);
		glVertex3f(0, m_radius, 0);
		glEnd();
	};
	if (m_whichConstraints == BODY_AXES)
	{
		glPushMatrix();
#ifdef REAL_IS_DOUBLE
		glMultMatrixd(bodyorientation);
#else
		glMultMatrixf(m_bodyorientation.ptr());
#endif
		glCallList(m_gldisplayList);
		glRotated(90,1.0,0.0,0.0);
		glCallList(m_gldisplayList);
		glRotated(90,0.0,1.0,0.0);
		glCallList(m_gldisplayList);

		glPopMatrix();
	};
}

void BallController::drawBall()
{
// change the projection matrix to identity (no view transformation )
	glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
// reset the transformations
	glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();
// prepare the circle display list the first time
		if (m_gldisplayList == 0) initDisplayLists();
// disable lighting and depth testing
	glPushAttrib(GL_ENABLE_BIT | GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glDisable(GL_DEPTH_TEST);
// draw the constraints or the ball limit if appropriate
		if (m_drawConstraints && m_whichConstraints != NO_AXES) drawConstraints();
		else if (m_drawBallArea) drawBallLimit();
	glPopAttrib();
// restore the modelview and projection matrices
		glPopMatrix();
	glMatrixMode(GL_PROJECTION);
		glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}
