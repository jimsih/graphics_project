
#ifndef _WIN32
/*
Workaround for some kind of linker bug. See https://bugs.launchpad.net/ubuntu/+source/nvidia-graphics-drivers-319/+bug/1248642
*/
#include <pthread.h>
void junk() {
  int i;
  i=pthread_getconcurrency();
};
#endif


#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <stdio.h>
#include <assert.h>
#include <vr/glErrorUtil.h>
#include <vector>
#include <vr/shaderUtils.h>
#include <vr/Vec3.h>

GLuint g_fb;
GLuint g_depth_rb;
GLuint g_color_tex;
GLuint g_depth_tex;
GLuint g_program;
GLuint g_vShader;
GLuint g_fShader;
GLuint g_texture0Uniform;
GLuint g_texture1Uniform;

int g_imageWinWidth = 512;
int g_imageWinHeight = 512;

// GLUT reshape function
void reshape(int w, int h)
{
  if (h == 0) h = 1;

  glViewport(0, 0, w, h);

  // GPGPU CONCEPT 3b: One-to-one Pixel to Texel Mapping: An Orthographic
  //                   Projection.
  // This code sets the projection matrix to orthographic with a range of
  // [-1,1] in the X and Y dimensions. This allows a trivial mapping of
  // pixels to texels.
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(-1, 1, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

static GLenum errCode;
const GLubyte *errString;


void myIdle(){
  glutPostRedisplay();
}

void keyboard (unsigned char key, int /*x*/, int /*y*/)
{
  switch (key) {
      case 27:
        exit(0);
        break;
      default:
        break;
  }
}

void MouseFunc( int button, int /*state*/, int /*x*/, int /*y*/)
{
  switch(button) {
    case GLUT_LEFT_BUTTON :
      break;
    case GLUT_RIGHT_BUTTON :
      break;
  }
}


void render_redirect() {

  //bind the FBO, and the associated texture.
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, g_fb);

  // draw a scene.  the results are being 
  // written into the associated texture,'tex'
  glClearColor(0.0, 0.0, 1.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glColor4f( 0.0, 1.0, 1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glPushMatrix();
  glRotatef(26, 1,1,0);
  glTranslatef(-0.5,-0.5,0);
  glutSolidTeapot(0.5);
  glPopMatrix();

  // 'unbind' the FBO. things will now be drawn to screen as usual
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

  glClearColor(0.0, 0.0, 0.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glColor3f(1.0, 1.0, 1.0); 
  glEnable(GL_TEXTURE_2D);



  // First enable our shader program
  DO_GL_AND_CHECK(glUseProgramObjectARB(g_program));

  glUniform1i(g_texture0Uniform, 0);
  glUniform1i(g_texture1Uniform, 1);

  std::vector<vr::Vec3> vertices;
  vertices.push_back( vr::Vec3(-0.9, -0.9, -1.0));
  vertices.push_back( vr::Vec3(-0.9, 0.9, -1.0));
  vertices.push_back( vr::Vec3(0.9,0.9, -1.0));
  vertices.push_back( vr::Vec3(0.9, -0.9, -1.0));

  // Vertex array object
  DO_GL_AND_CHECK(glEnableClientState(GL_VERTEX_ARRAY));
  DO_GL_AND_CHECK(glVertexPointer(3, GL_FLOAT,0,&vertices[0]));

  std::vector<vr::Vec2> texCoords;
  texCoords.push_back( vr::Vec2(0,0));
  texCoords.push_back( vr::Vec2(0,1));
  texCoords.push_back( vr::Vec2(1,1));
  texCoords.push_back( vr::Vec2(1,0));

  // Texture coordinates for unit0
  glClientActiveTexture(GL_TEXTURE0);
  DO_GL_AND_CHECK(glEnableClientState(GL_TEXTURE_COORD_ARRAY));
  DO_GL_AND_CHECK(glTexCoordPointer(2,GL_FLOAT,0,&texCoords[0]));
 
  // Texture coordinates for unit1
   glClientActiveTexture(GL_TEXTURE1);
   DO_GL_AND_CHECK(glEnableClientState(GL_TEXTURE_COORD_ARRAY));
   DO_GL_AND_CHECK(glTexCoordPointer(2,GL_FLOAT,0,&texCoords[0]));

  // First Activate texture unit 1
  glActiveTextureARB(GL_TEXTURE1);

  // THEN enable 2D texturing 
  glEnable(GL_TEXTURE_2D);

  // Now bind a texture to the activated unit
  glBindTexture(GL_TEXTURE_2D, g_depth_tex);

  // Setup texture unit 0
  glActiveTextureARB(GL_TEXTURE0);

  // THEN enable 2D texturing 
  glEnable(GL_TEXTURE_2D);

  // Now bind a texture to the activated unit
  glBindTexture(GL_TEXTURE_2D, g_color_tex);

  // Now do the drawing
  DO_GL_AND_CHECK(glDrawArrays(GL_QUADS,0,4));

  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  // Disable use of program
  glUseProgramObjectARB(0);

  glDisable(GL_TEXTURE_2D);

  glutSwapBuffers();
}

void myinit(void)
{
  if (glewGetExtension("GL_ARB_fragment_shader")      != GL_TRUE ||
    glewGetExtension("GL_ARB_vertex_shader")        != GL_TRUE ||
    glewGetExtension("GL_ARB_shader_objects")       != GL_TRUE ||
    glewGetExtension("GL_ARB_shading_language_100") != GL_TRUE)
  {
    std::cerr << "Driver does not support OpenGL Shading Language\n" << std::endl;
    exit(1);
  }

  GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat mat_shininess[] = { 50.0 };
  GLfloat light_position[] = { 1.0, -1.0, 1.0, 0.0 };

  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);
}

bool 
  initShaders()
{
  // Create a vertex shader from a string
  g_vShader = vr::loadShaderFromString( 
    STRINGIFY( 

    void main() 
  { 
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_TexCoord[1] = gl_MultiTexCoord1;
    gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex; 
  }  
  )
    , GL_VERTEX_SHADER);

  // Create a fragment shader from a string
  g_fShader = vr::loadShaderFromString(
    STRINGIFY(
    uniform sampler2D u_tex0;
    uniform sampler2D u_tex1;

  void main() 
  { 
    vec4 color1 = texture2D(u_tex0, gl_TexCoord[0].st);
    vec4 color2 = texture2D(u_tex1, gl_TexCoord[1].st);

   
		// Draw half with one texture and the other with the second texture
		if(gl_TexCoord[1].s >0.5 )
      gl_FragColor =  color1;
    else
      gl_FragColor =  color2;
  }
  )

    , GL_FRAGMENT_SHADER);

  // Create a program object where the both shaders are attached.
  // A Shader Program consists of 1 or two shaders.
  g_program = glCreateProgramObjectARB();
  glAttachObjectARB(g_program, g_vShader);
  glAttachObjectARB(g_program, g_fShader);

  // Try to link the program (with the two attached shaders)
  DO_GL_AND_CHECK(glLinkProgramARB(g_program));

  // Check if it was successful
  if (!vr::checkLinkStatus(g_program))
    return false;

  // Locate the two uniforms in the linked program. We will assign values to them later on
  DO_GL_AND_CHECK(g_texture0Uniform = glGetUniformLocation(g_program, "u_tex0"));
  DO_GL_AND_CHECK(g_texture1Uniform = glGetUniformLocation(g_program, "u_tex1"));

  return true;
}


int main(int argc, char *argv[] )  {

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(g_imageWinWidth, g_imageWinHeight);
  glutCreateWindow(argv[0]);

  myinit();

  // Initialize the "OpenGL Extension Wrangler" library
  glewInit();

  //gen the framebuffer object (FBO), similar manner as  a texture
  DO_GL_AND_CHECK(glGenFramebuffersEXT(1, &g_fb));


  const int TEXX=256;
  const int TEXY=256;


  // create depth texture & attach to FBO
  glGenTextures(1, &g_depth_tex);
  glBindTexture(GL_TEXTURE_2D, g_depth_tex);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);


  DO_GL_AND_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, TEXX, TEXY, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL));
  
  // initialize color texture
  // create depth texture & attach to FBO

  glGenTextures(1, &g_color_tex);
  glBindTexture(GL_TEXTURE_2D, g_color_tex);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  DO_GL_AND_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, TEXX, TEXY, 0, GL_RGB, GL_INT, NULL));

  // Enable render-to-texture
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, g_fb);

  // Set up color_tex and depth_rb for render-to-texture
  DO_GL_AND_CHECK(glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, g_color_tex, 0));

  // Check framebuffer completeness at the end of initialization.
  CHECK_FRAMEBUFFER_STATUS();

  
  DO_GL_AND_CHECK(glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, g_depth_tex, 0));
  
//   glDrawBuffer(GL_NONE);
//   glReadBuffer(GL_NONE);

  CHECK_FRAMEBUFFER_STATUS();

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  
 
  initShaders();

  glutDisplayFunc(render_redirect);
  glutIdleFunc(myIdle);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutMouseFunc(MouseFunc);
  glutMainLoop();
  return 0;
}
