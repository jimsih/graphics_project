// this file uses Doxygen comment blocks for automatic extraction of source code documentation.

/*!\file
 Declaration of the BallController class.
 This class implements a virtual trackball rotation controller suitable for
 use in 3D graphical applications.
 This implementation "takes inspiration" and merges features found in
 K.Shoemake's Arcball controller (see Graphic Gems vol.4) and J.Childs' GLcube rotation
 controller which is in turn based on M.J.Kilgard's GLUT code.
 The interaction method has been tailored on a Document-View-Controller paradigm
 such as that in Microsoft MFC framework but it's applicable to other
 contexts as well.
 \version 0.2
 \date 15/10/99
 \author Alessandro Falappa
*/

//-----------------------------------------------------------------------------
// BallController.h: interface for the BallController class.

#ifndef __BALL_CONTROLLER_H__
#define __BALL_CONTROLLER_H__


#ifdef HIGH_PRECISION

//! The base type for all the math helpers
typedef double real;
//! the treshold for comparisons with zero, mainly used to avoid division by zero errors
const real epsilon = 1e-12;
//! defined when high precision is requested
#define REAL_IS_DOUBLE

#else

// WARNING: these pragmas below could be MSVC compiler specific
#pragma warning( push )// memorize the warning status
#pragma warning( disable : 4305 )// disable "initializing : truncation from 'const double' to 'float'" warning
#pragma warning( disable : 4244 )// disable "double to real conversion possible loss of data" warning
#pragma warning( disable : 4136 )// disable "conversion between different floating-point types" warning
#pragma warning( disable : 4309 )// disable " 'conversion' : truncation of constant value" warning
#pragma warning( disable : 4051 )// disable " 'type conversion' ; possible loss of data" warning
//! The base type for all the math helpers
typedef float real;
//! the treshold for comparisons with zero, mainly used to avoid division by zero errors
const real epsilon = 1e-7;
//! defined when high precision is not requested
#define REAL_IS_FLOAT

#endif


//MSVC compiler specifics
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <vr/Vec3.h>
#include <vr/Quat.h>
#include <vr/Matrix.h>



/*!
This is the controller object
*/
class BallController  
{
public:
	enum Button
	{
		LEFT, MIDDLE, RIGHT, NUM_BUTTONS
	};

public:

	/*!
	Defines some constant values used to set the axes to which
	constrain rotations about.
	*/
	enum AxisSet
	{
		NO_AXES,
		CAMERA_AXES,
		BODY_AXES,
		OTHER_AXES
	};

	bool getDrawConstraints();
	void setDrawConstraints(bool flag=true);
	void drawBall();
	int getAngleKeyIncrement();
	void setAngleKeyIncrement(int ang);
	void useConstraints(AxisSet constraints);
	void toggleMethod();
	void setAlternateMethod(bool flag=true);
	vr::Vec3 getColorV();
	vr::Vec4 getColor();
	void setColor(vr::Vec4 col);
	void setColorV(vr::Vec3 colvec);

	BallController();
	BallController(const real& rad);
	BallController(const real& rad,const vr::Quat& initialOrient);
	BallController(const BallController& other);

	virtual ~BallController();
	BallController& operator=(const BallController& other);
	void resize(const real& newRadius);
	void clientAreaResize(real width, real height);
	void mouseDown(real x, real y, Button button);
	void mouseUp(real x, real y, Button button);
	void mouseMove(real x, real y);
	void applyTransform();
	void key(unsigned int nChar);
	void setTranslateScale(float scale);
	void home();
	void setHome( const vr::Vec3& position, const vr::Vec3& center, const vr::Quat& orientation);
	vr::Vec3 getCenter() const { return m_center;  }
private:
	void drawConstraints();
	vr::Vec3* getUsedAxisSet();
	void initDisplayLists();
	void initVars(void);
	void projectOnSphere(vr::Vec3& v) const;
	vr::Quat rotationFromMove(const vr::Vec3& vfrom, const vr::Vec3& vto);
	vr::Vec3 constrainToAxis(const vr::Vec3& loose, const vr::Vec3& axis);
	int nearestConstraintAxis(const vr::Vec3& loose);
	void drawBallLimit();

	vr::Vec3 m_center;

	vr::Vec3 m_homePosition;
	vr::Quat m_homeOrientation;

	bool m_drawConstraints;
	vr::Matrix m_bodyorientation;
	real m_angleKeyIncrement;
	vr::Vec3 m_ballColor;
	bool m_projectionMethod2;
	bool m_drawBallArea;
	int m_gldisplayList;
	vr::Quat m_currentQuat;
	vr::Quat m_previousQuat;
	
	vr::Vec3 m_translation;
	float m_translateScale;

	real m_radius;
	real m_winWidth;
	real m_winHeight;
	real m_xprev;
	real m_yprev;
	//	vr::Vec3 center;
	bool m_mouseButtonDown[NUM_BUTTONS];
	AxisSet m_whichConstraints;
	int m_currentAxisIndex;
	vr::Vec3 m_cameraAxes[3];
	vr::Vec3 m_bodyAxes[3];
	vr::Vec3* m_otherAxes;
	int m_otherAxesNum;


};

//---------------------------------------------------------------------------
// inlines

inline BallController::~BallController()
{
	if (m_otherAxes) delete[] m_otherAxes;
	m_otherAxes = 0L;
}

inline void BallController::setTranslateScale(float scale) 
{ 
	m_translateScale = scale;  
}

inline BallController::BallController(const BallController& other)
{
	*this=other;
}

inline void BallController::resize(const real& newRadius)
{
	m_radius = newRadius;
}

inline void BallController::clientAreaResize(real width, real height)
{
	m_winWidth = real(width);
	m_winHeight = real(height);
//	center=CPoint( (newSize.right-newSize.left)/2 , (newSize.bottom-newSize.top)/2);
}


inline vr::Vec4 BallController::getColor()
{
	return vr::Vec4(int(m_ballColor.x() * 255), int(m_ballColor.y() * 255), int(m_ballColor.z() * 255), 255);
}

inline vr::Vec3 BallController::getColorV()
{
	return m_ballColor;
}

inline void BallController::setAlternateMethod(bool flag)
{
	m_projectionMethod2 = flag;
}

inline void BallController::toggleMethod()
{
	if (m_projectionMethod2) m_projectionMethod2 = false;
	else m_projectionMethod2 = true;
}

inline void BallController::useConstraints(AxisSet constraints)
{
	m_whichConstraints = constraints;
}

inline int BallController::getAngleKeyIncrement()
{
	return m_angleKeyIncrement;
}

inline void BallController::setAngleKeyIncrement(int ang)
{
	m_angleKeyIncrement = abs(ang) % 360;
}

inline bool BallController::getDrawConstraints()
{
	return m_drawConstraints;
}

inline void BallController::setDrawConstraints(bool flag)
{
	m_drawConstraints = flag;
}

#endif
