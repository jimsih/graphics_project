#include "Geometry.h"
#include <vr/glErrorUtil.h>
#include <vr/glUtils.h>
#include <vr/Matrix.h>
#include <vr/FileSystem.h>
#include <vr/shaderUtils.h>


Geometry::Geometry() : m_dirty(true), m_hasMaterial(false), m_texture(0), m_drawNormals(false) 
{
}


void Geometry::setVertices(const vr::Vec3Vector& vertices)
{
	dirty();
	m_vertices = vertices;
	m_maxDistance = 0;
	m_center.set(0, 0, 0);

	if (m_vertices.empty())
		return;
	
	updateData();
}


void Geometry::updateData()
{

	vr::Vec3 minimum(1E30f, 1E30f, 1E30f);
	vr::Vec3 maximum(-1E30f, -1E30f, -1E30f);
	vr::Vec3 sum;
	for (vr::Vec3Vector::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
	{
		vr::Vec3 v = *it;
		sum += v;

		for (size_t i = 0; i < 3; i++)
		{
			minimum[i] = vr::minimum(minimum[i], v[i]);
			maximum[i] = vr::maximum(maximum[i], v[i]);

		}
	}
	m_center = sum / (vr::Vec3::value_type)m_vertices.size();
	m_maxDistance = (minimum - maximum).length();
}

void Geometry::applyMaterial() const
{
	if (!m_hasMaterial)
		return;

	glShadeModel(GL_SMOOTH);

	if (m_material.dissolve < 1)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glDisable(GL_BLEND);
	}

	glMaterialfv(GL_FRONT, GL_SPECULAR, vr::Vec4(m_material.specular,1).ptr());
	glMaterialfv(GL_FRONT, GL_AMBIENT, vr::Vec4(m_material.ambient,1).ptr());
	glMaterialfv(GL_FRONT, GL_DIFFUSE, vr::Vec4(m_material.diffuse, m_material.dissolve).ptr());
	glMaterialf(GL_FRONT, GL_SHININESS, m_material.shininess);


	if (m_dirty)
	{
		if (m_texture > 0)
		{
			glDeleteTextures(1, &m_texture);
		}
		if (!m_material.diffuse_texname.empty())
			m_texture = vr::createTexture(m_material.diffuse_texname);


		m_shaderProgram = createDefaultShaderProgram();


		m_dirty = false;
	}

	glUseProgram(m_shaderProgram->program);
	if (m_texture > 0)
	{
		// Setup texture unit 0
		// 1: First Activate texture unit 0
		glActiveTextureARB(GL_TEXTURE0);

		// 2: THEN enable 2D texturing 
		glEnable(GL_TEXTURE_2D);

		// Now bind a texture to the activated unit
		glBindTexture(GL_TEXTURE_2D, m_texture);

		glUniform1i(m_shaderProgram->hasTexture0, 1);
	}
	else
	{
		glUniform1i(m_shaderProgram->hasTexture0, 0);
	}

}

void Geometry::drawNormals() const
{
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glLineWidth(2.0);
	glColor3f(1, 1, 1);
	glBegin(GL_LINES);
	for (size_t i = 0; i < m_vertices.size(); i++)
	{
		vr::Vec3 start = m_vertices[i];
		vr::Vec3 dir = m_normals[i];
		vr::Vec3 end = start + dir*0.5;
		glVertex3fv(start.ptr());
		glVertex3fv(end.ptr());
	}
	glEnd();
	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
}


void Geometry::draw() const
{

	applyMaterial();

	if (1)
	{
		CheckErrorsGL("BEGIN : Draw array");

		glEnableClientState(GL_VERTEX_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, &m_vertices[0]);
		if (m_normals.size())
		{
			glEnableClientState(GL_NORMAL_ARRAY);
			glNormalPointer(GL_FLOAT, 0, &m_normals[0]);
		}

		if (m_texCoords.size() > 0) {
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glTexCoordPointer(2, GL_FLOAT, 0, &m_texCoords[0]);
		}

		if (m_indices.size() > 0)
		{
			glDrawElements(GL_TRIANGLES, (GLsizei)m_indices.size(), GL_UNSIGNED_INT, &m_indices[0]);
		}
		else	//If no indices exist, assume that the vertices are stored in order.
		{
			glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());
		}

		// deactivate vertex arrays after drawing
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		CheckErrorsGL("END : Draw array");


		if (m_drawNormals)
			drawNormals();


	}
	else
	{
		glBegin(GL_TRIANGLES);
		vr::UIntVector::const_iterator it = m_indices.begin();

		for (size_t i = 0; i < m_indices.size(); i = i + 3)

		{
			glVertex3fv(m_vertices[m_indices[i]].ptr());
			glNormal3fv(m_normals[m_indices[i]].ptr());
			if (m_texCoords.size())
				glTexCoord2fv(m_texCoords[m_indices[i]].ptr());

			glVertex3fv(m_vertices[m_indices[i + 1]].ptr());
			glNormal3fv(m_normals[m_indices[i + 1]].ptr());
			if (m_texCoords.size())
				glTexCoord2fv(m_texCoords[m_indices[i + 1]].ptr());

			glVertex3fv(m_vertices[m_indices[i + 2]].ptr());
			glNormal3fv(m_normals[m_indices[i + 2]].ptr());
			if (m_texCoords.size())
				glTexCoord2fv(m_texCoords[m_indices[i + 2]].ptr());

		}
		glEnd();

		if (m_drawNormals)
			drawNormals();

	}


	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}

void Geometry::setScale(float scale)
{
	m_scale = vr::Vec3(scale, scale, scale);

	if (m_vertices.empty())
		return;

	scaleVertices();
}

void Geometry::setScale(const vr::Vec3& scale)
{
	m_scale = scale;

	if (m_vertices.empty())
		return;

	scaleVertices();
}

void Geometry::scaleVertices()
{
	vr::Vec3Vector::iterator it = m_vertices.begin();

	vr::Matrix m = vr::Matrix::scale(m_scale);

	for (; it != m_vertices.end(); ++it)
	{
		*it = m * (*it);
	}

	updateData();
}


size_t Geometry::loadObjFile(const std::string& filename, GeometrySharedPtrVector& geometries)
{
	std::string filepath = filename;
	bool exist = vr::FileSystem::exists(filepath);

	char *vrPath = getenv("VR_PATH");
	if (!vrPath)
		std::cerr << "The environment variable VR_PATH is not set. It should point to the directory where the vr library is (just above models)" << std::endl;

	if (!exist && vrPath)
	{
		filepath = std::string(vrPath) + "/" + filename;
		exist = vr::FileSystem::exists(filepath);
	}

	if (!exist)
	{
		std::cerr << "The file " << filename << " does not exist" << std::endl;
		return 0;
	}


	// We will try to find the material file in the same directory as the obj file
	std::string path = vr::FileSystem::getDirectory(filepath);
	if (path.size() > 0)
		path = path + "/";

	vr::shape_t_vector shapes;
	vr::material_t_vector materials;

	// Now load the obj file and collect shapes and materials
	std::string err = vr::LoadObj(shapes, materials, filepath.c_str(), path.c_str());

	// Nothing found, something went wrong
	if (!err.empty())
	{
		std::cerr << "Error: " << err << std::endl;
		return 0;
	}

	// Go through each shape and create a Geometry that we can render
	for (vr::shape_t_vector::iterator it = shapes.begin(); it != shapes.end(); ++it)
	{
		vr::shape_t& shape = *it;
		std::shared_ptr<Geometry> geom(new Geometry);
		geom->setVertices(shape.mesh.positions);
		geom->setIndices(shape.mesh.indices);
		geom->setNormals(shape.mesh.normals);
		geom->setTexCoords(shape.mesh.texcoords);
		geometries.push_back(geom);

		// Do we have a material for this mesh?
		if (!materials.empty() && !shape.mesh.material_ids.empty())
		{
			vr::material_t material = materials[shape.mesh.material_ids[0]];


			// Collect all the textures if available
			if (!material.ambient_texname.empty())
				material.ambient_texname = path + material.ambient_texname;
			if (!material.diffuse_texname.empty())
				material.diffuse_texname = path + material.diffuse_texname;

			if (!material.specular_texname.empty())
				material.specular_texname = path + material.specular_texname;

			if (!material.bump_texname.empty())
				material.bump_texname = path + material.bump_texname;

			// Save the material spec to the Geometry
			geom->setMaterial(material);
		}
	}

	return geometries.size();
}

Geometry::~Geometry()
{
	if (m_texture)
	{
		glDeleteTextures(1, &m_texture);
	}
}



std::shared_ptr<ShaderProgram> Geometry::createDefaultShaderProgram() const
{
	// Create a vertex shader from a string

	std::shared_ptr<ShaderProgram> sh(new ShaderProgram());

	char *vrPath = getenv("VR_PATH");
	std::string vrPathString;
	if (!vrPath)
		std::cerr << "The environment variable VR_PATH is not set. It should point to the directory where the vr library is (just above models)" << std::endl;
	else
		vrPathString = vrPath + std::string("/");


	sh->v_shader = vr::loadShader(vrPathString+"shaders/default.vert", GL_VERTEX_SHADER);
	if (!sh->v_shader)
		return sh;

	sh->f_shader = vr::loadShader(vrPathString+"shaders/default.frag", GL_FRAGMENT_SHADER);
	if (!sh->f_shader)
		return sh;


	// Create a program object where the both shaders are attached.
	// A Shader Program consists of 1 or two shaders.
	sh->program = glCreateProgramObjectARB();
	glAttachObjectARB(sh->program, sh->v_shader);
	glAttachObjectARB(sh->program, sh->f_shader);

	// Try to link the program (with the two attached shaders)
	DO_GL_AND_CHECK(glLinkProgramARB(sh->program));

	// Check if it was successful
	if (!vr::checkLinkStatus(sh->program))
		return sh;

	CheckErrorsGL("BEGIN :GetLocation");

	// Locate the two uniforms in the linked program. We will assign values to them later on
	DO_GL_AND_CHECK(sh->tex0Uniform = glGetUniformLocation(sh->program, "tex0"));
	DO_GL_AND_CHECK(sh->hasTexture0 = glGetUniformLocation(sh->program, "hasTexture0"));

	CheckErrorsGL("END :GetLocation");

	return sh;
}

