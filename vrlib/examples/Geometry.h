#ifndef __GEOMETRY_H_
#define __GEOMETRY_H_

#include <vr/Vec3.h>
#include <vr/Vec2.h>
#include <vr/Vec4.h>
#include <vr/ObjLoader.h>
#include <GL/glew.h>
#include <memory>

// A simple, very non-complete implementation of your geometry
// Images should probably NOT be stored in the geometry, but rather in a State!!

class Geometry;

typedef std::vector<Geometry *> GeometryPtrVector;
typedef std::vector<std::shared_ptr<Geometry> > GeometrySharedPtrVector;


struct ShaderProgram {
	ShaderProgram() : v_shader(0), f_shader(0), program(0), tex0Uniform(0), hasTexture0(0) {}

	~ShaderProgram()
	{
		if (v_shader)
			glDeleteShader(v_shader);
		if (f_shader)
			glDeleteShader(f_shader);
		if (program)
			glDeleteProgram(program);

		f_shader = v_shader = program = tex0Uniform = 0;
	}
	GLuint v_shader, f_shader, program;
	GLuint tex0Uniform;
	GLuint hasTexture0;
};


class Geometry {
public:
	Geometry();

	void draw() const;

	const vr::Vec3Vector& getVerties() const { return m_vertices; }
	const vr::Vec3Vector& getNormals() const { return m_normals; }
	const vr::Vec2Vector& getTexCoords() const { return m_texCoords; }
	const vr::UIntVector& getIndices() const { return m_indices; }

	void setScale(float scale);
	void setScale(const vr::Vec3& scale );


	float getMaxDistance() const { return m_maxDistance; }
	vr::Vec3 getCenter() const { return m_center; }

	void setVertices(const vr::Vec3Vector& vertices);
	void setNormals(const vr::Vec3Vector& normals)  { dirty(); m_normals = normals; }
	void setIndices(const vr::UIntVector& indices)  { dirty(); m_indices = indices; }
	void setTexCoords(const vr::Vec2Vector& texCoords)  { dirty(); m_texCoords = texCoords; }
	void setTexture(const std::string& path) { m_texturePath = path; }

	void setMaterial(const vr::material_t& material) { m_material = material; m_hasMaterial = true; m_dirty = true; }

	bool getEnableDrawNormals() const { return m_drawNormals; }

	void setEnableDrawNormals(bool flag)
	{
		m_drawNormals = flag;
	}

	void dirty() { m_dirty = true; }
	~Geometry();


	static size_t loadObjFile(const std::string& filename, GeometrySharedPtrVector& geometries);


private:
	std::shared_ptr<ShaderProgram> createDefaultShaderProgram() const;
	void drawNormals() const;
	void applyMaterial() const;
	void scaleVertices();
	void updateData();

	vr::Vec3 m_scale;

	vr::Vec3Vector m_vertices;
	vr::UIntVector m_indices;
	vr::Vec3Vector m_normals;
	vr::Vec2Vector m_texCoords;
	vr::material_t m_materials;
	mutable GLuint m_texture;
	std::string m_texturePath;
	vr::material_t m_material;
	bool m_hasMaterial;
	float m_maxDistance;
	vr::Vec3 m_center;
	bool m_drawNormals;

	mutable bool m_dirty;

	mutable GLuint m_vertexArrayObj;

	mutable std::shared_ptr<ShaderProgram> m_shaderProgram;

};








#endif
