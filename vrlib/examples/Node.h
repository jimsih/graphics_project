#ifndef __NODE_H__
#define __NODE_H__
#include <memory>
#include "Geometry.h"

struct Node
{
	GeometrySharedPtrVector geometries;
	vr::Matrix matrix;

	~Node()
	{
		if (!geometries.empty())
		{
			for (size_t i = 0; i < geometries.size(); i++)
				geometries[i].reset();

			geometries.clear();
		}
	}

	Node(const GeometrySharedPtrVector& v) { geometries = v; }


	void setScale(float scale)
	{
		GeometrySharedPtrVector::iterator it = geometries.begin();
		for (; it != geometries.end(); ++it)
			(*it)->setScale(scale);
	}

	void setScale(const vr::Vec3& scale)
	{
		GeometrySharedPtrVector::iterator it = geometries.begin();
		for (; it != geometries.end(); ++it)
			(*it)->setScale(scale);
	}

	void draw()
	{
		glPushMatrix();
		glMultMatrixf(matrix.ptr());
		GeometrySharedPtrVector::const_iterator it = geometries.begin();
		for (; it != geometries.end(); ++it)
			(*it)->draw();
		glPopMatrix();
	}
};

typedef std::vector<std::shared_ptr<Node> > NodeSharedPtrVector;


#endif
