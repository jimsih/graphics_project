#ifndef __SCENE_H__
#define __SCENE_H__

#include <vr/Vec3.h>
#include <functional>
#include "Node.h"

template<typename T>
class Scene {

	typedef std::function<void(std::pair<std::shared_ptr<Node>, T>& )> UpdateFunction;

	static void nullUpdate(std::pair<std::shared_ptr<Node>, void *>&)
	{

	}


public:
	Scene(UpdateFunction f = &Scene<void *>::nullUpdate) : shouldDrawOrigo(true), shouldDrawFPS(true), m_enableDrawNormals(false), m_update(f), drawLights(true) {}
	void init();

	void add(std::shared_ptr<Node> node, T obj=T());

	void setShouldDrawOrigo(bool flag) { shouldDrawOrigo = flag; }
	void setShouldDrawFPS(bool flag) { shouldDrawFPS = flag; }
	void setEnableDrawNormals(bool f);
	bool getEnableDrawNormals() const { return m_enableDrawNormals; }


	void update();

	void drawFPS();

	void drawOrigo() const;

	float getMaxDistance() const;

	vr::Vec3 getCenter() const;

public:


	vr::Matrix modelView;
	BallController ballController;
	bool shouldDrawOrigo;
	bool shouldDrawFPS;
	bool drawLights;


private:

	typedef std::vector<std::pair<std::shared_ptr<Node>, T> > PhysicalVector;
	PhysicalVector m_physicalObjects;

	NodeSharedPtrVector m_nodes;

	bool m_enableDrawNormals;
	vr::Timer m_fpsTimer;

	UpdateFunction m_update;

};

template <typename T>
void Scene<T>::setEnableDrawNormals(bool f)
{
	if (m_nodes.empty())
		return;

	for (NodeSharedPtrVector::iterator nit = m_nodes.begin(); nit != m_nodes.end(); ++nit)
		for (GeometrySharedPtrVector::iterator git = (*nit)->geometries.begin(); git != (*nit)->geometries.end(); ++git)
			(*git)->setEnableDrawNormals(f);

	m_enableDrawNormals = f;
}

template<typename T>
void Scene<T>::add(std::shared_ptr<Node> node, T obj)
{
	if (node->geometries.empty())
	{
		std::cerr << "Node contains no geometries! Ignored" << std::endl;
		return;
	}
	m_nodes.push_back(node);

	if (obj)
		m_physicalObjects.push_back(std::make_pair(node, obj));
}

template<typename T>
void Scene<T>::drawFPS()
{
	if (!shouldDrawFPS)
		return;

	if (!m_fpsTimer.isRunning())
	{
		m_fpsTimer.start();
		return;
	}

	m_fpsTimer.stop();
	double time_ms = m_fpsTimer.getTime();

	double fps = 1000 / time_ms;


	std::ostringstream str;
	str << "FPS: " << fps << std::ends;


	vr::Text::setColor(vr::Vec4(1, 1, 0, 0.8));
	vr::Text::setFontSize(20);
	vr::Text::drawText(10, 20, str.str().c_str());
	m_fpsTimer.reset(true);

}

template<typename T>
void Scene<T>::drawOrigo() const
{
	if (!shouldDrawOrigo)
		return;

	float length = 10;
	glDisable(GL_LIGHTING);
	glLineWidth(4.0);

	glDisable(GL_DEPTH_TEST);
	glDepthMask(false);

	glColor3f(1, 0, 0);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(length, 0, 0);
	glEnd();

	glColor3f(0, 1, 0);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(0, length, 0);
	glEnd();

	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, length);
	glEnd();

	glEnable(GL_DEPTH_TEST);
	glDepthMask(true);


	glEnable(GL_LIGHTING);
}

template<typename T>
float Scene<T>::getMaxDistance() const
{
	float distance = -1;
	for (NodeSharedPtrVector::const_iterator nit = m_nodes.begin(); nit != m_nodes.end(); ++nit)
		for (GeometrySharedPtrVector::const_iterator git = (*nit)->geometries.begin(); git != (*nit)->geometries.end(); ++git)
			distance = vr::maximum(distance, (*git)->getMaxDistance());

	return distance;
}

template<typename T>
vr::Vec3 Scene<T>::getCenter() const
{
	vr::Vec3 center;
	if (!m_nodes.size())
		return center;

	size_t n = 0;
	for (NodeSharedPtrVector::const_iterator nit = m_nodes.begin(); nit != m_nodes.end(); ++nit)
		for (GeometrySharedPtrVector::const_iterator git = (*nit)->geometries.begin(); git != (*nit)->geometries.end(); ++git)
		{
			center += (*git)->getCenter();
			n++;
		}

	return center / n;
}

template<typename T>
void Scene<T>::init()
{
	m_nodes.clear();
	m_physicalObjects.clear();
}

template<typename T>
void Scene<T>::update()
{

	if (m_update && m_physicalObjects.size())
	{
		for (typename PhysicalVector::iterator it = m_physicalObjects.begin(); it != m_physicalObjects.end(); ++it)
		{
			m_update(*it);
			//T obj = it->second;
			//agx::AffineMatrix4x4 m = obj->getTransform();

			//std::shared_ptr<Node> node = it->first;
			//node->matrix.set(m.ptr());
		}
	}

	for (NodeSharedPtrVector::const_iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
	{
		(*it)->draw();
	}

}
#endif

