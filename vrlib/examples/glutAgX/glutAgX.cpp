// Always include windows.h BEFORE GLUT

#include <GL/glew.h>

#ifdef _WIN32
#define  _CRT_SECURE_NO_WARNINGS 1

#include <windows.h>
#endif
#include <GL/glut.h>

#include <vr/Matrix.h>
#include <vr/Vec3.h>

#include <iostream>
#include <list>
#include <numeric>

#include <vr/FileSystem.h>

#include <vr/ObjLoader.h>
#include <vr/glErrorUtil.h>
#include "BallController.h"
#include <vr/Timer.h>
#include <vr/DrawText.h>
#include <sstream>

#include "Geometry.h"


#include <agx/RigidBody.h>
#include <agxCollide/Geometry.h>
#include <agxCollide/Sphere.h>
#include <agxCollide/Box.h>
#include <agxSDK/Simulation.h>
#include "Scene.h"


void updateTransforms(std::pair<std::shared_ptr<Node>, agxCollide::Geometry *>& pair)
{
	agxCollide::Geometry *obj = pair.second;
	agx::AffineMatrix4x4 m = obj->getTransform();

	std::shared_ptr<Node> node = pair.first;
	node->matrix.set(m.ptr());
}


Scene<agxCollide::Geometry *> g_scene(updateTransforms);
agxSDK::Simulation *g_simulation = 0L;

void
idle()
{
  // update data
  glutPostRedisplay();
}

void initLight()
{
	
	const GLfloat light_ambient[] = { 0.1f, 0.1f, 0.1f, 1.0f };
	const GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	vr::Vec4 light_position(5, 5, 5, 1.0);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	//glEnable(GL_COLOR_MATERIAL);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position.ptr());

	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
	vr::Vec4 v =light_position *(-1);
	v[3] = 1;
	glLightfv(GL_LIGHT1, GL_POSITION, v.ptr());



	GLfloat globalAmbient[] = { 0.1, 0.1, 0.1 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbient);
}

void 
initDefaultMaterial()
{
	const GLfloat mat_ambient[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	const GLfloat mat_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat high_shininess[] = { 100.0f };


	// Default material properties
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
}

void
display()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	initLight();

	initDefaultMaterial();

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glMultMatrixf(g_scene.modelView.ptr());
	
	// Control the camera
	g_scene.ballController.applyTransform();
	
	// Draw the FPS count
	g_scene.drawFPS();

	// Good test model!
	//glutSolidTeapot(1.0);
	
	g_simulation->stepForward();
	// Iterate over all Geometries and render them
	g_scene.update();


	// Draw the origo
	g_scene.drawOrigo();


	// Ok we are done
  glFinish();
  glutSwapBuffers();
  glutIdleFunc( idle );
};


// We will come here if the window is resized
void reshape(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	float ratio = 1.0* w / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	g_scene.ballController.clientAreaResize(w, h);

	// Set the correct perspective.
	float farDistance = g_scene.getMaxDistance();
	gluPerspective(45, ratio, 0.1, farDistance*4);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}





// Get here if we move the mouse
void
motion(int x, int y)
{
	g_scene.ballController.mouseMove(x, y);
}


// Get here if we press a mouse button
void
mouse(int button, int state, int x, int y)
{
	int modifiers = glutGetModifiers();
	bool shiftDown = (modifiers & GLUT_ACTIVE_SHIFT) != 0;
	bool ctrlDown = (modifiers & GLUT_ACTIVE_CTRL) != 0;

	switch (button) {

		// Rotate the scene
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN) {

			if (ctrlDown)
				g_scene.ballController.mouseDown(x, y, BallController::MIDDLE);
			else if (shiftDown)
				g_scene.ballController.mouseDown(x, y, BallController::RIGHT);
			else
				g_scene.ballController.mouseDown(x, y, BallController::LEFT);
		}
		else
		{
			if (ctrlDown)
				g_scene.ballController.mouseUp(x, y, BallController::MIDDLE);
			else if (shiftDown)
				g_scene.ballController.mouseUp(x, y, BallController::RIGHT);
			else
				g_scene.ballController.mouseUp(x, y, BallController::LEFT);
		}

		break;

		// Pan & Zoom
	case GLUT_MIDDLE_BUTTON:

		if (shiftDown)
		{
			if (state == GLUT_DOWN)
				g_scene.ballController.mouseDown(x, y, BallController::RIGHT);
			else
				g_scene.ballController.mouseUp(x, y, BallController::RIGHT);
			break;
		}
		else
		{
			if (state == GLUT_DOWN)
				g_scene.ballController.mouseDown(x, y, BallController::MIDDLE);
			else
				g_scene.ballController.mouseUp(x, y, BallController::MIDDLE);
			break;
		}
	default:
		break;
	}
}


void loadScene();


// Get here if a keyboard key is pressed
void
keyboard(unsigned char c, int x, int y)
{
	g_scene.ballController.key(c);

	std::cout << "Key pressed at "
		<< x << "," << y << ": " << c << "(" << (int)c << ")" << std::endl;
	switch (c) {

	// Reload the scene
	case 'r':
		loadScene();
		break;
		// Quit
	case 'Q':
	case 'q':
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}


// Get here if a special key is pressed
void
special(int c, int /*x*/, int /*y*/)
{
	g_scene.ballController.key(c);

	switch (c) {
	case GLUT_KEY_F1:
		std::cout << "F1" << std::endl;
		break;
	case GLUT_KEY_F2:
		std::cout << "F2" << std::endl;
		break;
	case GLUT_KEY_HOME:
		std::cout << "Home" << std::endl;
		break;
	case GLUT_KEY_END:
		std::cout << "End" << std::endl;
		break;
	case GLUT_KEY_UP:
		std::cout << "Up" << std::endl;
		break;
	case GLUT_KEY_DOWN:
		std::cout << "Down" << std::endl;
		break;
	default:
		break;
	}
}


// Available menu actions
enum MenuItems
{
	TOGGLE_NORMALS,
	TOGGLE_DRAW_ORIGO,
	RESET_CAMERA,
	RELOAD_SCENE,
	TOGGLE_USE_ORIGO_AS_ROTATION_CENTER,
	TOGGLE_DRAW_FPS
};


// Come here if a menu item is selected
// val will have one of the values from the MenuItems enum
void menuCallback(int val)
{

	if (val == TOGGLE_NORMALS)
	{

		bool f = g_scene.getEnableDrawNormals();
		g_scene.setEnableDrawNormals(!f);
	}

	if (val == TOGGLE_DRAW_ORIGO)
	{
		bool f = g_scene.shouldDrawOrigo;
		g_scene.setShouldDrawOrigo(!f);
	}

	if (val == TOGGLE_DRAW_FPS)
	{
		bool f = g_scene.shouldDrawFPS;
		g_scene.setShouldDrawFPS(!f);
	}


	if (val == RESET_CAMERA)
	{
		vr::Vec3 center = g_scene.getCenter();
		g_scene.modelView.makeLookAt(vr::Vec3(center[0], center[1] - g_scene.getMaxDistance() * 2, center[2]), center, vr::Vec3(0, 0, 1));
		g_scene.ballController.home();
	}

	if (val == RELOAD_SCENE)
	{
		loadScene();
	}

	if (val == TOGGLE_USE_ORIGO_AS_ROTATION_CENTER)
	{
		vr::Vec3 center = g_scene.getCenter();
		vr::Vec3 homeCenter = g_scene.ballController.getCenter();

		if (center == homeCenter)
		{
			g_scene.ballController.setHome(vr::Vec3(), vr::Vec3(), vr::Quat(vr::Vec3::X_AXIS(), vr::Vec3::X_AXIS()));
		}
		else
			g_scene.ballController.setHome(vr::Vec3(), center, vr::Quat(vr::Vec3::X_AXIS(), vr::Vec3::X_AXIS()));

	}
}


int
main( int argc, char** argv )
{

  glutInit( &argc, argv );

  glutInitDisplayMode( GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE );

  glutInitWindowSize( 640, 480 );
  glutInitWindowPosition( 200, 50 );
  glutCreateWindow( "GLUT Viewer" );

  glutDisplayFunc( display );

  glutKeyboardFunc( keyboard );
  glutSpecialFunc( special );
  glutMouseFunc( mouse );
  glutMotionFunc( motion );
	glutReshapeFunc(reshape);

	glutCreateMenu(menuCallback);
	glutAddMenuEntry("Toggle normals", TOGGLE_NORMALS);
	glutAddMenuEntry("Toggle origo", TOGGLE_DRAW_ORIGO);
	glutAddMenuEntry("Toggle FPS", TOGGLE_DRAW_FPS);
	glutAddMenuEntry("Reset camera", RESET_CAMERA);
	glutAddMenuEntry("Reload scene", RELOAD_SCENE);
	glutAddMenuEntry("Toggle origo as rotation center", TOGGLE_USE_ORIGO_AS_ROTATION_CENTER);

	glutAttachMenu(GLUT_RIGHT_BUTTON);

	glewInit();

	// Ensure we have the necessary OpenGL Shading Language extensions.
	if (glewGetExtension("GL_ARB_fragment_shader") != GL_TRUE ||
		glewGetExtension("GL_ARB_vertex_shader") != GL_TRUE ||
		glewGetExtension("GL_ARB_shader_objects") != GL_TRUE ||
		glewGetExtension("GL_ARB_shading_language_100") != GL_TRUE)
	{
		std::cerr << "Driver does not support OpenGL Shading Language" << std::endl;
		exit(1);
	}


	agx::AutoInit init;
	agxSDK::SimulationRef simulation = new agxSDK::Simulation;
	g_simulation = simulation.get();

	loadScene();
	
	vr::Vec3 center = g_scene.getCenter();

	g_scene.modelView.makeLookAt(vr::Vec3(center[0], center[1] - g_scene.getMaxDistance()*2 , center[2]), center, vr::Vec3(0, 0, 1));


	g_scene.ballController.setDrawConstraints(true);
	g_scene.ballController.setHome(vr::Vec3(), center, vr::Quat(vr::Vec3::X_AXIS(), vr::Vec3::X_AXIS()));
	g_scene.ballController.home();


	// Start the app
  glutMainLoop();
}


std::shared_ptr<Node> createVisualObject(agx::RigidBody *body)
{

	std::shared_ptr<Node> return_node;

	const agxCollide::GeometryRefVector& geometries = body->getGeometries();
	for (agxCollide::GeometryRefVector::const_iterator cit = geometries.begin(); cit != geometries.end(); ++cit)
	{
		agxCollide::Geometry *geometry = *cit;
		size_t i = 0; // Assume only one shape per Geometry

		agx::AffineMatrix4x4 shapeTransform = geometry->getShapeTransforms()[i] ? (*geometry->getShapeTransforms()[i]) : agx::AffineMatrix4x4();
		agxCollide::Shape *shape = geometry->getShapes()[i];
		switch (shape->getType())
		{
		case agxCollide::Shape::BOX:
		{
			const agxCollide::Box* agxBox = static_cast<const agxCollide::Box*>(shape);

			GeometrySharedPtrVector visualGeometries;
			Geometry::loadObjFile("models/box.obj", visualGeometries);

			std::shared_ptr<Node> node(new Node(visualGeometries));
			return_node = node;

			agx::Vec3 size = agxBox->getHalfExtents();
			node->setScale(vr::Vec3(size[0] * 2, size[1] * 2, size[2] * 2));
			g_scene.add(node, geometry);
		}
		break;

		case agxCollide::Shape::SPHERE:
		{

			break;
		}
		}
	}

	return return_node;
}

// Load the files into the scene
void loadScene()
{
	g_scene.init();
	g_simulation->cleanup(agxSDK::Simulation::CLEANUP_ALL);

	{
		agx::RigidBodyRef body = new agx::RigidBody();
		agxCollide::GeometryRef geom = new agxCollide::Geometry;
		agxCollide::ShapeRef shape = new agxCollide::Box(agx::Vec3(3, 3, 0.1));
		geom->add(shape);
		body->add(geom);
		body->setPosition(0, 0, -0.1);

		body->setMotionControl(agx::RigidBody::STATIC);
		g_simulation->add(body);

		std::shared_ptr<Node> node = createVisualObject(body);
		if (!node)
		{
			std::cerr << "Error creating visual for a rigid body. Using a shape which has not been implemented?" << std::endl;
		}
	}

	for (size_t i = 0; i < 5; i++)
	{
		agx::RigidBodyRef body = new agx::RigidBody();
		agxCollide::GeometryRef geom = new agxCollide::Geometry;
		agxCollide::ShapeRef shape = new agxCollide::Box(agx::Vec3(0.2, 0.2, 0.2));
		geom->add(shape);
		body->add(geom);
		body->setPosition(i*0.1, 0, 0.5*i);

		g_simulation->add(body);

		std::shared_ptr<Node> node = createVisualObject(body);
		if (!node)
		{
			std::cerr << "Error creating visual for a rigid body. Using a shape which has not been implemented?" << std::endl;
		}
	}

	g_scene.ballController.setTranslateScale(g_scene.getMaxDistance()*0.3);
}