#version 120

varying vec3 N;
varying vec3 V;
uniform sampler2D tex0;
uniform int hasTexture0=0;

void main(void)
{
  vec4 Idiff, Iamb;
  vec3 Ispec;

  // Only alpha in diffuse color should be used for transparency
  float a = gl_FrontMaterial.diffuse.a;

  for(int i=0; i < 2; i++) {

    vec3 L = normalize(gl_LightSource[i].position.xyz - V);   
	  vec3 d3 = gl_FrontLightProduct[i].diffuse.rgb;

	  vec4 d =  vec4(d3* max(dot(N,L), 0.0),0);  
	  Idiff += clamp(d, 0.0, 1.0); 
	

	  vec3 E = normalize(-V); // we are in Eye Coordinates, so EyePos is (0,0,0)  
	  vec3 R = normalize(-reflect(L,N));  

	  //calculate Ambient Term:  
	  Iamb += gl_FrontLightProduct[i].ambient;  

	  // calculate Specular Term:
	  vec3 s = gl_FrontLightProduct[i].specular.rgb
					* pow(max(dot(R,E),0.0),gl_FrontMaterial.shininess);
	  Ispec += clamp(s, 0.0, 1.0); 
  }

  if (hasTexture0 != 0)
  {
    vec4 diffTex = texture2D(tex0, gl_TexCoord[0].st);
    Idiff *= diffTex;
  }

  Idiff.a = a;

  // write Total Color:
  vec4 color = vec4(gl_FrontLightModelProduct.sceneColor.rgb,0) + vec4(Iamb.rgb,0) + Idiff + vec4(Ispec,0);  
   
  gl_FragColor = color;     
}
