#version 120

varying vec3 N;
varying vec3 V;

void main(void)
{
   V = vec3(gl_ModelViewMatrix * gl_Vertex);       
   N = gl_NormalMatrix * gl_Normal;
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
   gl_TexCoord[0] = gl_MultiTexCoord0;
}
