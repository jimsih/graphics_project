# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/AC3DLoader.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/AC3DLoader.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/BMPReader.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/BMPReader.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/DrawText.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/DrawText.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/FIleSystem.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/FIleSystem.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/FrameBufferObject.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/FrameBufferObject.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/Image.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/Image.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/JPGReader.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/JPGReader.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/Matrix.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/Matrix.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/ObjLoader.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/ObjLoader.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/PNGReader.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/PNGReader.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/Quat.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/Quat.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/RenderBuffer.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/RenderBuffer.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/Timer.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/Timer.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/glErrorUtil.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/glErrorUtil.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/glUtils.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/glUtils.cpp.o"
  "/home/tfy12/tfy12jsg/Documents/vrlib/src/shaderUtils.cpp" "/home/tfy12/tfy12jsg/Documents/vrlib/src/CMakeFiles/vrlib.dir/shaderUtils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "HAVE_FREETYPE"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/usr/include/freetype2"
  "/usr/include/freetype2/freetype2"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
