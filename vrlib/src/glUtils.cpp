#include <vr/glUtils.h>

#include <vr/PNGReader.h>
#include <vr/BMPReader.h>
#include <vr/JPGReader.h>
#include <vr/FileSystem.h>
#include <algorithm>
#include <iostream>


// Function that loads a bmp/png file and creates and returns a 2d texture
GLuint vr::createTexture(const std::string& image_path)
{
	GLuint id;

	std::string type = vr::FileSystem::getFileExtension(image_path);
	std::transform(type.begin(), type.end(), type.begin(), ::toupper);

	std::string nativePath = vr::FileSystem::convertToNativeFilePath(image_path);
	vr::ImageReader *reader;
	if (type == "BMP")
		reader = new vr::BMPReader();
	else if (type == "PNG")
		reader = new vr::PNGReader();
	else if (type == "JPG")
		reader = new vr::JPGReader();
	else
	{
		std::cerr << "Invalid image type: " << image_path << std::endl;
		return 0;
	}

	vr::Image *image = reader->readImage(nativePath);
	if (!image){
		std::cerr << "Unable to load image: " << image_path << std::endl;
		delete reader;
		return 0;
	}

	delete reader;

	// allocate a texture name
	glGenTextures(1, &id);

	// select the current texture
	glBindTexture(GL_TEXTURE_2D, id);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);



	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_NEAREST);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
		GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
		GL_REPEAT);

	// build our texture mipmaps
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, image->width(), image->height(),
		GL_RGBA, GL_UNSIGNED_BYTE, image->data());

	glBindTexture(GL_TEXTURE_2D, 0);

	delete image;

	return id;
}
