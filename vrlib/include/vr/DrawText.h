#ifndef __DRAW_TEXT_H__
#define __DRAW_TEXT_H__
#include <vr/Vec4.h>

namespace vr
{
	class Text
	{
	public:
		static void drawText(float x, float y, const char *s);
		static void setFontSize(int size);
		static void setColor(const vr::Vec4& color);
	};

}

#endif
