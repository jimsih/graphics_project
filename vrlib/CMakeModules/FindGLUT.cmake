#
# Try to find GLUT library and include path.
# Once done this will define
#
# GLUT_FOUND
# GLUT_INCLUDE_DIR
# GLUT_LIBRARY
# 
	
IF (WIN32)
	FIND_PATH( GLUT_INCLUDE_DIR GL/glut.h
		$ENV{GLUT_ROOT_PATH}/include
		DOC "The directory where GL/glut.h resides")
	FIND_LIBRARY( GLUT_LIBRARY
		NAMES glut GLUT glut32 glut32s
		PATHS
		$ENV{PROGRAMFILES}/GLUT/lib	
		$ENV{GLUT_ROOT_PATH}/lib

		DOC "The GLUT library")


ELSE (WIN32)
	FIND_PATH( GLUT_INCLUDE_DIR GL/glut.h
		/usr/include
		/usr/local/include
		/sw/include
		/opt/local/include
		DOC "The directory where GL/glut.h resides")
	FIND_LIBRARY( GLUT_LIBRARY
		NAMES GLUT glut
		PATHS
		/usr/lib64
		/usr/lib
		/usr/local/lib64
		/usr/local/lib
		/sw/lib
		/opt/local/lib
		DOC "The GLUT library")
ENDIF (WIN32)


IF (GLUT_INCLUDE_DIR)
	SET( GLUT_FOUND 1 CACHE STRING "Set to 1 if GLUT is found, 0 otherwise")
ELSE (GLUT_INCLUDE_DIR)
	SET( GLUT_FOUND 0 CACHE STRING "Set to 1 if GLUT is found, 0 otherwise")
ENDIF (GLUT_INCLUDE_DIR)

MARK_AS_ADVANCED( GLUT_FOUND )
