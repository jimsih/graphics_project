# Locate PNG under windows
# This module defines
# PNG_LIBRARY
# PNG_FOUND, if false, do not try to link to CPPUNIT
# PNG_PNG_INCLUDE_DIR, where to find the headers
#
# $CPPUNIT_DIR is an environment variable that would
# correspond to the ./configure --prefix=$CPPUNIT_DIR
#

INCLUDE(FindZLIBWIN32)


FIND_PATH(PNG_PNG_INCLUDE_DIR png.h
		$ENV{PNG_DIR}/include
		DOC "The directory where png.h resides"
)



FIND_LIBRARY(PNG_LIBRARY libpng
	$ENV{PROGRAMFILES}/lib	
	$ENV{PNG_DIR}/lib

)

IF (PNG_LIBRARY AND PNG_PNG_INCLUDE_DIR)
  # png.h includes zlib.h. Sigh.
  SET(PNG_INCLUDE_DIR ${PNG_PNG_INCLUDE_DIR} ${ZLIB_INCLUDE_DIR} )
  SET(PNG_LIBRARIES ${PNG_LIBRARY} ${ZLIB_LIBRARY})
  SET(PNG_FOUND "YES")

ENDIF (PNG_LIBRARY AND PNG_PNG_INCLUDE_DIR)


IF (PNG_FOUND)
  IF (NOT PNG_FIND_QUIETLY)
    MESSAGE(STATUS "Found PNG: ${PNG_LIBRARY}")
  ENDIF (NOT PNG_FIND_QUIETLY)
ELSE (PNG_FOUND)
  IF (PNG_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find PNG library")
  ENDIF (PNG_FIND_REQUIRED)
ENDIF (PNG_FOUND)

MARK_AS_ADVANCED(PNG_PNG_INCLUDE_DIR PNG_LIBRARY )
