#version 330 core

in vec3 vPosition;
out vec3 TexCoords;

uniform mat4 P;
uniform mat4 V;


void main()
{
    mat4 view = mat4(mat3(V)); 
    vec4 pos = P * view * vec4(vPosition, 1.0);
    gl_Position = pos.xyww;
    TexCoords = vPosition;
}  
