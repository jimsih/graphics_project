#version 330 core

struct Material 
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gDiffuse;
layout (location = 3) out vec4 gSpecular;
layout (location = 4) out vec3 gAmbient;

in vec3 fN;
in vec3 fPosition;
in vec2 f_texcoord;

uniform sampler2D diffuseTex;
uniform bool hasDiffuseTex;
uniform sampler2D specularTex;
uniform bool hasSpecularTex;

uniform Material material;

void main() {
   
    gPosition = fPosition;
    gNormal = normalize(fN);
    
    gDiffuse = material.diffuse;
    if (hasDiffuseTex) {
        gDiffuse *= texture(diffuseTex, f_texcoord).rgb;
    }
    
    gSpecular.rgb = material.specular;
    if (hasSpecularTex) {
        gSpecular.rgb *= texture(specularTex, f_texcoord).rgb;
    }
    
    /* Make shininess float value in interval (0,1) */
    gSpecular.a = material.shininess / 1000;
    
    gAmbient = material.ambient;

}
