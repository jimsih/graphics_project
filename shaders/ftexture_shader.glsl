#version 330

in vec2 f_texcoord;

out vec4  fColor;

uniform sampler2D diffuseTex;
uniform bool hasDiffuseTex;

void
main()
{

    if (hasDiffuseTex) {
        fColor = texture(diffuseTex, f_texcoord);
    } else {
		fColor = vec4(0, 0, 1.0f, 1.0f);
	}
	 
}
