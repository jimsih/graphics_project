#version 330

in vec3 vPosition;
in vec2 vTexCoord;

out vec2 f_texcoord;

void
main()
{
	gl_Position =  vec4(vPosition, 1.0f);
	f_texcoord = vTexCoord;

}
