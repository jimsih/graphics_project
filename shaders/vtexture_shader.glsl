#version 330

in vec3 vPosition;
in vec2 vTexCoord;

/* output that are interpolated per fragment */
out vec2 f_texcoord;

/* Transformations */
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void
main()
{
	gl_Position =  P*V*M*vec4(vPosition, 1.0f);
	f_texcoord = vTexCoord;

}
