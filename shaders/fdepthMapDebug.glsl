#version 330

out vec4 color;
in vec2 TexCoords;

uniform sampler2D diffuseTex;

void main()
{             
    float depthValue = texture(diffuseTex, TexCoords).r;
    color = vec4(vec3(depthValue), 1.0);
} 
