#version 330

layout (location = 0) out vec4 FragColor;
in vec2 fFragCoord;

void main() {

    float x = gl_FragCoord.x;
    float y = gl_FragCoord.y;
    
    float color = sin(x/6)+cos(y/6);

    FragColor = vec4(0, color, 0, 1.0f);
}
