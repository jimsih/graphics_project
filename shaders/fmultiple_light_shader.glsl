#version 330

#define MAX_LIGHTS 10

struct Material 
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct PointLight 
{
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};


in vec3 fN;
in vec3 fE;
in vec3 fPosition;
in vec2 f_texcoord;
in vec4 ShadowCoord;

out vec4  fColor;

/* Lightning and materials */

uniform Material material;
uniform PointLight pointLights[MAX_LIGHTS];
uniform int numLights;

uniform sampler2D diffuseTex;
uniform bool hasDiffuseTex;
uniform sampler2D specularTex;
uniform bool hasSpecularTex;

uniform bool hasShadowMap;
uniform sampler2DShadow shadowMap;
//uniform sampler2D shadowMap;

/*vec2 poissonDisk[16] = vec2[]( 
   vec2( -0.94201624, -0.39906216 ), 
   vec2( 0.94558609, -0.76890725 ), 
   vec2( -0.094184101, -0.92938870 ), 
   vec2( 0.34495938, 0.29387760 ), 
   vec2( -0.91588581, 0.45771432 ), 
   vec2( -0.81544232, -0.87912464 ), 
   vec2( -0.38277543, 0.27676845 ), 
   vec2( 0.97484398, 0.75648379 ), 
   vec2( 0.44323325, -0.97511554 ), 
   vec2( 0.53742981, -0.47373420 ), 
   vec2( -0.26496911, -0.41893023 ), 
   vec2( 0.79197514, 0.19090188 ), 
   vec2( -0.24188840, 0.99706507 ), 
   vec2( -0.81409955, 0.91437590 ), 
   vec2( 0.19984126, 0.78641367 ), 
   vec2( 0.14383161, -0.14100790 ) 
);*/

vec2 poissonDisk[4] = vec2[](
   vec2( -0.94201624, -0.39906216 ),
   vec2( 0.94558609, -0.76890725 ),
   vec2( -0.094184101, -0.92938870 ),
   vec2( 0.34495938, 0.29387760 )
);

// Returns a random number based on a vec3 and an int.
float random(vec3 seed, int i){
	vec4 seed4 = vec4(seed,i);
	float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
	return fract(sin(dot_product) * 43758.5453);
}

vec3 computeDiffuse(vec3 diffuseProduct, vec3 normal, vec3 lightDirection) {
	float Kd = max(dot(normal, lightDirection), 0.0);
	return Kd*diffuseProduct;
}

vec3 computeSpecular(vec3 specularProduct, vec3 lightDirection, vec3 normal, vec3 eyeDirection, float shininess) {
    vec3 H = normalize(lightDirection + eyeDirection);
    float Ks = pow(max(dot(normal,H), 0.0), shininess);
    return Ks*specularProduct;
}

void
main()
{

	vec3 N = normalize(fN);
	vec3 E = normalize(fE);
	
	vec3 ambient = vec3(0.0f, 0.0f, 0.0f);
	vec3 diffuse = vec3(0.0f, 0.0f, 0.0f);
	vec3 specular = vec3(0.0f, 0.0f, 0.0f);
  
    for (int i=0; i<numLights; i++) {
        vec3 L = normalize(pointLights[i].position - fPosition);
        ambient += material.ambient*pointLights[i].ambient;
        diffuse += computeDiffuse(material.diffuse*pointLights[i].diffuse, N, L);
        specular += computeSpecular(material.specular*pointLights[i].specular, L, N, E, material.shininess);
        
    }
    
    float bias = 0.005; //*tan(acos(dot(N, normalize(pointLights[0].position - fPosition))));
    //bias = clamp(bias, 0,0.01);
    float visibility = 1.0;
    
    if (hasShadowMap) {
    /* shadowSampler */
        for (int i=0; i<4; i++) {
            int index = i;  //int(4.0*random(floor(fPosition.xyz*1000.0), i))%4;
            visibility -= 0.1*(1.0-texture( shadowMap, vec3(ShadowCoord.xy + poissonDisk[index]/700.0, (ShadowCoord.z-bias)/ShadowCoord.w) ));
       }
    }
      
    //for (int i=0; i<4; i++) {
          /* regular sampler */
    //    if ( texture( shadowMap, ShadowCoord.xy + poissonDisk[i]/700.0).z <  ShadowCoord.z - bias){
    //        visibility -= 0.2;
    //    }
   // }
   
    if (hasDiffuseTex) {
        diffuse *= texture(diffuseTex, f_texcoord).rgb;
    }
    
    if (hasSpecularTex) {
        specular *= texture(specularTex, f_texcoord).rgb;
    }
    
    vec3 color = ambient + visibility*diffuse + visibility*specular;
    fColor = vec4(color, 1.0);
    //fColor = vec4((ambient+diffuse+specular), 1.0);



	 
}
