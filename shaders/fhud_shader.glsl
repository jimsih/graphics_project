#version 330

in vec2 f_texcoord;

out vec4  fColor;

uniform sampler2D diffuseTex;

void
main()
{
    fColor = texture(diffuseTex, f_texcoord);
}
