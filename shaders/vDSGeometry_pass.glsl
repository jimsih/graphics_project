#version 330

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

/* output that are interpolated per fragment */
out vec3 fN;
out vec3 fPosition;
out vec2 f_texcoord;

/* Transformations */
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void
main()
{
	vec4 wPosition = M*vec4(vPosition, 1.0);

	fN = (M*vec4(vNormal, 0.0)).xyz;
	fPosition = wPosition.xyz;
	f_texcoord = vTexCoord;

	gl_Position =  P*V*wPosition;
}
