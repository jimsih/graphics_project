#version 330 core

in vec3 TexCoords;
out vec4 color;

uniform samplerCube diffuseTex;

void main()
{   
    vec3 texC = vec3(TexCoords.x, -TexCoords.y, TexCoords.z);
    color = texture(diffuseTex, texC);
}
