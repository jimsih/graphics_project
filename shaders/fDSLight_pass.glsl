#version 330 core

#define MAX_LIGHTS 10

struct PointLight 
{
    vec3 position;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    
    float constant;
    float linear;
    float quadratic;
};

struct DirectionalLight 
{
    vec3 direction;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

out vec4 FragColor;
in vec2 fTexCoord;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gDiffuse;
uniform sampler2D gSpecular;
uniform sampler2D gAmbient;

uniform bool hasShadowMap;
uniform sampler2DShadow shadowMap;

uniform PointLight pointLights[MAX_LIGHTS];
uniform int numPointLights;

uniform DirectionalLight directionalLights[MAX_LIGHTS];
uniform int numDirectionalLights;

uniform vec3 EyePosition;

uniform mat4 DepthBiasMVP;

vec2 poissonDisk[4] = vec2[](
   vec2( -0.94201624, -0.39906216 ),
   vec2( 0.94558609, -0.76890725 ),
   vec2( -0.094184101, -0.92938870 ),
   vec2( 0.34495938, 0.29387760 )
);

vec3 computeDiffuse(vec3 diffuseProduct, vec3 normal, vec3 lightDirection) {
	float Kd = max(dot(normal, lightDirection), 0.0);
	return Kd*diffuseProduct;
}

vec3 computeSpecular(vec3 specularProduct, vec3 lightDirection, vec3 normal, vec3 eyeDirection, float shininess) {
    vec3 H = normalize(lightDirection + eyeDirection);
    float Ks = pow(max(dot(normal,H), 0.0), shininess);
    return Ks*specularProduct;
}

void main() {
    
    vec3 fPosition = texture(gPosition, fTexCoord).rgb;
    vec3 fNormal = texture(gNormal, fTexCoord).rgb;
    vec3 fDiffuse = texture(gDiffuse, fTexCoord).rgb;
    vec3 fSpecular = texture(gSpecular, fTexCoord).rgb;
    float fShininess = texture(gSpecular, fTexCoord).a * 1000; /* Extract real shininess value */
    vec3 fAmbient = texture(gAmbient, fTexCoord).rgb;
    
    vec3 fEyeDirection = normalize(EyePosition - fPosition);


	vec3 ambient = vec3(0.0f, 0.0f, 0.0f);
	vec3 diffuse = vec3(0.0f, 0.0f, 0.0f);
	vec3 specular = vec3(0.0f, 0.0f, 0.0f);
	
    for (int i=0; i<numDirectionalLights; i++) {
        vec3 L = directionalLights[i].direction;
        ambient += fAmbient*directionalLights[i].ambient;
        diffuse += computeDiffuse(fDiffuse*directionalLights[i].diffuse, fNormal, L);
        specular += computeSpecular(fSpecular*directionalLights[i].specular, L, fNormal, fEyeDirection, fShininess);
        
    }
    
    for (int i=0; i<numPointLights; i++) {
        float distance = length(pointLights[i].position - fPosition);
        float attenuation = 1.0f / (pointLights[i].constant + pointLights[i].linear * distance + pointLights[i].quadratic*distance);
    
        vec3 L = normalize(pointLights[i].position - fPosition);
        ambient += fAmbient*pointLights[i].ambient * attenuation;
        diffuse += computeDiffuse(fDiffuse*pointLights[i].diffuse, fNormal, L) * attenuation;
        specular += computeSpecular(fSpecular*pointLights[i].specular, L, fNormal, fEyeDirection, fShininess) * attenuation;
    }
   
    vec4 ShadowCoord = DepthBiasMVP * vec4(fPosition, 1.0f);
    
    float bias = 0.005;
    float visibility = 1.0;
    
    if (hasShadowMap) {
    /* shadowSampler */
        for (int i=0; i<4; i++) {
            int index = i; 
            visibility -= 0.1*(1.0-texture( shadowMap, vec3(ShadowCoord.xy + poissonDisk[index]/700.0, (ShadowCoord.z-bias)/ShadowCoord.w) ));
        }
    }
    
    vec3 color = ambient + visibility*diffuse + visibility*specular;
    FragColor = vec4(color, 1.0);
}
