#version 330

in vec3 vPosition;
in vec2 vTexCoord;

out vec2 fFragCoord;
void
main()
{
	gl_Position = vec4(vPosition, 1.0f);
	fFragCoord = vTexCoord;
}
