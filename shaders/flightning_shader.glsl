#version 330

struct Material 
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct PointLight 
{
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};


in vec3 fN;
in vec3 fL;
in vec3 fE;
in vec2 f_texcoord;

out vec4  fColor;

/* Lightning and materials */

uniform Material material;
uniform PointLight pointLight;

uniform sampler2D tex;
uniform bool showTex;

vec3 computeDiffuse(vec3 diffuseProduct, vec3 normal, vec3 lightDirection) {
	float Kd = max(dot(normal, lightDirection), 0.0);
	return Kd*diffuseProduct;
}

vec3 computeSpecular(vec3 specularProduct, vec3 lightDirection, vec3 normal, vec3 eyeDirection, float shininess) {
    vec3 H = normalize(lightDirection + eyeDirection);
    float Ks = pow(max(dot(normal,H), 0.0), shininess);
    return Ks*specularProduct;
}

void
main()
{

	vec3 N = normalize(fN);
	vec3 E = normalize(fE);
	vec3 L = normalize(fL);

	vec3 ambient, diffuse, specular;

	ambient = material.ambient*pointLight.ambient;
	diffuse = computeDiffuse(material.diffuse*pointLight.diffuse, N, L);
	specular = computeSpecular(material.specular*pointLight.specular, L, N, E, material.shininess);

	if (showTex) {
  	fColor = vec4((ambient + diffuse + specular), 1.0) * texture(tex, f_texcoord); 
	} else {
		fColor = vec4((ambient + diffuse + specular), 1.0);
	}
	 
}
