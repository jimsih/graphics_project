#version 330

struct PointLight 
{
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

/* output that are interpolated per fragment */
out vec3 fN;
out vec3 fE;
out vec3 fL;
out vec2 f_texcoord;

/* Transformations */
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;


uniform vec3 EyePosition;
uniform PointLight pointLight;

void
main()
{
	vec4 wPosition = M*vec4(vPosition, 1.0);

	fN = (M*vec4(vNormal, 0.0)).xyz;
	fE = EyePosition - wPosition.xyz;
	fL = pointLight.position - wPosition.xyz;

	gl_Position =  P*V*wPosition;
	f_texcoord = vTexCoord;

}
