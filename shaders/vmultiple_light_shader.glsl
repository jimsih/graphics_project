#version 330

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

/* output that are interpolated per fragment */
out vec3 fN;
out vec3 fE;
out vec3 fPosition;
out vec2 f_texcoord;

/* Transformations */
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

uniform vec3 EyePosition;

/* Shadow */
uniform mat4 DepthBiasMVP;
out vec4 ShadowCoord;

void
main()
{
	vec4 wPosition = M*vec4(vPosition, 1.0);

	fN = (M*vec4(vNormal, 0.0)).xyz;
	fE = EyePosition - wPosition.xyz;
	
	fPosition = wPosition.xyz;
	

	gl_Position =  P*V*wPosition;
	f_texcoord = vTexCoord;
	
	ShadowCoord = DepthBiasMVP * vec4(vPosition, 1.0f);

}
