#version 330 core

in vec3 vPosition;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void main() {
    gl_Position = P*V*M * vec4(vPosition,1);
}
