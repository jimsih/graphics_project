/*
 * Transform.h
 *
 *  Created on: Jan 28, 2016
 *      Author: tfy12jsg
 */

#ifndef TRANSFORM_H_
#define TRANSFORM_H_

#include <vr/Matrix.h>
#include "Group.h"
#include "NodeVisitor.h"

class Transform : public Group {

public:
	Transform();
	Transform(vr::Matrix transform);
	virtual ~Transform();

	void acceptVisitor(NodeVisitor *visitor);

	void setTransform(vr::Matrix transform);
	vr::Matrix getTransform();

protected:

private:
	vr::Matrix m_transform;
};


#endif /* TRANSFORM_H_ */
