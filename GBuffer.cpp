/*
 * GBuffer.cpp
 *
 *  Created on: Mar 3, 2016
 *      Author: tfy12jsg
 */

#include <iostream>
#include <GL/glew.h>
#include <vr/glErrorUtil.h>

#include "Globals.h"
#include "GBuffer.h"
#include "GeometryBuilder.h"
#include "Scene.h"
#include "Light.h"
#include "Node.h"

GBuffer::GBuffer() {
	glGenFramebuffers(1, &gBuffer);
	init();
	GeometryBuilder gb;
	screenQuad = gb.buildQuad(1);
}

GBuffer::~GBuffer() {
	// TODO Auto-generated destructor stub
}

void GBuffer::cleanBuffers() {
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
	glDeleteTextures(1, &gPosition);
	glDeleteTextures(1, &gNormal);
	glDeleteTextures(1, &gDiffuse);
	glDeleteTextures(1, &gSpecular);
	glDeleteTextures(1, &gAmbient);

	glDeleteRenderbuffers(1, &rboDepth);
}

void GBuffer::init() {
	cleanBuffers();

	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

	int width = Globals::getInstance()->WIDTH;
	int height = Globals::getInstance()->HEIGHT;

	// Position buffer
	glGenTextures(1, &gPosition);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);

	// Normal buffer
	glGenTextures(1, &gNormal);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);

	// Diffuse buffer
	glGenTextures(1, &gDiffuse);
	glBindTexture(GL_TEXTURE_2D, gDiffuse);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gDiffuse, 0);

	// Specular buffer
	glGenTextures(1, &gSpecular);
	glBindTexture(GL_TEXTURE_2D, gSpecular);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, gSpecular, 0);

	// Ambient buffer
	glGenTextures(1, &gAmbient);
	glBindTexture(GL_TEXTURE_2D, gAmbient);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, gAmbient, 0);

    GLuint attachments[5] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2,
    						GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4};
    glDrawBuffers(5, attachments);

    // Create and attach depth buffer (renderbuffer)
    glGenRenderbuffers(1, &rboDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);

    // Check if framebuffer is complete
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

GLuint GBuffer::getBuffer(GBUFFER_TYPE buffer) {
	switch (buffer) {
	case GBUFFER_POSITION: return gPosition;
	case GBUFFER_NORMAL: return gNormal;
	case GBUFFER_DIFFUSE: return gDiffuse;
	case GBUFFER_SPECULAR: return gSpecular;
	case GBUFFER_AMBIENT: return gAmbient;
	default: return 0;
	}
}

void GBuffer::apply() {
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
}

void GBuffer::copyDepthBuffer(GLuint framebuffer) {
	int width = Globals::getInstance()->WIDTH;
	int height = Globals::getInstance()->HEIGHT;
	glBindFramebuffer(GL_READ_FRAMEBUFFER, gBuffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
	glBlitFramebuffer(
		0, 0, width, height, 0, 0, width, height, GL_DEPTH_BUFFER_BIT, GL_NEAREST
	);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
}

void GBuffer::draw() {
	Scene* scene = Scene::getInstance();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLuint program = Globals::getInstance()->getProgram("lightPassProgram");
	glUseProgram(program);

	glUniform1i(glGetUniformLocation(program, "gPosition"), 0);
	glUniform1i(glGetUniformLocation(program, "gNormal"), 1);
	glUniform1i(glGetUniformLocation(program, "gDiffuse"), 2);
	glUniform1i(glGetUniformLocation(program, "gSpecular"), 3);
	glUniform1i(glGetUniformLocation(program, "gAmbient"), 4);
	glUniform1i(glGetUniformLocation(program, "shadowMap"), 5);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gPosition);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, gDiffuse);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, gSpecular);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, gAmbient);
	scene->getShadowMap()->getTexture()->apply(5);

	if (scene->isRenderShadow()) {
		glUniform1i(glGetUniformLocation(program, "hasShadowMap"), true);
		vr::Matrix depthBiasMVP = scene->getShadowBiasMatrix(0);
		glUniformMatrix4fv(glGetUniformLocation(program, "DepthBiasMVP"), 1, GL_FALSE, depthBiasMVP.ptr());
	} else {
		glUniform1i(glGetUniformLocation(program, "hasShadowMap"), false);
	}

	scene->applyLightsources(program);

	scene->getMainCamera()->apply(program);

	screenQuad->draw(program);

	glUseProgram(0);
}
