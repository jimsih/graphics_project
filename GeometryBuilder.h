/*
 * GeometryBuilder.h
 *
 *  Created on: Feb 16, 2016
 *      Author: tfy12jsg
 */

#ifndef GEOMETRYBUILDER_H_
#define GEOMETRYBUILDER_H_

#include <memory>

#include "Geometry.h"

class GeometryBuilder {
public:
	GeometryBuilder();
	virtual ~GeometryBuilder();

	std::shared_ptr<Geometry> buildQuad(float size);
};

#endif /* GEOMETRYBUILDER_H_ */
