/*
 * HUD.cpp
 *
 *  Created on: Mar 1, 2016
 *      Author: tfy12jsg
 */

#include "HUD.h"

#include "GeometryBuilder.h"
#include "shadertools.h"
#include "UpdateCallback.h"

HUD::HUD() {
	hudProgram = initProgram("shaders/vhud_shader.glsl", "shaders/fhud_shader.glsl");
}

HUD::~HUD() {
	// TODO Auto-generated destructor stub
}

/* Position, width, and height are in screen coordinates ( -1 to 1) */
void HUD::addItem(vr::Vec2 position, float width, float height, Texture* texture) {
	vr::Vec3Vector v_quad;
	vr::UIntVector i_quad;
	vr::Vec2Vector t_quad;

	v_quad.push_back(vr::Vec3(position.x() - width/2, position.y() - height/2, 0));
	v_quad.push_back(vr::Vec3(position.x() + width/2, position.y() - height/2, 0));
	v_quad.push_back(vr::Vec3(position.x() - width/2, position.y() + height/2, 0));
	v_quad.push_back(vr::Vec3(position.x() + width/2, position.y() + height/2, 0));

	i_quad.push_back(0);
	i_quad.push_back(1);
	i_quad.push_back(2);
	i_quad.push_back(1);
	i_quad.push_back(2);
	i_quad.push_back(3);

	t_quad.push_back(vr::Vec2(0, 0));
	t_quad.push_back(vr::Vec2(1.0f, 0));
	t_quad.push_back(vr::Vec2(0, 1.0f));
	t_quad.push_back(vr::Vec2(1.0f, 1.0f));

	shared_ptr<Geometry> geom(new Geometry);
	geom->setVertices(v_quad);
	geom->setIndices(i_quad);
	geom->setTexCoords(t_quad);

	displays.push_back(pair<shared_ptr<Geometry>, Texture*>(geom, texture));
}

void HUD::draw() {
	glDisable(GL_DEPTH_TEST);
	glUseProgram(hudProgram);

	for (pair<shared_ptr<Geometry>, Texture*> &display : displays) {
		if (display.first->hasUpdateFunction()) {
			shared_ptr<Node> node(display.first);
			display.first->getUpdateFunction()(node);
		}
		display.second->apply(0);
		display.first->draw(hudProgram);
	}

	glUseProgram(0);
	glEnable(GL_DEPTH_TEST);
}
