/*
 * NodeVisitor.h
 *
 *  Created on: Jan 28, 2016
 *      Author: tfy12jsg
 */

#ifndef NODEVISITOR_H_
#define NODEVISITOR_H_


class Group;
class Camera;
class Geometry;
class Transform;
class RenderToTexture;

#include <memory>

using namespace std;

class NodeVisitor {

public:
	virtual ~NodeVisitor() {};

	virtual void traverseScene() =0;
	virtual void visit(shared_ptr<Group> group);
	virtual void visit(shared_ptr<Camera> camera) =0;
	virtual void visit(shared_ptr<Transform> transform) =0;
	virtual void visit(shared_ptr<Geometry> geometry) =0;
	virtual void visit(shared_ptr<RenderToTexture> render2Texture) =0;
};



#endif /* NODEVISITOR_H_ */
