#include <vr/Vec3.h>
#include <vr/Timer.h>
#include <vr/DrawText.h>
#include <vr/FileSystem.h>
#include <vr/ObjLoader.h>
#include <vr/glUtils.h>
#include <string>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include "Globals.h"
#include "Reader.h"
#include "Camera.h"
#include "shadertools.h"
#include "State.h"
#include "Light.h"
#include "InputHandler.h"
#include "Transform.h"
#include "Texture.h"
#include "TextureSkyBox.h"
#include "GeometryBuilder.h"
#include "PostProcessor.h"
#include "ProceduralTexture.h"

#include "Scene.h"

using namespace std;

void reshape(int width, int height) {
	glViewport(0,0,width,height);
	Globals::getInstance()->WIDTH = width;
	Globals::getInstance()->HEIGHT = height;
	Scene::getInstance()->screenSizeUpdate();
	glutPostRedisplay();
}

void display() {
	Scene::getInstance()->draw();
}

static void timerCallback (int value)
{
	display();
	glutTimerFunc(15, timerCallback, 0);
	//glutIdleFunc(display);
}

void mouseTracker(int x, int y) {
	InputHandler::getInstance()->mouseListener(x, y);
}

void mouseClickListener(int button, int state, int x, int y) {
	InputHandler::getInstance()->mouseClickListener(button, state, x, y);
}

void keyboardListener(unsigned char key, int x, int y) {
	if (key == '\e') {
		glutExit();
	}

	vector<shared_ptr<Node>> lights = Scene::getInstance()->getLightSources();
	if (isdigit(key)) {
		unsigned int nr = key - '0';

		if (lights.size() > nr) {
			Light* light = (Light*)lights[nr].get();
			light->setActive(!light->isActive());

			if (nr == 0) {
				Scene::getInstance()->setRenderShadow(!Scene::getInstance()->isRenderShadow());
			}
		}
	}


	InputHandler::getInstance()->keyboardListener(key, x, y);
}

void initGlut(int argc, char **argv) {
	/* Initialize glut */
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize(Globals::getInstance()->WIDTH, Globals::getInstance()->HEIGHT);
	glutInitContextVersion(3, 1);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow("SceneGraph");
	glewExperimental = GL_TRUE;

	glewInit();
	if (glewGetExtension("GL_ARB_draw_buffers") != GL_TRUE) {
		std::cerr << "glDrawbuffers is not supported" << std::endl;
	}

	/* Set graphics attributes */
	glLineWidth(1.0);
	glPointSize(20.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);

	/* Initialize callback funcs */
	glutDisplayFunc(display);
	glutTimerFunc(15, timerCallback, 0);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboardListener);
//	glutSpecialFunc(specialKeyboardListener);
	glutMotionFunc(mouseTracker);
	glutMouseFunc(mouseClickListener);
}

typedef vector<pair<string,vr::Vec3>> importModelVector;
importModelVector loadableFiles;

void loadScene(importModelVector loadableFiles)
{
	Reader reader;

	for (pair<string,vr::Vec3> &model : loadableFiles)
	{
		size_t numRead = 0;
		std::string path = vr::FileSystem::convertToNativeFilePath(model.first);
		std::cerr << "Loading \'" << path << "\'" << std::endl;
		GeometrySharedPtrVector geometries;
		numRead = reader.loadObjFile(path, geometries);
		if (!numRead)
		{
			std::cerr << "Error loading model: " << path << std::endl;
		}
		else
		{
			for (shared_ptr<Geometry> &geom : geometries) {
				shared_ptr<State> state (new State(new Material(geometries[0]->getMaterial()),
										Globals::getInstance()->getProgram("lightProgram")));
				if (!geom->getMaterial().diffuse_texname.empty()) {
					state->setDiffuseTexture(new Texture(geom->getMaterial().diffuse_texname));
				}
				if (!geom->getMaterial().specular_texname.empty()) {
					state->setSpecularTexture(new Texture(geom->getMaterial().specular_texname));
				}
				geom->setState(state);
			}
			shared_ptr<Transform> T (new Transform());
			T->setTransform(vr::Matrix::translate(model.second));
			for (shared_ptr<Geometry> &geom : geometries) {
				T->addNode(geom);
			}
			Scene::getInstance()->addNode(T);
		}
	}

}

void initMainCamera() {
	shared_ptr<Camera> camera(new Camera(vr::Vec3(0.0f, 10.0f, 30.0f), vr::Vec3(0.0f,0.0f,0.0f), vr::Vec3(0.0f, 1.0f, 0.0f)));
	camera->setMouseMotionFunction(cameraMouseTracker);
	camera->setKeyboardInputFunction(cameraKeyboardInput);
	camera->setUpdateFunction(cameraUpdate);

	InputHandler::getInstance()->addListener(camera);
	Scene::getInstance()->setMainCamera(camera);
}

void createMultipleSpheres() {
	Reader reader;
	GeometrySharedPtrVector geometries;
	size_t numRead = reader.loadObjFile("vrlib/models/sphere.obj", geometries);

	if (numRead == 0) {
		std::cout << "Could not load spheres" << std::endl;
		return;
	}

	shared_ptr<State> state (new State(new Material(geometries[0]->getMaterial()),
							Globals::getInstance()->getProgram("lightProgram")));
	state->setDiffuseTexture(new Texture("textures/Stone_wall.jpg"));

	shared_ptr<Group> G (new Group());
	G->setState(state);

	shared_ptr<Transform> T1 (new Transform(vr::Matrix::translate(vr::Vec3(-1.0f, 0, 0))));
	shared_ptr<Transform> T2 (new Transform(vr::Matrix::translate(vr::Vec3(0.0f, 0, -2.0f))));

	T1->addNode(geometries[0]);
	T2->addNode(geometries[0]);

	T2->setUpdateFunction([](shared_ptr<Node>& node) {
		Transform* T = (Transform*) node.get();
		double dt = Globals::getInstance()->timer.getTime();
		double rotation = 0.5*vr::PI * (dt/1000);
		T->setTransform(T->getTransform() * vr::Matrix::rotate(rotation, vr::Vec3(-1.0f, 1.0f, 0)));
	});

	G->addNode(T1);
	G->addNode(T2);

	Scene::getInstance()->addNode(G);

}

void createRobot() {
	Reader reader;
	GeometrySharedPtrVector geometries;
	size_t numRead = reader.loadObjFile("vrlib/models/5426_C3PO_Robot_Star_Wars.obj", geometries);

	if (numRead == 0) {
		std::cout << "Could not load robot" << std::endl;
		return;
	}

	for (shared_ptr<Geometry> &geom : geometries) {
		shared_ptr<State> state (new State(new Material(geom->getMaterial()),
							Globals::getInstance()->getProgram("lightProgram")));
		geom->setState(state);
	}
	shared_ptr<Transform> T (new Transform());
	T->setTransform(vr::Matrix::scale(0.003f, 0.003f, 0.003f)*vr::Matrix::translate(2.0f, 0.0f, -8.0f));


	for (shared_ptr<Geometry> &geom : geometries) {
		T->addNode(geom);
	}

	Scene::getInstance()->addNode(T);
}

void createFloor() {

	shared_ptr<Transform> T (new Transform());
	T->setTransform(vr::Matrix::rotate(-vr::PI/2, 1.0f, 0.0f, 0)*vr::Matrix::translate(0.0f, -3.0f, 0.0f));

	shared_ptr<Geometry> geom = GeometryBuilder().buildQuad(15.0f);

	shared_ptr<State> state (new State(new Material, Globals::getInstance()->getProgram("lightProgram")));
	state->setDiffuseTexture(new Texture("textures/soilBeach.jpg"));
	geom->setState(state);

	T->addNode(geom);

	Scene::getInstance()->addNode(T);
}

void initLights() {
	shared_ptr<Node> light1 (new Light(Light::DIRECTIONAL, vr::Vec3(30.0f, 30.0f, 0.0f),
			vr::Vec3(0.2f, 0.2f, 0.2f), vr::Vec3(0.8f, 0.8f, 0.1f), vr::Vec3(0.4f, 0.4f, 0.4f)));

	light1->setKeyboardInputFunction([](shared_ptr<Node>& node, unsigned char key, int x, int y) {
		Light* light = (Light*)node.get();
		if (light->isActive()) {
			if (key == 'j') {
				light->setPosition(light->getPosition() + vr::Vec3(-0.2f, 0, 0));
			}
			if (key == 'l') {
				light->setPosition(light->getPosition() + vr::Vec3(0.2f, 0, 0));
			}
			if (key == 'i') {
				light->setPosition(light->getPosition() + vr::Vec3(0, 0, -0.2));
			}
			if (key == 'k') {
				light->setPosition(light->getPosition() + vr::Vec3(0, 0, 0.2f));
			}
			if (key == 'u') {
				light->setPosition(light->getPosition() + vr::Vec3(0, 0.2f, 0));
			}
			if (key == 'o') {
				light->setPosition(light->getPosition() + vr::Vec3(0, -0.2f, 0));
			}
		}

	});

	InputHandler::getInstance()->addListener(light1);

	shared_ptr<Node> light2 (new Light(Light::POINT, vr::Vec3(-10.0f, 2.0f, -5.0f), vr::Vec3(0.2f, 0.0f, 0.0f),
			vr::Vec3(0.9f, 0.1f, 0.1f), vr::Vec3(0.5f, 0.1f, 0.1f)));

	light2->setUpdateFunction([](shared_ptr<Node>& node) {
		Light* L = (Light*) node.get();
		double dt = Globals::getInstance()->timer.getTime();
		double rotation = 0.2*vr::PI * (dt/1000);
		vr::Vec4 v = vr::Matrix::rotate(rotation, vr::Vec3(0, 1.0f, 0))*vr::Vec4(L->getPosition(),1.0f);
		L->setPosition(vr::Vec3(v.x(), v.y(), v.z()));
	});

	shared_ptr<Node> light3 (new Light(Light::POINT, vr::Vec3(0.0f, 0.0f, -5.0f), vr::Vec3(0.2f, 0.0f, 0.0f),
			vr::Vec3(0.1f, 0.9f, 0.1f), vr::Vec3(0.5f, 0.1f, 0.1f)));

	light3->setUpdateFunction([](shared_ptr<Node>& node) {
		Light* L = (Light*) node.get();
		double dt = Globals::getInstance()->timer.getTime();
		double rotation = -0.2*vr::PI * (dt/1000);
		vr::Vec4 v = vr::Matrix::rotate(rotation, vr::Vec3(0, 1.0f, 0))*vr::Vec4(L->getPosition(),1.0f);
		L->setPosition(vr::Vec3(v.x(), v.y(), v.z()));
	});

	shared_ptr<Node> light4 (new Light(Light::POINT, vr::Vec3(-5, 2.0f, 5.0f), vr::Vec3(0.0f, 0.0f, 0.2f),
			vr::Vec3(0.0, 0.0, 1.0f), vr::Vec3(0.0f, 0.0f, 1.0f)));

	shared_ptr<Node> light5 (new Light(Light::POINT, vr::Vec3(10, 2.0f, 10.0f), vr::Vec3(0.0f, 0.0f, 0.2f),
				vr::Vec3(1.0, 140.0f/255, 0), vr::Vec3(1.0, 140.0f/255, 0)));


	Scene* scene = Scene::getInstance();
	scene->addLightSource(light1);
	scene->addLightSource(light2);
	scene->addLightSource(light3);
	scene->addLightSource(light4);
	scene->addLightSource(light5);
}


void createSkyBox() {

	GLuint skyboxProgram = initProgram("shaders/vskybox_shader.glsl", "shaders/fskybox_shader.glsl");
	Reader reader;

	vector<string> faces;
	faces.push_back("textures/skybox/right.jpg");
	faces.push_back("textures/skybox/left.jpg");
	faces.push_back("textures/skybox/bottom.jpg");
	faces.push_back("textures/skybox/top.jpg");
	faces.push_back("textures/skybox/back.jpg");
	faces.push_back("textures/skybox/front.jpg");

	TextureSkyBox* skyboxTexture = new TextureSkyBox();
	skyboxTexture->loadCubemap(faces);

	GeometrySharedPtrVector geoms;
	reader.loadObjFile("vrlib/models/box.obj", geoms);

	shared_ptr<State> state (new State());
	state->setProgram(skyboxProgram);
	state->setDiffuseTexture(skyboxTexture);

	geoms[0]->setState(state);
	Scene::getInstance()->setSkybox(geoms[0]);

}

enum MenuItems
{
	TOGGLE_RENDER_FROM_LIGHT,
	TOGGLE_DEBUG,
	TOGGLE_RENDER_SHADOW,
	TOGGLE_BLUR
};

void menuCallback(int val) {
	Scene* scene = Scene::getInstance();
	if (val == TOGGLE_RENDER_FROM_LIGHT) {
		scene->setRenderFromLight(!scene->isRenderFromLight());
	}

	if (val == TOGGLE_DEBUG) {
		scene->setDebugMode(!scene->getDebugMode());
	}

	if (val == TOGGLE_RENDER_SHADOW) {
		scene->setRenderShadow(!scene->isRenderShadow());
	}

	if (val == TOGGLE_BLUR) {
		scene->getPostProcessor()->setActive(!scene->getPostProcessor()->isActive());
	}

}

void createMenu() {
	glutCreateMenu(menuCallback);
	glutAddMenuEntry("Toggle render from light", TOGGLE_RENDER_FROM_LIGHT);
	glutAddMenuEntry("Toggle debug screens", TOGGLE_DEBUG);
	glutAddMenuEntry("Toggle shadows", TOGGLE_RENDER_SHADOW);
	glutAddMenuEntry("Toggle blur", TOGGLE_BLUR);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char **argv) {

	initGlut(argc, argv);

	createMenu();

	GLuint lightProgram = initProgram("shaders/vmultiple_light_shader.glsl", "shaders/fmultiple_light_shader.glsl");
	GLuint shadowProgram = initProgram("shaders/vshadow_map.glsl", "shaders/fshadow_map.glsl");
	GLuint lightsourceProgram = initProgram("shaders/vlightsource_shader.glsl", "shaders/flightsource_shader.glsl");
	GLuint geomPassProgram = initProgram("shaders/vDSGeometry_pass.glsl", "shaders/fDSGeometry_pass.glsl");
	GLuint lightPassProgram = initProgram("shaders/vDSLight_pass.glsl", "shaders/fDSLight_pass.glsl");
	GLuint blurProgram = initProgram("shaders/vDSLight_pass.glsl", "shaders/fblur_shader.glsl");
	Globals::getInstance()->addProgram("blurProgram", blurProgram);
	Globals::getInstance()->addProgram("lightPassProgram", lightPassProgram);
	Globals::getInstance()->addProgram("geomPassProgram", geomPassProgram);
	Globals::getInstance()->addProgram("lightProgram", lightProgram);
	Globals::getInstance()->addProgram("shadowProgram", shadowProgram);
	Globals::getInstance()->addProgram("lightsourceProgram", lightsourceProgram);

	GeometrySharedPtrVector geoms;
	// 5426_C3PO_Robot_Star_Wars

	importModelVector loadableFiles;

	//loadableFiles.push_back(pair<string, vr::Vec3>("vrlib/models/Paris/paris.obj", vr::Vec3(0, -2.0f, 0)));
	//loadableFiles.push_back(pair<string, vr::Vec3>("vrlib/models/sphere.obj", vr::Vec3(-1.0f, 0, 0)));
	//loadableFiles.push_back(pair<string, vr::Vec3>("vrlib/models/box.obj", vr::Vec3(1.0f, 0, 0)));
	//loadableFiles.push_back(pair<string, vr::Vec3>("vrlib/models/House01/House01.obj", vr::Vec3(0.0f, 0, 0)));

	createMultipleSpheres();
	createRobot();
	createFloor();
	createSkyBox();

	initMainCamera();

	initLights();

	Scene* scene = Scene::getInstance();

//	GLuint depthMapDebug = initProgram("shaders/vdepthMapDebug.glsl", "shaders/fdepthMapDebug.glsl");
	GLuint billboardProgram = initProgram("shaders/vtexture_shader.glsl", "shaders/ftexture_shader.glsl");
	//shared_ptr<Transform> T(new Transform(vr::Matrix::translate(-10.0f, 0, -2.0f)));
	shared_ptr<Geometry> quad = GeometryBuilder().buildQuad(3);
	shared_ptr<State> state (new State());
	state->setProgram(billboardProgram);
	Texture* doom = new Texture("textures/doom.jpg");
	state->setDiffuseTexture(doom);
	quad->setState(state);
	//state->setDiffuseTexture(Scene::getInstance()->getShadowMap()->getTexture());
	//T->setState(state);
	//T->addNode(quad);
	scene->debugQuad = quad;

//	shared_ptr<State> shadowState(new State);
//	shadowState->setProgram(Globals::getInstance()->getShadowMapProgram());
//	scene->setGlobalState(shadowState);
	scene->setRenderShadow(true);

	loadScene(loadableFiles);

	scene->getHUD()->addItem(vr::Vec2(-0.8, -0.8), 0.4, 0.4,
			new Texture(scene->getGBuffer()->getBuffer(GBuffer::GBUFFER_POSITION)));
	scene->getHUD()->addItem(vr::Vec2(-0.4, -0.8), 0.4, 0.4,
			new Texture(scene->getGBuffer()->getBuffer(GBuffer::GBUFFER_NORMAL)));
	scene->getHUD()->addItem(vr::Vec2(0.0, -0.8), 0.4, 0.4,
				new Texture(scene->getGBuffer()->getBuffer(GBuffer::GBUFFER_DIFFUSE)));
	scene->getHUD()->addItem(vr::Vec2(0.4, -0.8), 0.4, 0.4,
					new Texture(scene->getGBuffer()->getBuffer(GBuffer::GBUFFER_SPECULAR)));
	scene->getHUD()->addItem(vr::Vec2(0.8, -0.8), 0.4, 0.4,
						new Texture(scene->getGBuffer()->getBuffer(GBuffer::GBUFFER_AMBIENT)));

	scene->setPostProcessor(new PostProcessor(Globals::getInstance()->getProgram("blurProgram")));

	GLuint procTex = initProgram("shaders/vproceduralTexture_shader.glsl", "shaders/fproceduralTexture_shader.glsl");
	ProceduralTexture pt;
	shared_ptr<Geometry> geom = GeometryBuilder().buildQuad(4.0f);
	pt.apply();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(procTex);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	geom->draw(procTex);
	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	shared_ptr<Transform> T (new Transform());
	T->setTransform(vr::Matrix::translate(0.0f, 10.0f, 5.0f));

	shared_ptr<State> state1 (new State(new Material, billboardProgram));
	state1->setDiffuseTexture(new Texture(pt.getTexture()));
	geom->setState(state1);

	T->addNode(geom);

	Scene::getInstance()->addNode(T);

	Globals::getInstance()->timer.start();
	/* Loop forever */
	glutMainLoop();

	return 0;
}
