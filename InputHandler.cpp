/*
 * InputHandler.cpp
 *
 *  Created on: Feb 5, 2016
 *      Author: tfy12jsg
 */

#include "InputHandler.h"

#include <GL/freeglut.h>

InputHandler* InputHandler::instance = nullptr;

void InputHandler::keyboardListener(unsigned char key, int x, int y) {
	for (shared_ptr<Node> &node : listeners) {
		if (node.get()->hasKeyboardInputFunction()) {
			node.get()->getKeyboardInputFunction()(node, key, x, y);
		}
	}
}

void InputHandler::mouseListener(int x, int y) {
	for (shared_ptr<Node> &node : listeners) {
		if (node.get()->hasMouseMotionFunction()) {
			node.get()->getMouseMotionFunction()(node, x, y);
		}
	}

	mouseX = x;
	mouseY = y;
}

void InputHandler::mouseClickListener(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			mouseX = x;
			mouseY = y;
			clicked = true;
		} else if (state == GLUT_UP) {
			clicked = false;
		}
	}
}

void InputHandler::addListener(shared_ptr<Node> listener) {
	listeners.push_back(listener);
}
