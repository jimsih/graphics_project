/*
 * Camera.cpp
 *
 *  Created on: Jan 21, 2016
 *      Author: tfy12jsg
 */

#include "Camera.h"

#include <iostream>
#include <vr/Matrix.h>
#include <vr/glErrorUtil.h>

#include "UpdateCallback.h"
#include "InputHandler.h"

Camera::Camera() {
	m_position = vr::Vec3(0.0f, 1.0f, 4.0f);
	m_lookAt = vr::Vec3(0.0f, 0.0f, 0.0f);
	m_up = vr::Vec3(0.0f, 1.0f, 0.0f);

	m_startPosition = m_position;
	m_startOrientation = m_lookAt;

	m_view = vr::Matrix::lookAt(m_position, m_lookAt, m_up);
	m_projection = vr::Matrix::perspective(m_fov, Globals::getInstance()->Aspect(), m_near, m_far);
}

Camera::Camera(vr::Vec3 pos, vr::Vec3 lookAt, vr::Vec3 up) {
	m_position = pos;
	m_lookAt = lookAt;
	m_up = up;

	m_startPosition = m_position;
	m_startOrientation = m_lookAt;

	m_view = vr::Matrix::lookAt(m_position, m_lookAt, m_up);
	m_projection = vr::Matrix::perspective(m_fov, Globals::getInstance()->Aspect(), m_near, m_far);
}

Camera::~Camera() {

}

void Camera::acceptVisitor(NodeVisitor *visitor) {
	visitor->visit(std::dynamic_pointer_cast<Camera>(shared_from_this()));
}

void Camera::apply(GLuint program) {
	glUniformMatrix4fv(glGetUniformLocation(program, "V"), 1, GL_FALSE, m_view.ptr());
	glUniformMatrix4fv(glGetUniformLocation(program, "P"), 1, GL_FALSE, m_projection.ptr());
	glUniform3fv(glGetUniformLocation(program, "EyePosition"), 1, m_position.ptr());
	CheckErrorsGL("Camera apply");
}

void Camera::reset() {
	m_position = m_startPosition;
	m_lookAt = m_startOrientation;
}

void Camera::update() {
	m_view = vr::Matrix::lookAt(m_position, m_lookAt, m_up);
	updateProjection();
}

void Camera::updateProjection() {
	if (isPerspective) {
		m_projection = vr::Matrix::perspective(m_fov, Globals::getInstance()->Aspect(), m_near, m_far);
	} else {
		m_projection = vr::Matrix::ortho(-10, 10, -10, 10, -10, 500);
	}
}

void Camera::setProjection(vr::Matrix projection) {
	m_projection = projection;
}

void Camera::usePerspectiveProj(bool b) {
	isPerspective = b;
}

void Camera::setPosition(vr::Vec3 pos) {
	m_position = pos;
}

vr::Vec3 Camera::getPosition() {
	return m_position;
}

void Camera::setLookAt(vr::Vec3 lookAt) {
	m_lookAt = lookAt;
}

void Camera::set_top(float top) {m_top = top;}
void Camera::set_near(float near) {m_near = near;}
void Camera::set_far(float far) {m_far = far;}
void Camera::set_fov(float fov) {m_fov = fov;}

void Camera::translate(const vr::Vec3 &v) {
	vr::Vec3 n =  (m_position-m_lookAt) / (m_position-m_lookAt).length();
	vr::Vec3 u = (m_up^n) / (m_up ^ n).length();
	vr::Vec3 T = u*v[0] + m_up*v[1] + n*v[2];
	m_position = m_position + T;
	m_lookAt = m_lookAt + T;

}

void Camera::rotatex(float rad) {
	vr::Vec3 n = (m_position-m_lookAt) / (m_position-m_lookAt).length();
	vr::Vec3 u = (m_up ^ n) / (m_up ^ n).length();
	m_lookAt = m_lookAt - m_position;
	m_lookAt = vr::Matrix::rotate(vr::Quat(rad,u)) * m_lookAt;
	m_lookAt = m_lookAt + m_position;

}

void Camera::rotatey(float rad) {
	m_lookAt = m_lookAt - m_position;
	m_lookAt = vr::Matrix::rotate(vr::Quat(rad, m_up)) * m_lookAt;
	m_lookAt = m_lookAt + m_position;
}



void cameraMouseTracker(shared_ptr<Node>& camera, int x, int y) {
	Camera* c = (Camera*) camera.get();

	InputHandler* input = InputHandler::getInstance();
	std::pair<int, int> mouseClickPos = input->getLastMousePosition();
	float dx = (x - mouseClickPos.first);
	float dy = (y - mouseClickPos.second);

	float rotationSpeed = 0.15;
	if (dx != 0) {
		rotationSpeed = rotationSpeed * Globals::getInstance()->Aspect();
		c->rotatey(rotationSpeed*dx*vr::PI/360);
	}
	if (dy != 0) {
		c->rotatex(rotationSpeed*dy*vr::PI/360);
	}
}

void cameraKeyboardInput(shared_ptr<Node>& camera, unsigned char key, int x, int y) {
	Camera* c = (Camera*) camera.get();


	if (key == 'r') {
		c->reset();
	}

	/* Movement */
	if (key == 'w') {
		c->translate(vr::Vec3(0.0f, 0.0f, -0.4f));
	} else if (key == 's') {
		c->translate(vr::Vec3(0.0f, 0.0f, 0.4f));
	}
	if (key == 'a') {
		c->translate(vr::Vec3(-0.4f, 0.0f, 0.0f));
	} else if (key == 'd') {
		c->translate(vr::Vec3(0.4f, 0.0f, 0.0f));
	}
	if (key == 'z') {
		c->translate(vr::Vec3(0.0f, 0.4f, 0.0f));
	} else if (key == 'x') {
		c->translate(vr::Vec3(0.0f, -0.4f, 0.0f));
	}
}

void cameraUpdate(shared_ptr<Node>& camera) {
	Camera* c = (Camera*) camera.get();
	c->update();
}
