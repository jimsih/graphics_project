/*
 * State.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: tfy12jsg
 */

#include "State.h"
#include "Scene.h"
#include "Light.h"

#include <vr/glErrorUtil.h>

void State::apply() {
	bindState(m_program);
}

void State::apply(GLuint program) {
	bindState(program);
}

void State::bindState(GLuint program) {
	Scene* scene = Scene::getInstance();

	if (m_material != nullptr) {
		m_material->apply(program);
	}

	scene->applyLightsources(program);

	int nrOfTextures = 0;
	glUniform1i(glGetUniformLocation(program, "hasDiffuseTex"), m_hasDiffuseTex);
	if (m_hasDiffuseTex) {
		glUniform1i(glGetUniformLocation(program, "diffuseTex"), nrOfTextures);
		m_diffuseTexture->apply(nrOfTextures);
		nrOfTextures++;
	}

	glUniform1i(glGetUniformLocation(program, "hasSpecularTex"), m_hasSpecularTex);
	if (m_hasSpecularTex) {
		glUniform1i(glGetUniformLocation(program, "specularTex"), nrOfTextures);
		m_specularTexture->apply(nrOfTextures);
		nrOfTextures++;
	}

	glUniform1i(glGetUniformLocation(program, "shadowMap"), nrOfTextures);
	scene->getShadowMap()->getTexture()->apply(nrOfTextures);
	nrOfTextures++;

	glPolygonMode(GL_FRONT_AND_BACK, polygonMode);
	CheckErrorsGL("State apply");
}

 void State::setPolygonMode(GLenum mode) {polygonMode = mode;}

 void State::setMaterial(Material* material) {
	 delete(m_material);
	 m_material = material;
 }

 void State::setDiffuseTexture(Texture* texture) {
	 if (m_diffuseTexture != nullptr)
		 delete(m_diffuseTexture);

	 m_diffuseTexture = texture;
	 m_hasDiffuseTex = true;
 }

 void State::setSpecularTexture(Texture* texture) {
	 delete(m_specularTexture);
	 m_specularTexture = texture;
	 m_hasSpecularTex = true;
 }






