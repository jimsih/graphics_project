
#include "Geometry.h"

#include <iostream>
#include <GL/glew.h>
#include <vr/glErrorUtil.h>

Geometry::Geometry() {
	glGenVertexArrays(1, &vao);

	CheckErrorsGL("Geometry gen vao");
	glBindVertexArray(0);
}

void Geometry::draw(GLuint program) {
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vertices_vbo);
	GLuint locPosition = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(locPosition);
	glVertexAttribPointer(locPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

	if (m_normals.size() != 0) {
		glBindBuffer(GL_ARRAY_BUFFER, normals_vbo);
		GLuint locNormal = glGetAttribLocation(program, "vNormal");
		if (locNormal < GL_MAX_VERTEX_ATTRIBS) {
			glEnableVertexAttribArray(locNormal);
			glVertexAttribPointer(locNormal, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}
	}

	if (m_texCoords.size() != 0) {
		glBindBuffer(GL_ARRAY_BUFFER, texCoords_vbo);
		GLuint locTex = glGetAttribLocation(program, "vTexCoord");
		if (locTex < GL_MAX_VERTEX_ATTRIBS) {
			glEnableVertexAttribArray(locTex);
			glVertexAttribPointer(locTex, 2, GL_FLOAT, GL_FALSE, 0, 0);
		}
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_ebo);

	glDrawElements(GL_TRIANGLES, (GLsizei)m_indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	CheckErrorsGL("Geometry draw");
}

void Geometry::acceptVisitor(NodeVisitor *visitor) {
	visitor->visit(std::dynamic_pointer_cast<Geometry>(shared_from_this()));
}

void Geometry::setVertices(const vr::Vec3Vector& vertices) {
	m_vertices = vertices;

	glBindVertexArray(vao);
	glDeleteBuffers(1, &vertices_vbo);

	glGenBuffers(1, &vertices_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vertices_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size()*sizeof(vr::Vec3), m_vertices.data(), GL_STATIC_DRAW);


	CheckErrorsGL("setVertices");

	glBindVertexArray(0);
}

void Geometry::setNormals(const vr::Vec3Vector& normals) {
	if (normals.size() == 0) {
		return;
	}

	m_normals = normals;

	glBindVertexArray(vao);
	glDeleteBuffers(1, &normals_vbo);

	glGenBuffers(1, &normals_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, normals_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_normals.size()*sizeof(vr::Vec3), m_normals.data(), GL_STATIC_DRAW);

	CheckErrorsGL("setNormals");

	glBindVertexArray(0);
}
void Geometry::setIndices(const vr::UIntVector& indices) {
	m_indices = indices;

	glBindVertexArray(vao);
	glDeleteBuffers(1, &indices_ebo);

	glGenBuffers(1, &indices_ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_ebo);
	CheckErrorsGL("indices0");
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size()*sizeof(unsigned int), m_indices.data(), GL_STATIC_DRAW);

	CheckErrorsGL("setIndices");

	glBindVertexArray(0);
}
void Geometry::setTexCoords(const vr::Vec2Vector& texCoords) {
	if (texCoords.size() == 0) {
		return;
	}

	m_texCoords = texCoords;

	glBindVertexArray(vao);
	glDeleteBuffers(1, &texCoords_vbo);

	glGenBuffers(1, &texCoords_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, texCoords_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_texCoords.size()*sizeof(vr::Vec2), m_texCoords.data(), GL_STATIC_DRAW);

	CheckErrorsGL("setTexCoords");

	glBindVertexArray(0);
}

void Geometry::initBuffers() {
	glBindVertexArray(vao);
	glDeleteBuffers(1, &vbo);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);


	glBufferData(GL_ARRAY_BUFFER, m_vertices.size()*sizeof(vr::Vec3), m_vertices.data(), GL_STATIC_DRAW);

	CheckErrorsGL("setVertices");
}




