/*
 * Node.h
 *
 *  Created on: Jan 28, 2016
 *      Author: tfy12jsg
 */

#ifndef NODE_H_
#define NODE_H_

#include <memory>

#include "State.h"
#include "NodeVisitor.h"
#include "UpdateCallback.h"

using namespace std;

class Node : public std::enable_shared_from_this<Node> {

public:
	virtual ~Node() {};

	virtual void acceptVisitor(NodeVisitor *visitor) =0;

	void setState(shared_ptr<State> state) {m_state = state;}
	bool hasState() {return (m_state) ? true : false;}
	shared_ptr<State> getState() {return m_state;}

	void setUpdateFunction(UpdateFunction update_function) {m_updateFunction = update_function;}
	bool hasUpdateFunction() {return (m_updateFunction != NULL) ? true : false;}
	UpdateFunction getUpdateFunction() {return m_updateFunction;}

	void setKeyboardInputFunction(keyboardInputFunction function) {m_keyboardFunction = function;}
	bool hasKeyboardInputFunction() {return (m_keyboardFunction != NULL) ? true : false;}
	keyboardInputFunction getKeyboardInputFunction() {return m_keyboardFunction;}

	void setMouseMotionFunction(mouseMotionInputFunction function) {m_mouseMotionFunction = function;}
	bool hasMouseMotionFunction() {return (m_mouseMotionFunction != NULL) ? true : false;}
	mouseMotionInputFunction getMouseMotionFunction() {return m_mouseMotionFunction;}

protected:
	shared_ptr<State> m_state;
	UpdateFunction m_updateFunction = NULL;
	keyboardInputFunction m_keyboardFunction = NULL;
	mouseMotionInputFunction m_mouseMotionFunction = NULL;

private:

};



#endif /* NODE_H_ */
