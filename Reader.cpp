/*
 * Reader.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: tfy12jsg
 */

#include "Reader.h"

#include <vr/ObjLoader.h>
#include <vr/FileSystem.h>
#include <vr/ImageReader.h>
#include <vr/BMPReader.h>
#include <vr/JPGReader.h>
#include <vr/PNGReader.h>
#include <vr/Matrix.h>
#include <iostream>
#include <string>
#include <memory>

using namespace std;

size_t Reader::loadObjFile(const std::string& filename, GeometrySharedPtrVector& geometries)
{
	std::string filepath = filename;
	bool exist = vr::FileSystem::exists(filepath);

	char *vrPath = getenv("VR_PATH");
	if (!vrPath)
		std::cerr << "The environment variable VR_PATH is not set. It should point to the directory where the vr library is (just above models)" << std::endl;

	if (!exist && vrPath)
	{
		filepath = std::string(vrPath) + "/" + filename;
		exist = vr::FileSystem::exists(filepath);
	}

	if (!exist)
	{
		std::cerr << "The file " << filename << " does not exist" << std::endl;
		return 0;
	}

	// We will try to find the material file in the same directory as the obj file
	std::string path = vr::FileSystem::getDirectory(filepath);
	if (path.size() > 0)
		path = path + "/";

	vr::shape_t_vector shapes;
	vr::material_t_vector materials;

	// Now load the obj file and collect shapes and materials
	std::string err = vr::LoadObj(shapes, materials, filepath.c_str(), path.c_str());

	// Nothing found, something went wrong
	if (!err.empty())
	{
		std::cerr << "Error: " << err << std::endl;
		return 0;
	}

	// Go through each shape and create a Geometry that we can render
	for (vr::shape_t_vector::iterator it = shapes.begin(); it != shapes.end(); ++it)
	{
		vr::shape_t& shape = *it;
		std::shared_ptr<Geometry> geom(new Geometry);
		geom->setVertices(shape.mesh.positions);
		geom->setIndices(shape.mesh.indices);
		geom->setNormals(shape.mesh.normals);
		geom->setTexCoords(shape.mesh.texcoords);
		geometries.push_back(geom);

		// Do we have a material for this mesh?
		if (!materials.empty() && !shape.mesh.material_ids.empty())
		{
			vr::material_t material = materials[shape.mesh.material_ids[0]];


			// Collect all the textures if available
			if (!material.ambient_texname.empty()) {
				std::cout <<"ambient: " <<material.ambient_texname << std::endl;
				material.ambient_texname = path + material.ambient_texname;
			}

			if (!material.diffuse_texname.empty()) {
				std::cout << "diffuse: "<<material.diffuse_texname << std::endl;
				material.diffuse_texname = path + material.diffuse_texname;
			}


			if (!material.specular_texname.empty()) {
				std::cout << "specular: "<<material.specular_texname << std::endl;
				material.specular_texname = path + material.specular_texname;
			}


			if (!material.bump_texname.empty()) {
				std::cout << "bump: " << material.bump_texname << std::endl;
				material.bump_texname = path + material.bump_texname;
			}

			geom->setMaterial(material);

		}
	}

	return geometries.size();
}

vr::Image* Reader::loadImage(string filepath) {
	std::string type = vr::FileSystem::getFileExtension(filepath);
	std::transform(type.begin(), type.end(), type.begin(), ::toupper);

	std::string nativePath = vr::FileSystem::convertToNativeFilePath(filepath);
	vr::ImageReader *reader;
	if (type == "BMP")
		reader = new vr::BMPReader();
	else if (type == "PNG")
		reader = new vr::PNGReader();
	else if (type == "JPG")
		reader = new vr::JPGReader();
	else
	{
		std::cerr << "Invalid image type: " << filepath << std::endl;
		return 0;
	}

	vr::Image *image = reader->readImage(nativePath);
	if (!image){
		std::cerr << "Unable to load image: " << filepath << std::endl;
		delete reader;
		return 0;
	}

	delete reader;

	return image;
}

void Reader::normalizeModel(vr::shape_t_vector &shapes) {
	for (vr::shape_t &shape : shapes) {
		float minX=shape.mesh.positions[0]._v[0], maxX=shape.mesh.positions[0]._v[0];
		float minY=shape.mesh.positions[0]._v[1], maxY=shape.mesh.positions[0]._v[1];
		float minZ=shape.mesh.positions[0]._v[2], maxZ=shape.mesh.positions[0]._v[2];
		float maxLength = 0;
		for (vr::Vec3 &v : shape.mesh.positions) {
			if (v[0] < minX)
				minX = v[0];
			else if (v[0] > maxX)
				maxX = v[0];
			if (v[1] < minY)
				minY = v[1];
			else if (v[1] > maxY)
				maxY = v[1];
			if (v[2] < minZ)
				minZ = v[2];
			else if (v[2] > maxZ)
				maxZ = v[2];

		}

		for (vr::Vec3 &v : shape.mesh.positions) {
			v = vr::Matrix::translate(-(maxX+minX)/2, -(maxY+minY)/2,-(maxZ+minZ)/2) * v;
			if (v.length() > maxLength)
				maxLength = v.length();
		}
	    cout << maxLength << endl;
		for (vr::Vec3 &v : shape.mesh.positions) {
			v = v * (1/maxLength);
			//cout << v << endl;
		}
	}
}

