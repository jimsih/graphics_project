/*
 * UpdateCallback.h
 *
 *  Created on: Feb 1, 2016
 *      Author: tfy12jsg
 */

#ifndef UPDATECALLBACK_H_
#define UPDATECALLBACK_H_

#include <functional>

using namespace std;

class Node;

typedef function<void(shared_ptr<Node>& )> UpdateFunction;

typedef function<void(shared_ptr<Node>&, unsigned char key, int x, int y)> keyboardInputFunction;

typedef function<void(shared_ptr<Node>&, int x, int y)> mouseMotionInputFunction;

typedef function<void(shared_ptr<Node>&, int button, int state, int x, int y)> mouseInputFunction;


#endif /* UPDATECALLBACK_H_ */
