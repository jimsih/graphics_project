/*
 * UpdateVisitor.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: jimmy
 */

#include <iostream>

#include "UpdateVisitor.h"

#include "Scene.h"
#include "Node.h"
#include "Camera.h"
#include "Transform.h"
#include "Geometry.h"
#include "Light.h"

UpdateVisitor::UpdateVisitor() {
	// TODO Auto-generated constructor stub

}

UpdateVisitor::~UpdateVisitor() {
	// TODO Auto-generated destructor stub
}

void UpdateVisitor::traverseScene() {
	Scene *scene = Scene::getInstance();

	shared_ptr<Node> mainCamera = scene->getMainCamera();
	if (mainCamera->hasUpdateFunction()) {
		mainCamera->getUpdateFunction()(mainCamera);
	}

	for (shared_ptr<Node> &node : scene->getNodes()) {
		node->acceptVisitor(this);
	}

	for (shared_ptr<Node> light : scene->getLightSources()) {
		Light* l = (Light*) light.get();
		if (l->isActive()) {
			if (l->hasUpdateFunction()) {
				l->getUpdateFunction()(light);
			}
		}
	}
}

void UpdateVisitor::visit(shared_ptr<Camera> camera) {
	shared_ptr<Node> node(camera);
	if (node->hasUpdateFunction()) {
		node->getUpdateFunction()(node);
	}
}

void UpdateVisitor::visit(shared_ptr<Transform> transform) {
	shared_ptr<Node> node(transform);
	if (node->hasUpdateFunction()) {
		node->getUpdateFunction()(node);
	}

}

void UpdateVisitor::visit(shared_ptr<Geometry> geometry) {
	shared_ptr<Node> node(geometry);
	if (node->hasUpdateFunction()) {
		node->getUpdateFunction()(node);
	}
}

void UpdateVisitor::visit(shared_ptr<RenderToTexture> render2Texture) {

}
