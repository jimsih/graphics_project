
#include "Material.h"

#include <iostream>

Material::Material() {
	vr::material_t material;
	material.ambient = vr::Vec3(0.3, 0.3 ,0.3);
	material.diffuse = vr::Vec3(1.0, 1.0 ,1.0);
	material.specular = vr::Vec3(0.5, 0.5 ,0.5);
	material.shininess = 10.0f;

	m_material = material;
}

Material::~Material() {

}

void Material::apply(GLuint program) {
	glUniform3fv(glGetUniformLocation(program, "material.ambient"), 1, m_material.ambient.ptr());
	glUniform3fv(glGetUniformLocation(program, "material.diffuse"), 1, m_material.diffuse.ptr());
	glUniform3fv(glGetUniformLocation(program, "material.specular"), 1, m_material.specular.ptr());
	glUniform1f(glGetUniformLocation(program, "material.shininess"), m_material.shininess);

}


