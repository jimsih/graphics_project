/*
 * Texture.h
 *
 *  Created on: Feb 8, 2016
 *      Author: jimmy
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <string>

using namespace std;

class Texture {
public:
	Texture();
	Texture(string file);
	Texture(GLuint textureID);
	virtual ~Texture();

	void loadFile(string file);

	virtual void apply(int textureUnit);

protected:
	GLuint m_idTexture =0;

private:

};

#endif /* TEXTURE_H_ */
