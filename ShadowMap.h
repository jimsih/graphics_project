/*
 * ShadowMap.h
 *
 *  Created on: Feb 16, 2016
 *      Author: tfy12jsg
 */

#ifndef SHADOWMAP_H_
#define SHADOWMAP_H_

#include "RenderToTexture.h"

class ShadowMap : public RenderToTexture {
public:
	ShadowMap();
	virtual ~ShadowMap();

	void acceptVisitor(NodeVisitor *visitor);
	void apply();

private:
	GLuint m_framebuffer;

};

#endif /* SHADOWMAP_H_ */
