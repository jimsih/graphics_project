/*
 * InputHandler.h
 *
 *  Created on: Feb 5, 2016
 *      Author: tfy12jsg
 */

#ifndef INPUTHANDLER_H_
#define INPUTHANDLER_H_

#include <vector>
#include <memory>

#include "Node.h"

using namespace std;

class InputHandler {

public:
	static InputHandler* getInstance() {
		if (!instance) {
			instance = new InputHandler();
		}
		return instance;
	}

	void keyboardListener(unsigned char key, int x, int y);

	void mouseListener(int x, int y);

	void mouseClickListener(int button, int state, int x, int y);

	void addListener(shared_ptr<Node> listener);

	bool isClicked() {return clicked;}
	pair<int,int> getLastMousePosition() {return pair<int,int>(mouseX, mouseY);}

private:
	static InputHandler* instance;

	vector<shared_ptr<Node>> listeners;
	bool clicked = false;
	int mouseX = 0;
	int mouseY = 0;
};


#endif /* INPUTHANDLER_H_ */
