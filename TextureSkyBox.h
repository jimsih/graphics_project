/*
 * TextureSkyBox.h
 *
 *  Created on: Feb 24, 2016
 *      Author: tfy12jsg
 */

#ifndef TEXTURESKYBOX_H_
#define TEXTURESKYBOX_H_

#include <vector>
#include <string>

#include "Texture.h"

using namespace std;

class TextureSkyBox : public Texture {
public:
	TextureSkyBox();
	virtual ~TextureSkyBox();

	void apply(int textureUnit);
	void loadCubemap(vector<string> faces);

private:

};

#endif /* TEXTURESKYBOX_H_ */
