/*
 * RenderVisitor.h
 *
 *  Created on: Feb 7, 2016
 *      Author: jimmy
 */

#ifndef RENDERVISITOR_H_
#define RENDERVISITOR_H_

#include <stack>
#include <deque>
#include <memory>
#include <vr/Matrix.h>

#include "NodeVisitor.h"
#include "State.h"
#include "RenderToTexture.h"
#include "ShadowMap.h"

using namespace std;

class RenderVisitor : NodeVisitor {
public:
	RenderVisitor();
	virtual ~RenderVisitor();

	void traverseScene();
	void visit(shared_ptr<Group> group);
	void visit(shared_ptr<Camera> camera);
	void visit(shared_ptr<Transform> transform);
	void visit(shared_ptr<Geometry> geometry);
	void visit(shared_ptr<RenderToTexture> render2Texture);

private:
	stack<vr::Matrix> m_matrixStack;
	stack<shared_ptr<State>> m_stateStack;
	stack<shared_ptr<Camera>> m_cameraStack;

	GLuint m_program = 0;
};

#endif /* RENDERVISITOR_H_ */
