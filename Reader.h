/*
 * Reader.h
 *
 *  Created on: Feb 3, 2016
 *      Author: tfy12jsg
 */

#ifndef READER_H_
#define READER_H_

#include <vector>
#include <memory>
#include <string>
#include <GL/glew.h>
#include <vr/Image.h>

#include "Geometry.h"

using namespace std;

typedef vector<shared_ptr<Geometry>> GeometrySharedPtrVector;

class Reader {
public:
	Reader() {}

	size_t loadObjFile(const string& filename, GeometrySharedPtrVector& geometries);
	vr::Image* loadImage(string filepath);
	void normalizeModel(vr::shape_t_vector &shapes);
protected:

private:
};



#endif /* READER_H_ */
