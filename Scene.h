/*
 * Scene.h
 *
 *  Created on: Feb 5, 2016
 *      Author: tfy12jsg
 */

#ifndef SCENE_H_
#define SCENE_H_

#include <memory>
#include <vector>

#include "Camera.h"
#include "State.h"
#include "Reader.h"
#include "ShadowMap.h"
#include "HUD.h"
#include "GBuffer.h"
#include "UpdateVisitor.h"
#include "RenderVisitor.h"
#include "PostProcessor.h"

using namespace std;

class Scene {

public:
	static Scene* getInstance() {
		if (!instance) {
			instance = new Scene();
		}
		return instance;
	}

	Scene();

	void draw();

	void screenSizeUpdate();

	shared_ptr<Camera> getMainCamera() {return m_defaultCamera;}
	void setMainCamera(shared_ptr<Camera> camera) {m_defaultCamera = camera;}

	vector<shared_ptr<Node>> getNodes() {return m_nodes;}
	void addNode(shared_ptr<Node> node) {m_nodes.push_back(node);}

	vector<shared_ptr<Node>> getLightSources() {return m_lightsources;}
	void addLightSource(shared_ptr<Node> light) {m_lightsources.push_back(light);}

	shared_ptr<Geometry> debugQuad;

	void setGlobalProgram(GLuint program) {m_program = program;}
	GLuint getGlobalProgram() {return m_program;}

	void useGlobalProgram(bool b) {m_globalProgram = b;}
	bool isGlobalProgram() {return m_globalProgram;}

	void setRenderShadow(bool b) {m_renderShadow = b;}
	bool isRenderShadow() {return m_renderShadow;}

	bool isRenderFromLight() {return renderFromLight;}
	void setRenderFromLight(bool b) {renderFromLight = b;}

	ShadowMap* getShadowMap() {return &m_shadowMap;}
	vr::Matrix getShadowBiasMatrix(unsigned int lightNumber);

	void setSkybox(shared_ptr<Node> skybox) {m_skybox = skybox;}
	void renderSkybox();

	void renderBillboards();

	HUD* getHUD() {return &m_hud;}
	GBuffer* getGBuffer() {return &m_gBuffer;}

	void setDebugMode(bool b) {m_debugMode = b;}
	bool getDebugMode() {return m_debugMode;}

	void applyLightsources(GLuint program);

	void setPostProcessor(PostProcessor *postProcessor) {m_postProcessor = postProcessor;}
	PostProcessor* getPostProcessor() {return m_postProcessor;}


private:
	void renderShadowPass();
	void renderGeometryPass();

	static Scene* instance;

	shared_ptr<Camera> m_defaultCamera;

	vector<shared_ptr<Node>> m_nodes;

	vector<shared_ptr<Node>> m_lightsources;

	GLuint m_program = 0;
	bool m_globalProgram = false;

	bool m_renderShadow = false;

	bool renderFromLight = false;

	ShadowMap m_shadowMap;
	vr::Matrix biasMatrix;

	shared_ptr<Node> m_skybox;

	HUD m_hud;

	GBuffer m_gBuffer;

	RenderVisitor renderVisitor;
	UpdateVisitor updateVisitor;

	bool m_debugMode = false;

	PostProcessor* m_postProcessor = nullptr;
};



#endif /* SCENE_H_ */
